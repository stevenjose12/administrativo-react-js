import React from "react";
import { Button, Input, Select } from "../../components";
import { axios, Globals } from "../../utils";

class CreateEditBrand extends React.Component {
  state = {
    form: {},
    users: [],
    textButton: "Crear"
  };
  componentDidMount() {
    if (this.props.edit) {
      this.edit();
      this.getEnterprises();
    } else {
      this.getEnterprises();
    }
  }
  submit = async () => {
    let param = { ...this.state.form };
    Globals.setLoading();
    axios
      .upload(
        this.props.edit ? "admin/brands/edit" : "admin/brands/create",
        param
      )
      .then(res => {
        this.setState({
          form: {}
        });
        Globals.showSuccess(res.data.msg);
        this.props.onClose();
      })
      .catch(err => {
        if (err.response.status === 422) {
          Globals.showError(err.response.data.msg);
          return;
        }
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  edit = async () => {
    await this.setState({
      form: {
        creator_id: this.props.edit.element.creator_id,
        code: this.props.edit.element.code,
        name: this.props.edit.element.name,
        user_id: this.props.edit.element.user_id,
        id: this.props.edit.element.id
      },
      textButton: "Editar"
    });
  };

  change = e => {
    if (e.target.name === "code") {
      e.target.value = e.target.value.toUpperCase();
    }
    let user_id = "";
    if (this.props.user.role === 1 || this.props.user.role === 2) {
      user_id = this.props.user.id;
    } else {
      user_id =
        this.props.user.role === 4
          ? this.props.user.enterprise_users.enterprise_id
          : this.props.user.id;
    }
    this.setState({
      form: {
        user_id: user_id,
        creator_id: this.props.user.id,
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  getEnterprises = async () => {
    Globals.setLoading();
    axios
      .get("admin/warehouses/enterprises")
      .then(res => {
        if (res.data.result) {
          this.setState({
            users: res.data.users
          });
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  render() {
    const admin =
      this.props.user.role === 1 || this.props.user.role === 2 ? true : false;
    let selectEnterprise;
    if (admin) {
      selectEnterprise = (
        <Select
          color="gray"
          name="user_id"
          label="Empresa"
          defaultname="Seleccione"
          disabledFirst={false}
          onChange={this.change}
          value={this.state.form.user_id}
          options={this.state.users.map(i => {
            return {
              value: i.id,
              label: i.name
            };
          })}
        />
      );
    }
    return (
      <div className="text-center container-create-edit-user">
        <div className="row">
          <div className="col-md-12">
            <Input
              color="gray"
              value={this.state.form.name}
              name="name"
              label="Nombre de la marca"
              onChange={this.change}
            />
          </div>
          <div className="col-md-12">
            <Input
              color="gray"
              value={this.state.form.code}
              name="code"
              label="Código"
              onChange={this.change}
            />
          </div>
          <div className="col-md-12">{selectEnterprise}</div>
        </div>
        <div id="button">
          <Button block="true" type="button" onClick={() => this.submit()}>
            {this.state.textButton}
          </Button>
        </div>
      </div>
    );
  }
}

export default CreateEditBrand;
