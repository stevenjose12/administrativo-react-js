import React from 'react';
import { List } from '../../../components';
import { Colors } from '../../../utils';
// import { Avatar, Icon, List } from '../../../components';
// import { ENV, Colors } from '../../../utils';
// import User from '../../../assets/img/user.jpg';

class ViewUser extends React.Component {
	state = {
		role: this.props.role
	};

	render() {
		let renderModules
		if(this.state.role.role_modules.length > 0) {
			renderModules = 
			this.state.role.role_modules.map((value, index) => {
				return (
					<List.Item key={value.module.id} label="Nombre">
						{value.module.name}
					</List.Item>
				)
			})
		} else {
			renderModules = 
			<List.Item key="none">
				No posee ningun modulo asignado
			</List.Item>
		}
		return (
			<div className="text-center container-view-user">
				<List.Container>
					{renderModules}
				</List.Container>
			</div>
		);
	}
}

export default ViewUser;
