const token = (state = null, action) => {
  switch (action.type) {
    case "SET_TOKEN":
      return action.payload;
    case "REFRESH_TOKEN":
      return action.payload;
    case "REMOVE_TOKEN":
      return null;
    default:
      return state;
  }
};

export default token;
