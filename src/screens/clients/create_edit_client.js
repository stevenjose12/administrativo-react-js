import React from "react";
import { Button, Select, Input, Textarea } from "../../components";
import { axios, Globals, Constants } from "../../utils";

class CreateEditClient extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {},
      zones: props.zones,
      enterprises: props.administrators,
      retentions: props.retentions,
      textButton: "Crear"
    };
  }

  ident_docs = {
    document_type: "V",
    document: ""
  };

  fiscal_docs = {
    document_type: "V",
    document: ""
  };

  componentDidMount() {
    if (this.props.edit) {
      this.edit();
    }
  }

  submit = async () => {
    if (!this.state.form.document_type) {
      this.setState({
        ...this.state.form,
        document_type: "V",
        document: "V-" + this.state.form.document
      });
    } else if (
      !this.state.form.document_type.indexOf("-") ||
      this.state.form.document_type
    ) {
      this.setState({
        ...this.state.form,
        document: this.state.form.document_type + "-" + this.state.form.document
      });
    }

    Globals.setLoading();
    axios
      .post(
        this.props.edit ? "admin/clients/edit" : "admin/clients/create",
        this.state.form
      )
      .then(res => {
        if (res.data.result) {
          this.setState({
            form: {}
          });
          Globals.showSuccess(res.data.msg);
          this.props.onClose();
        }
      })
      .catch(err => {
        let message = "Disculpe, ha ocurrido un error";
        if (err.response.status === 422) {
          message = err.response.data.msg;
        }
        Globals.showError(message);
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  edit = async () => {
    if (this.props.edit.element.identity_document) {
      if (
        !this.props.edit.element.identity_document.indexOf("-") ||
        parseInt(this.props.edit.element.identity_document)
      ) {
        this.ident_docs = {
          document_type: "V",
          document: this.props.edit.element.identity_document
        };
      } else {
        let division = this.props.edit.element.identity_document.split("-");
        this.ident_docs = {
          document_type: division[0],
          document: division[1]
        };
      }
    }
    if (this.props.edit.element.fiscal_identification) {
      if (
        !this.props.edit.element.fiscal_identification.indexOf("-") ||
        parseInt(this.props.edit.element.fiscal_identification)
      ) {
        this.fiscal_docs = {
          document_type: "V",
          document: this.props.edit.element.fiscal_identification
        };
      } else {
        let division = this.props.edit.element.fiscal_identification.split("-");
        this.fiscal_docs = {
          document_type: division[0],
          document: division[1]
        };
      }
    }
    await this.setState({
      form: {
        first_name: this.props.edit.element.first_name,
        last_name: this.props.edit.element.last_name,
        email: this.props.edit.element.email,
        phone: this.props.edit.element.phone,
        direction: this.props.edit.element.direction,
        person_type: this.props.edit.element.person_type,
        fiscal_document_type: this.fiscal_docs.document_type,
        fiscal_identification: this.fiscal_docs.document,
        identity_document: this.ident_docs.document,
        document_type: this.ident_docs.document_type,
        zone_id: this.props.edit.element.zone_user.zone_id,
        enterprise_id: this.props.edit.element.client_enterprise.enterprise_id,
        days_deadline:
          (this.props.edit.element.configuration_client || {}).days_deadline ||
          "",
        retention_type_id: this.props.edit.element.user_retention_types
          ? this.props.edit.element.user_retention_types.retention_type_id
          : "",
        creator_id: this.props.user.id,
        id: this.props.edit.element.id
      },
      textButton: "Editar"
    });
  };

  change = e => {
    let enterprise_id = "";
    if (this.props.user.role === 1 || this.props.user.role === 2) {
      enterprise_id = this.props.user.id;
    } else {
      enterprise_id =
        parseInt(this.props.user.role) === 4
          ? parseInt(this.props.user.enterprise_users.enterprise_id)
          : parseInt(this.props.user.id);
    }
    this.setState({
      form: {
        enterprise_id: enterprise_id,
        creator_id: this.props.user.id,
        fiscal_document_type: this.fiscal_docs.document_type,
        document_type: this.ident_docs.document_type,
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  setSelectedOption = async (value, key) => {
    this.setState({
      form: {
        ...this.state.form,
        [key]: value
      }
    });
    if (key === "document_type") this.ident_docs.document_type = value;
    if (key === "fiscal_document_type") this.fiscal_docs.document_type = value;
  };

  render() {
    const admin =
      this.props.user.role === 1 || this.props.user.role === 2 ? true : false;
    let selectEnterprise;
    if (admin) {
      selectEnterprise = (
        <div className="col-md-6">
          <Select
            color="gray"
            name="enterprise_id"
            label="Empresa"
            defaultname="Seleccione"
            disabledfirst="false"
            onChange={this.change}
            value={this.state.form.enterprise_id}
            options={this.state.users.map(i => {
              return {
                value: i.id,
                label: i.name
              };
            })}
          />
        </div>
      );
    }
    return (
      <div className="text-center container-create-edit-user">
        <div className="row">
          <div className="col-md-12">
            <Input
              color="gray"
              value={this.state.form.first_name}
              name="first_name"
              label="Nombre y apellido / Razón social"
              onChange={this.change}
            />
          </div>
          {/* <div className="col-md-6">
                        <Input
                            color="gray"
                            value={this.state.form.last_name}
                            name="last_name" 
                            label="Apellido"
                            onChange={this.change} />
                    </div> */}
        </div>
        <div className="row">
          <div className="col-md-6">
            <div style={{ display: "flex" }}>
              <Select
                color="gray"
                label="Tipo"
                name="document_type"
                defaultname="Seleccione"
                disabledFirst="true"
                onChange={e =>
                  this.setSelectedOption(e.target.value, e.target.name)
                }
                value={
                  this.state.form.document_type
                    ? this.state.form.document_type
                    : "V"
                }
                options={Constants.TYPE_IDENTITY}
              />
              <Input
                color="gray"
                value={this.state.form.identity_document}
                name="identity_document"
                label="Cédula"
                onKeyPress={e => {
                  Globals.soloNumeros(e);
                }}
                maxLength={this.state.form.document_type === "J" ? 9 : 8}
                onChange={this.change}
              />
            </div>
          </div>
          <div className="col-md-6">
            <div style={{ display: "flex" }}>
              <Select
                color="gray"
                label="Tipo"
                name="fiscal_document_type"
                defaultname="Seleccione"
                disabledFirst="true"
                onChange={e =>
                  this.setSelectedOption(e.target.value, e.target.name)
                }
                value={
                  this.state.form.fiscal_document_type
                    ? this.state.form.fiscal_document_type
                    : "V"
                }
                options={Constants.TYPE_DOCUMENTS}
              />
              <Input
                color="gray"
                value={this.state.form.fiscal_identification}
                name="fiscal_identification"
                label="RIF"
                onKeyPress={e => {
                  Globals.soloNumeros(e);
                }}
                maxLength={this.state.form.document_type === "J" ? 11 : 10}
                onChange={this.change}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className={admin ? "col-md-6" : "col-md-12"}>
            <Input
              color="gray"
              value={this.state.form.email}
              name="email"
              type="email"
              label="E-Mail"
              onChange={this.change}
            />
          </div>
          {selectEnterprise}
        </div>
        <div className="row">
          <div className="col-md-6">
            <Input
              color="gray"
              value={this.state.form.phone}
              name="phone"
              label="Teléfono"
              maxLength={11}
              onKeyPress={e => {
                Globals.soloNumeros(e);
              }}
              onChange={this.change}
            />
          </div>
          <div className="col-md-6">
            <Select
              color="gray"
              name="zone_id"
              label="Zona"
              defaultname="Seleccione"
              onChange={this.change}
              disabledFirst="false"
              value={this.state.form.zone_id}
              options={this.state.zones.map(i => {
                return {
                  value: i.id,
                  label: i.name
                };
              })}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Select
              color="gray"
              name="person_type"
              label="Personalidad Jurídica"
              defaultname="Seleccione"
              disabledFirst="false"
              onChange={this.change}
              value={this.state.form.person_type}
              options={[
                {
                  value: 1,
                  label: "Natural"
                },
                {
                  value: 2,
                  label: "Jurídica"
                }
              ]}
            />
          </div>
          <div className="col-md-6">
            <Select
              color="gray"
              name="retention_type_id"
              label="Tipo de Retencion"
              defaultname="Seleccione"
              disabledFirst="false"
              onChange={this.change}
              value={this.state.form.retention_type_id}
              options={this.state.retentions.map(i => {
                return {
                  value: i.id,
                  label: i.percentage
                };
              })}
            />
          </div>
        </div>
        <div className="row">
          <div className="col col-md">
            <Input
              color="gray"
              name="days_deadline"
              label="Plazo de crédito"
              maxLength={3}
              onKeyPress={e => {
                Globals.soloNumeros(e);
              }}
              onChange={this.change}
              value={this.state.form.days_deadline}
            />
          </div>
        </div>
        <div className="row">
          <div className="col col-md">
            <Textarea
              label="Dirección"
              name="direction"
              value={this.state.form.direction ? this.state.form.direction : ""}
              onChange={this.change}
            />
          </div>
        </div>

        {/*<CheckBox
                    label="Contribuyente Especial"
                    name="especial_contributor"
                    value={this.state.form.especial_contributor }
                    onChange={this.change}
                />*/}
        <div id="button">
          <Button block="true" type="button" onClick={() => this.submit()}>
            {this.state.textButton}
          </Button>
        </div>
      </div>
    );
  }
}
export default CreateEditClient;
