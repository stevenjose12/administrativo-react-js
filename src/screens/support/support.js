import React from 'react';
import Menu from '../menu';
import { connect } from 'react-redux';
import { Input, Button, Icon, Modal} from '../../components';
import File from '../../components/file';
import { Globals, axios, Socket, ENV } from '../../utils';
import moment from 'moment';
import { on } from 'jetemit';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css'; // This only needs to be imported once in your app
import CreateChat from './create_chat';


class Support extends React.Component {

	state = {
		chat: null,
		chats: [],
		messages: [],
		message: '',
		unsuscriber: null,
		unsuscriberCreate: null,
		image: null,
		lightBox: {},
		makeChat: false,
	}

	componentWillUnmount() {
		if (this.state.unsuscriber) {
			this.state.unsuscriber();
		}
		if (this.state.unsuscriberCreate) {
			this.state.unsuscriberCreate();
		}
	}

	componentDidMount() {
		this.loadChats();

		const unsuscriber = on('message',async message => {
			if (this.state.chat && message.chat.id == this.state.chat.id) {
				message.viewed = 1;

				await this.setState({
					messages: [
						...this.state.messages,
						message
					]				
				},this.scrollToBottom);
				if (message.user_id != this.props.user.id) {
					await axios.post('admin/support/viewed',{ id: message.id }).catch(err => {
					});
				}
			}
			this.loadChats(false)
		});

		this.setState({
			unsuscriber: unsuscriber,
		});
	}

	loadChats = (loading = true) => {
		if (loading)
			Globals.setLoading();
		axios.all([
			axios.post('admin/support/chats', { id: this.props.user.id }),
			// axios.post('admin/support/count')
		]).then(axios.spread((res,count) => {
			if (res.data.result) {
				let chatsFilter = res.data.chats.data.filter(i => i.updated_at != null);
				const chats = chatsFilter.sort((a,b) => {
					return new Date(b.updated_at) - new Date(a.updated_at);
				});
				this.setState({
					chats: chats
				});
				this.props.dispatch({
					type: 'SET_SUPPORT',
					payload: res.data.count
				});
			}
		}))
			.catch(err => {
				Globals.showError();
			})
			.then(() => {
				Globals.quitLoading();
			});
	}

	loadMessages = () => {
		Globals.setLoading();
		axios.post('admin/support/messages',{ id: this.state.chat.id, user_id: this.props.user.id })
			.then(res => {
				if (res.data.result) {
					let chats = this.state.chats;
					let index = chats.findIndex(i => i.id == this.state.chat.id);
					chats[index] = res.data.chat;
					this.setState({
						messages: res.data.chat.messages,
						chats: chats
					},this.scrollToBottom);

					this.props.dispatch({
						type: 'SET_SUPPORT',
						payload: res.data.count
					});
				}
			})
			.catch(err => {
				Globals.showError();
			})
			.then(() => {
				Globals.quitLoading();
			});
	}

	setActive = async chat => {
		await this.setState({
			chat: chat,
		});
		this.loadMessages();
	}

	getPosition = message => {
		let position = 'left';
		if (message.user.level != 1) {
			position = 'right';
		}
		return position;
	}

	scrollToBottom = () => {
		this.messages.scrollTop = this.messages.scrollHeight;
	}

	send = async (validate = true) => {
		if(validate){
			if (this.state.message == '') {
			  return false;
			}
		}
		let image = null;
		if(this.state.image){
			await axios.upload('admin/support/image', {image: this.state.image})
				.then(res => {
					if (res.data.result) {
						image = res.data.route
					}
				})
			.catch(err => {
				Globals.showError();
			})
		}
	    // Socket.emit('message',{
		// 	user_id: this.props.user.id,
	    // 	chat_id: this.state.chat.id,
		// 	text: !validate ? ' ' : this.state.message,
		// 	file: image
		// });
		this.setState({
			message: !validate ? this.state.message : '',
			image: null
		},this.scrollToBottom);
	}

	change = async e => {
		const name = e.target.name
		await this.setState({
			[e.target.name]: e.target.value
		});
		if(name == 'image'){
			this.send(false)
		}
	}

	onKeyUp = e => {
		e.preventDefault();
		if (e.key == 'Enter') {
			this.send();
		}
	}

	render() {
		return (
			<div>

				<Menu history={ this.props.history }>
					<div id="support">
						<div className="container-chat">
							<div className="row">
								<div className="col-md-4 no-padding-right">
									<div className="list-chats">
										<div className={ `item-chat` } 
											onClick={ () => this.setState({
												makeChat: true,
											}) }
											>
											<h2>Iniciar un chat</h2>
										</div>
										{ this.state.chats.map((i,index) => {
											return (
												<div key={ index } className={ `item-chat ${ this.state.chat && this.state.chat.id == i.id ? 'active' : '' }` } onClick={ () => this.setActive(i) }>
													{ i.count > 0 && <div className="badge">{ i.count }</div> }
													<h2>{ i.users[0].person.name + ' ' + i.users[0].person.lastname }</h2>
													{ i.messages.length > 0 && (
														<React.Fragment>
															<p>{ i.messages[i.messages.length - 1].message }</p>
															<p className="date">{ moment(i.updated_at).format('DD/MM/YYYY HH:mm') }</p> 
														</React.Fragment>
													) }
													{ i.messages.length == 0 && <p>No hay mensajes</p> }
												</div>
											)
										}) }
									</div>
								</div>
								<div className="col-md-8 no-padding-left">
									<div className="container-messages">
										{ this.state.chat && <div className="top-messages">
											<h2><span>Chat con:</span> { this.state.chat.users[0].person.name+' '+this.state.chat.users[0].person.lastname }</h2>
										</div> }

										<div className="container-scroll" ref={ ref => this.messages = ref }>
											{ !this.state.chat && <h2 className="no-chat">Por favor, seleccione un chat</h2> }

											{ this.state.messages.map((i,index) => {
												return (
													<div className={ `message ${ this.getPosition(i) }` } key={ index }>
														<h2>{ i.text }</h2>
														{
															i.file ?
															<img
																src={ENV.BasePublic+i.file}
																style={{
																	width: '80px',
																	height: '80px',
																	borderRadius: '40px',
																	margin: '5px 0px',
																}}
																onClick={() => this.setState({
																	lightBox:{
																		open: true,
																		route: ENV.BasePublic+i.file
																	}
																})}
															/>
															:null
														}
														<p>{ moment(i.updated_at).format('DD/MM/YYYY HH:mm') }</p>
														<div className={ `triangle-${ this.getPosition(i) }` }></div>
													</div>
												)									
											}) }
										</div>

										<div className="container-input">
											<Input 
												disabled={ !this.state.chat || this.state.chat.support_status == 0 }
												value={ this.state.message }
												onChange={ this.change }
												name="message"
												onKeyUp={ this.onKeyUp } />
											<Button disabled={ !this.state.chat }>
												{
													this.state.chat ? 	
													<File
														renderItem={
															<Icon name="paperclip" style={{fontSize: '20px', marginTop: '3px'}} />
														}
														onChange={ this.change }
														name="image"
														inputstyle={{
															display: 'contents'
														}}
														className="btn-product" />
													: <Icon name="paperclip" style={{fontSize: '20px', marginTop: '3px'}} />
												}
											</Button>
											<Button onClick={ this.send } disabled={ !this.state.chat || this.state.chat.support_status == 0  || !this.state.message}>
												<Icon name="caret-right" />
											</Button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</Menu>
				{ this.state.lightBox.open && (
					<Lightbox
						mainSrc={this.state.lightBox.route}
						onCloseRequest={() => this.setState({ 
							lightBox: {
								open: false,
								route: null
							}
						})}
					/>
				) }
				<Modal
                    title="Crear Chat" 
                    onClose={ () => {
							this.setState({
								makeChat: false,
							})
							this.loadChats()
						}
					}
                    visible={ this.state.makeChat }>
                    <CreateChat
						onClose={ () => {
								this.setState({
									makeChat: false,
								})
								this.loadChats()
							}
						}
					/>
                </Modal>
			</div>
		)
	}
}

export default connect(state => {
	return {
		user: state.user,
		support: state.support
	}
})(Support);