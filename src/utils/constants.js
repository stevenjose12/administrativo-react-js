const STATUS_ACTIVE = 1;
const STATUS_INACTIVE = 0;

const STATUS_INV_COMMITTED = 0;
const STATUS_INV_PROCESSED = 1;

const STATUS_PAYMENT_INCOME = 1;
const STATUS_PAYMENT_EXPENSES = 2;

const STATE_SUSPENDED = 2;
const STATE_PROCESSED = 1;

const ACTION_CLASS_ENTRY = 1;
const ACTION_CLASS_OUTPUT = 2;

const EXEMPT_ACTIVE = 0;
const EXEMPT_INACTIVE = 1;

const PURCHASE_BILL = 2;
const PURCHASE_ORDER = 1;

const STEP_SELECTION = 1;
const STEP_FORM = 2;

const STEP_SELLER_DATA = 1;
const STEP_SELLER_CONF = 2;

const TYPE_ENTRY = 1;
const TYPE_OUTPUT = 2;

const TYPE_DELIVERY = 1;
const TYPE_BILL = 2;

const TYPE_PAYMENTS_TRANSFER = 4;

const TYPE_PRODUCT = 1;
const TYPE_SALE = 2;

const VALUE_ZERO = 0;

const ROLE_SUPER = 1;
const ROLE_ADMIN = 2;
const ROLE_ENTERPRISE = 3;
const ROLE_USER = 4;
const ROLE_SELLER = 5;

const STATUS_RESPONSE_SERVER = 500;
const STATUS_RESPONSE_UNAUTHORIZED = 401;

const TYPE_DOCUMENTS = [
  { value: "V", label: "V" },
  { value: "E", label: "E" },
  { value: "J", label: "J" }
];

const TYPE_IDENTITY = [
  { value: "V", label: "V" },
  { value: "E", label: "E" },
  // { value: "J", label: "J" }
];

const TYPE_PERSON = [
  {
    value: 1,
    label: "Natural"
  },
  {
    value: 2,
    label: "Jurídica"
  }
];

const TYPE_ADJUSTMENT = [
  {
    value: 1,
    label: "Carga"
  },
  {
    value: 2,
    label: "Descarga"
  },
  {
    value: 3,
    label: "Traslado"
  }
];

const TYPE_ADJUSTMENT_LIMITED = [
  {
    value: 1,
    label: "Carga"
  },
  {
    value: 2,
    label: "Descarga"
  }
];

const SERIALIZATION = {
  ACTIVE: 1,
  INACTIVE: 0
};

const DISCOUNT_TYPES = {
  byProduct: 1,
  bySale: 2 
}

const SELLER_MODULES = {
  SALE_POINT: 22
}

const TYPE_REPORT_CHARGES_STATUS = [
  {
    value: 0,
    label: 'Sin Usar'
  },
  {
    value: 1,
    label: 'Usado'
  }
]

const TYPE_BANKS = [
  {
    value: 1,
    label: 'Bancaria'
  },
  {
    value: 2,
    label: 'Solo Efectivo'
  }
]

const TYPE_BANK_ACCOUNTS = [
  {
    value: 1,
    label: 'Corriente'
  },
  {
    value: 2,
    label: 'Ahorro'
  },
  {
    value: 3,
    label: 'Caja'
  }
]

const EXPENSES_INGRESS_PAYMENT_TYPE_LIST = {
  CASH: 1,
  CREDIT: 2
}

const EXPENSES_INGRESS_METHOD_TYPE_LIST = {
  CASH: 1,
  CREDIT: 2,
  DEBIT: 3,
  TRANSFER: 4
}

const TYPE_BANKS_LIST = {
  REGULAR: 1,
  CASH_ONLY: 2
}

const TYPE_BANK_ACCOUNTS_LIST = {
  CHECKING: 1,
  SAVINGS: 2,
  CASH_ONLY: 3
}

const SALE_PAYMENT_TYPES = {
  CASH: 1,
  CCARD: 2,
  DEBIT: 3,
  TRANSFER: 4,
  CREDIT: 5
}

const SALE_PAYMENT_PROCESSED_LIST = {
  NOT_PROCESSED: 0,
  PROCESSED: 1
}

const CASH_COUNT_METHOD_PAYMENTS = [
  {
    value: 1,
    label: 'Efectivo',
    name: 'cash'
  },
  {
    value: 2,
    label: 'T. Credito',
    name: 'credit'
  },
  {
    value: 3,
    label: 'Debito',
    name: 'debit'
  },
  {
    value: 4,
    label: 'Transferencia',
    name: 'transfer'
  }
]

const SCANNER = "SCANNER"; // Para eventos de escaneo de código de barra

export default {
  STATUS_ACTIVE,
  STATUS_INACTIVE,
  STATUS_INV_COMMITTED,
  STATUS_INV_PROCESSED,
  STATUS_PAYMENT_INCOME,
  STATUS_PAYMENT_EXPENSES,
  STATUS_RESPONSE_SERVER,
  STATUS_RESPONSE_UNAUTHORIZED,
  ACTION_CLASS_ENTRY,
  ACTION_CLASS_OUTPUT,
  STATE_SUSPENDED,
  STATE_PROCESSED,
  STEP_SELECTION,
  STEP_FORM,
  STEP_SELLER_DATA,
  STEP_SELLER_CONF,
  TYPE_ENTRY,
  TYPE_OUTPUT,
  TYPE_PERSON,
  TYPE_ADJUSTMENT,
  TYPE_ADJUSTMENT_LIMITED,
  TYPE_DELIVERY,
  TYPE_BILL,
  TYPE_PAYMENTS_TRANSFER,
  TYPE_PRODUCT,
  TYPE_SALE,
  EXEMPT_ACTIVE,
  EXEMPT_INACTIVE,
  PURCHASE_BILL,
  PURCHASE_ORDER,
  TYPE_DOCUMENTS,
  SERIALIZATION,
  VALUE_ZERO,
  ROLE_SUPER,
  ROLE_ADMIN,
  ROLE_ENTERPRISE,
  ROLE_USER,
  ROLE_SELLER,
  SCANNER,
  TYPE_IDENTITY,
  DISCOUNT_TYPES,
  SELLER_MODULES,
  TYPE_REPORT_CHARGES_STATUS,
  TYPE_BANKS,
  TYPE_BANK_ACCOUNTS,
  EXPENSES_INGRESS_PAYMENT_TYPE_LIST,
  EXPENSES_INGRESS_METHOD_TYPE_LIST,
  TYPE_BANK_ACCOUNTS_LIST,
  TYPE_BANKS_LIST,
  SALE_PAYMENT_TYPES,
  SALE_PAYMENT_PROCESSED_LIST,
  CASH_COUNT_METHOD_PAYMENTS
};
