import React from "react";

import { Button, Input, InputFormat, Select } from "../../components";
import { Globals, Constants } from "../../utils";
import { Bank, Payment } from "../../services";
import File from "../../components/file";

const TYPE_PAYMENT = [
  { value: 1, name: "cash", label: "Efectivo" },
  { value: 2, name: "credit", label: "Credito" },
  { value: 3, name: "debit", label: "Debito" },
  { value: 4, name: "transfer", label: "Transferencia" }
];

class Payments extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      form: {
        client_id: (props.customer || {}).id || "",
        method_payment: "",
        bank_id: "",
        bank_account_id: "",
        amount: "",
        current_amount: "",
        type: 1,
        voucher: "",
        creator_id: props.Id,
        reference_number: ""

      },
      banks: props.banks,
      bank_accounts: [],
      errors: []
    };
  }

  handleChange = emitter => {
    const { name, value } = emitter.target;
    const { banks } = this.props;


    switch (name) {
      case 'bank_id':
        this.setState(state => ({
          form: {
            ...state.form,
            [name]: parseInt(value),
            bank_account_id: ""
          }
        }), () => this.loadBankAccounts());
        break;
      case "reference_number":
        this.setState(state => ({
          form: {
            ...state.form,
            [name]: value
          },
        }))
        break;
      case 'method_payment':
        let banksMapped = []
        if (value && parseInt(value) === Constants.EXPENSES_INGRESS_METHOD_TYPE_LIST.CASH) {
          banksMapped = banks.filter(Item => Item.type === Constants.TYPE_BANKS_LIST.CASH_ONLY)
        } else {
          banksMapped = banks.filter(Item => Item.type !== Constants.TYPE_BANKS_LIST.CASH_ONLY)
        }
        this.setState(state => ({
          form: {
            ...state.form,
            [name]: value,
            bank_id: "",
            bank_account_id: ""
          },
          banks: banksMapped,
          bank_accounts: []
        }));
        break;
      default:
        this.setState(state => ({
          form: {
            ...state.form,
            [name]: parseInt(value)
          }
        }));
        break;
    }
  };

  handleValueChange = (values, name) => {
    const { floatValue } = values;

    this.setState(state => ({
      form: {
        ...state.form,
        [name]: floatValue,
        current_amount: floatValue
      }
    }));
  };

  handleSubmit = emitter => {
    emitter.preventDefault();

    const { form, submitted } = this.state;
    const { onClose, processingPayment, customer } = this.props;

    if (!form.amount) {
      Globals.showError('El monto debe ser mayor a 0')
      return
    }

    if (submitted) {
      return;
    }

    // Vaciando los campos de errores al enviar solicitud
    this.setState({ submitted: true, errors: [] });

    Payment.createPayment(form)
      .then(response => {
        Globals.showSuccess(`Pago agregado exitosamente!`);
        !!customer ? processingPayment(response) : onClose();
      })
      .catch(error => {
        this.setState({ submitted: false, errors: error });
      });
  };

  loadBankAccounts = () => {
    const { form } = this.state
    Bank.getBankAccounts({ bank_id: form.bank_id })
      .then(response => {

        let bankAccountsMapped = [];

        if (form.method_payment && parseInt(form.method_payment) === Constants.EXPENSES_INGRESS_METHOD_TYPE_LIST.CASH) {
          bankAccountsMapped = response
            .filter(Item => Item.status === Constants.STATUS_ACTIVE)
            .filter(Item => Item.type === Constants.TYPE_BANK_ACCOUNTS_LIST.CASH_ONLY)
            .map((Item, key) => {
              return {
                value: Item.id,
                label: Item.account_number
              };
            })
        } else {
          bankAccountsMapped = response
            .filter(Item => Item.status === Constants.STATUS_ACTIVE)
            .map((Item, key) => {
              return {
                value: Item.id,
                label: Item.account_number
              };
            })
        }

        this.setState(state => ({
          form: {
            ...state.form,
            bank_account_id: ""
          },
          bank_accounts: bankAccountsMapped
        }));

      })
      .catch(() => Globals.showError());
  }

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback my-2 text-left">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  render() {
    const { banks, submitted, form, bank_accounts } = this.state;
    const { customers, customer } = this.props;

    const showFile = parseInt(form.method_payment) === Constants.TYPE_PAYMENTS_TRANSFER;

    return (
      <form onSubmit={this.handleSubmit}>
        <div className="container-create-edit-payment">
          <div className="row">
            <div className="col-md">
              {customer ? (
                <Input
                  color="gray"
                  value={(customer.person || {}).first_name || ""}
                  name="name"
                  label="Nombre y apellido"
                  disabled
                />
              ) : (
                  <Select
                    color="gray"
                    label="Cliente"
                    name="client_id"
                    options={customers.map(Item => {
                      return {
                        value: Item.id,
                        label: (Item.person || {}).first_name || (Item || {}).name
                      };
                    })}
                    value={this.state.form.client_id}
                    error={`${this.hasErrorFor("client_id") ? "is-invalid" : ""}`}
                    invalidfeedback={this.renderErrorFor("client_id")}
                    onChange={this.handleChange}
                  />
                )}
            </div>
          </div>
          <div className="row">
            <div className="col-md">
              <Select
                color="gray"
                label="Método de pago"
                name="method_payment"
                options={TYPE_PAYMENT.map(Item => {
                  return {
                    value: Item.value,
                    label: Item.label
                  };
                })}
                value={this.state.form.method_payment}
                error={`${
                  this.hasErrorFor("method_payment") ? "is-invalid" : ""
                  }`}
                invalidfeedback={this.renderErrorFor("method_payment")}
                onChange={this.handleChange}
              />
            </div>
          </div>
          {
            (parseInt(this.state.form.method_payment)
              ===
              Constants.EXPENSES_INGRESS_METHOD_TYPE_LIST.TRANSFER)
            && (
              <div className="row">
                <div className="col-md">
                  <Input
                    label="Numero de referencia"
                    name="reference_number"
                    value={this.state.form.reference_number}
                    color="gray"
                    onKeyPress={emitter =>
                      Globals.soloNumeros(emitter)
                    }
                    onChange={this.handleChange}
                  />
                </div >
              </div >

            )
          }
          <div className="row">
            <div className="col-md">
              <InputFormat
                color="gray"
                name="amount"
                thousandSeparator={true}
                allowNegative={false}
                isNumericString={true}
                decimalScale="2"
                label="Monto"
                value={this.state.form.amount}
                error={`${this.hasErrorFor("amount") ? "is-invalid" : ""}`}
                invalidfeedback={this.renderErrorFor("amount")}
                onValueChange={values =>
                  this.handleValueChange(values, "amount")
                }
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md">
              <Select
                color="gray"
                label="Banco"
                name="bank_id"
                options={banks}
                value={this.state.form.bank_id}
                error={`${
                  this.hasErrorFor("bank_id") ? "is-invalid" : ""
                  }`}
                invalidfeedback={this.renderErrorFor("bank_id")}
                onChange={this.handleChange}
              />
            </div>
            <div className="col-md">
              <Select
                color="gray"
                label="Cuenta Bancaria"
                name="bank_account_id"
                options={bank_accounts}
                value={this.state.form.bank_account_id}
                error={`${
                  this.hasErrorFor("bank_account_id") ? "is-invalid" : ""
                  }`}
                invalidfeedback={this.renderErrorFor("bank_account_id")}
                onChange={this.handleChange}
              />
            </div>
          </div>

          {showFile && (
            <div className="row">
              <div className="col-md">
                <File
                  label="Comprobante de pago"
                  placeholder="Agregar comprobante"
                  placeholdersuccess="Comprobante agregado"
                  showcheck={true.toString()}
                  onChange={this.handleChange}
                  name="voucher"
                  value={this.state.form.voucher}
                  inputstyle={{
                    display: "contents"
                  }}
                  className="btn-product"
                  error={`${this.hasErrorFor("voucher") ? "is-invalid" : ""}`}
                  invalidfeedback={this.renderErrorFor("voucher")}
                />
              </div>
            </div>
          )}

          <div className="row text-center">
            <div className="col-md">
              <div id="button">
                <Button
                  submitted={submitted ? submitted : undefined}
                  type="submit"
                  block="true"
                  disabled={submitted}
                >
                  Agregar
                </Button>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default Payments;
