const sidebar = (state = [], action) => {
  switch (action.type) {
    case "SET_SIDEBAR":
      return action.payload;
    case "QUIT_SIDEBAR":
      return [];
    default:
      return state;
  }
};

export default sidebar;
