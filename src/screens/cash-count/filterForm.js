import React from "react";

import { Select, Button } from "../../components";
import { Globals, Constants, Format } from "../../utils";
import { Warehouse } from "../../services";

const TYPE_LIST = [
  { value: -1, label: "Todos" },
  { value: 0, label: "Pendiente" },
  { value: 1, label: "Procesado" },
  { value: 2, label: "Anulado" }
];

class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        branch_id: [],
        warehouse_id: Globals.getAssignedWarehouses(props.user),
        user_id: Globals.getUserId(props.user)
      },
      warehouses: []
    };
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.form.branch_id !== this.state.form.branch_id &&
      this.state.form.branch_id
    ) {
      this.maybeLoadWarehouses();
    }
  }

  maybeLoadWarehouses = () => {
    const { form } = this.state;
    const { user } = this.props;
    const warehouse_id = Globals.getAssignedWarehouses(user)

    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    Warehouse.getWarehouseByBranch({
      user_id: enterpriseId,
      branch_id: form.branch_id
    }).then(response => {
      let warehouses_filtered = response.filter(
        ({ deleted_at }) => !deleted_at
      );

      const warehousesMap = Format.rawWarehouse(
        warehouses_filtered,
        user.assigned_warehouses,
        user.role
      )
        .filter(({ status }) => status === Constants.STATUS_ACTIVE)
        .map(({ id, name }) => ({
          value: id,
          label: name
        }));

      this.setState(state => ({
        form: {
          ...state.form,
          warehouse_id: warehouse_id
        },
        warehouses: warehousesMap
      }));
    });
  };

  handleChange = (emitter, string) => {
    let { name, value } = emitter.target;

    if(name === 'branch_id' || name === 'warehouse_id') {
      value = [parseInt(value)]
    }

    this.setState(state => ({
      form: {
        ...state.form,
        [name]: value
      }
    }));
  };

  render() {
    const { customers, sellers, branches, status, report } = this.props;
    const { form, warehouses } = this.state;

    return (
      <form
        onSubmit={emitter => {
          emitter.preventDefault();
          this.props.submitted(form);
        }}
      >
        <div className="row">

          {status && (
            <div className="col-md">
              <Select
                color="white"
                name="status"
                label="Estatus"
                style={{ textTransform: "capitalize" }}
                onChange={emitter => this.handleChange(emitter, null)}
                value={form.status}
                options={TYPE_LIST}
              />
            </div>
          )}

          {!report && (
            <div className="col-md">
              <div className="form-group">
                <Button
                  className="btn-block btn-align-bottom">
                  Filtrar
                </Button>
              </div>
            </div>
          )}
        </div>

        {report && (
          <React.Fragment>
            <div className="row">
              <div className="col-md">
                <Select
                  color="white"
                  name="branch_id"
                  label="Sucursal"
                  style={{ textTransform: "capitalize" }}
                  onChange={emitter => this.handleChange(emitter, null)}
                  value={form.branch_id}
                  options={branches}
                />
              </div>
              <div className="col-md">
                <Select
                  color="white"
                  name="warehouse_id"
                  label="Almacenes"
                  style={{ textTransform: "capitalize" }}
                  onChange={emitter => this.handleChange(emitter, null)}
                  value={form.warehouse_id}
                  options={[
                    ...warehouses,
                    { value: 0, label: "Todos" }
                  ].reverse()}
                />
              </div>
              <div className="col-md">
                <div className="form-group">
                  <Button
                    className="btn-block btn-align-bottom">
                    Filtrar
                  </Button>
                </div>
              </div>
            </div>
          </React.Fragment>
        )}
      </form>
    );
  }
}

export default Filter;
