import React from "react";
import { Avatar, List } from "../../components";
import { ENV } from "../../utils";

class ViewProduct extends React.Component {
  state = {
    data: this.props.data
  };

  render() {
    return (
      <div className="text-center container-view-user">
        {this.state.data.avatar ? (
          <Avatar
            source={ENV.BasePublic + this.state.data.avatar}
            size="130px"
          />
        ) : null}
        <List.Container>
          <List.Item label="Nombre">{this.state.data.name}</List.Item>
          <List.Item label="Código">{this.state.data.code}</List.Item>
          <List.Item label="Descripcion">
            {this.state.data.description ? this.state.data.description : "Sin descripción"}
          </List.Item>
          <List.Item label="Porcentaje de comision">
            {this.state.data.percentage_commission}
          </List.Item>
          <List.Item label="Exento">
            {this.state.data.exempt === 0 ? "NO" : "SI"}
          </List.Item>
          _{" "}
          <List.Item label="Categoria">
            {this.state.data.product_category
              ? this.state.data.product_category.category
                ? this.state.data.product_category.category.name
                : "Sin categoría"
              : "Sin categoría"}
          </List.Item>
          <List.Item label="Subcategoría">
            {this.state.data.product_subcategory
              ? this.state.data.product_subcategory.subcategory
                ? this.state.data.product_subcategory.subcategory.name
                : "Sin subcategoría"
              : "Sin subcategoría"}
          </List.Item>
          <List.Item label="Marca">
            {this.state.data.product_brand
              ? this.state.data.product_brand.brand
                ? this.state.data.product_brand.brand.name
                : "Sin marca"
              : "Sin marca"}
          </List.Item>
          <List.Item label="Modelo">
            {this.state.data.product_model
              ? this.state.data.product_model.model
                ? this.state.data.product_model.model.name
                : "Sin modelo"
              : "Sin modelo"}
          </List.Item>
        </List.Container>
      </div>
    );
  }
}

export default ViewProduct;
