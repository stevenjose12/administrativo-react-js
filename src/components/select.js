import React from "react";

class Select extends React.Component {
  render() {
    return (
      <div className={`form-group ${this.props.size === "small" ? "m-0" : ""}`}>
        {this.props.label && (
          <label htmlFor={this.props.name}>
            {this.props.label} {this.props.icon}
          </label>
        )}
        <select
          {...this.props}
          className={`form-control${this.props.size === "small" ? "-sm" : ""} ${
            this.props.color ? this.props.color : ""
          } ${this.props.error ? this.props.error : ""}
          ${this.props.className ? this.props.className : ""}`}
        >
          <option
            value={this.props.defaultvalue !== undefined ? this.props.defaultvalue : ""}
            disabled={this.props.disabledFirst === undefined ? true : false}
          >
            {this.props.defaultname ? this.props.defaultname : "Seleccione"}
          </option>
          {this.props.options.map((i, index) => {
            return (
              <option
                key={index}
                value={i.value}
                second={i.second ? i.second : ""}
              >
                {i.label}
              </option>
            );
          })}
        </select>
        {this.props.invalidfeedback}
      </div>
    );
  }
}

export default Select;
