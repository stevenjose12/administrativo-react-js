import React from "react";
import { connect } from "react-redux";

import { Globals, Format, Constants, Colors } from "../../utils";
import { Button, Modal, Table, Icon } from "../../components";
import { Bank, Request, Seller, Branch, CashCount as CC } from "../../services";

// Components
import Menu from "../menu";
import Filter from "./filterForm";
import ManageCashCount from "./manage-cash-count";

const TABLE_HEADER = [
  "Metodo de Pago",
  "Monto",
  "Estatus",
  "Procesar"
];

class CashCount extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      form: null,
      total: 0,
      pending: 0,
      Item: null,
      visible: false,
      customers: [],
      sellers: [],
      branches: [],
      banks: []
    };
  }

  componentDidMount() {
    this.load();
  }

  load = () => {
    const { user } = this.props;
    const Id = user.id;
    const zonesId = Globals.getZones(user);
    const warehouse_id = Globals.getAssignedWarehouses(user);

    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    const getCustomers =
      user.role === Constants.ROLE_SELLER
        ? Request.getCustomersBySeller({
            zones_id: zonesId,
            enterprise_id: enterpriseId
          })
        : Request.getCustomersByEnterprise({ Id: enterpriseId });

    Promise.all([
      CC.getNotProcessedRequests({
        enterprise_id: enterpriseId,
        user_id: Id,
        warehouse_id: warehouse_id,
        role: user.role
      }),
      getCustomers,
      Seller.getSellersByEnterprise({ user_id: enterpriseId }),
      Branch.getBranchesByEnterprise({ Id: enterpriseId }),
      Bank.getBanks({enterprise_id: enterpriseId})
    ])
      .then(response => {
        const data = response[0];
        const customersMap = response[1];
        const sellersMap = Format.rawSeller(response[2]);
        let total = 0;
        let pending = 0;

        const dataMap = Format.rawMethodPayments(data);
        const dataFormatted = Format.rawCashCounts(dataMap);
        if(dataFormatted.length > 0) {
          total = dataFormatted.reduce((prev, next) => prev + next.amount, 0);
          pending = dataFormatted.reduce((prev, next) => prev + next.current_amount, 0);
        }

        let branches_filtered = response[3].filter(
          ({ deleted_at }) => !deleted_at
        );

        branches_filtered = Format.rawBranches(
          branches_filtered,
          user.assigned_warehouses,
          user.role
        );

        const branchesMap = branches_filtered
          .filter(({ status }) => status === Constants.STATUS_ACTIVE)
          .map(({ id, name }) => ({
            value: id,
            label: name
          }));
          
        const banks = response[4]
          .filter(({ status }) => status === Constants.STATUS_ACTIVE)
          .map(({ id, name, type }) => ({
            value: id,
            label: name,
            type: type
          }));

        this.setState({
          customers: customersMap,
          sellers: sellersMap,
          branches: branchesMap,
          data: dataFormatted,
          total: total,
          pending: pending,
          banks: banks
        });
      })
      .catch((err) => {
        console.log(err, 'err') 
        Globals.showError() 
      });
  };

  loadMore = () => {
    const { user } = this.props;
    const { form } = this.state;

    const Id = user.id;
    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;
    const warehouse_id = Globals.getAssignedWarehouses(user);

    CC.getNotProcessedRequests({
      enterprise_id: enterpriseId,
      user_id: Id,
      warehouse_id: warehouse_id,
      role: user.role,
      ...form
    })
      .then(response => {
        const data = response;
        let total = 0;
        let pending = 0;

        const dataMap = Format.rawMethodPayments(data);
        const dataFormatted = Format.rawCashCounts(dataMap);
        if(dataFormatted.length > 0) {
          total = dataFormatted.reduce((prev, next) => prev + parseFloat((next.amount).toFixed(2)), 0);
          pending = dataFormatted.reduce((prev, next) => prev + parseFloat((next.current_amount).toFixed(2)), 0);
        }

        this.setState({
          data: dataFormatted,
          total: total,
          pending: pending
        });
      })
      .catch(() => Globals.showError());
  };

  filterMap = form => {
    const { user } = this.props;
    const Id = user.id;
    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    CC.getNotProcessedRequests({
      ...form,
      enterprise_id: enterpriseId,
      user_id: Id,
      role: user.role,
    })
      .then(response => {
        const data = response;
        let total = 0;
        let pending = 0;

        const dataMap = Format.rawMethodPayments(data);
        const dataFormatted = Format.rawCashCounts(dataMap);
        if(dataFormatted.length > 0) {
          total = dataFormatted.reduce((prev, next) => prev + parseFloat((next.amount).toFixed(2)), 0);
          pending = dataFormatted.reduce((prev, next) => prev + parseFloat((next.current_amount).toFixed(2)), 0);
        }

        this.setState({
          data: dataFormatted,
          total: total,
          pending: pending,
          form: { ...form }
        });
      })
      .catch(() => Globals.showError());
  };

  cashCount = () => {
    
    const { data } = this.state;
    const { user } = this.props;
    const userId = user.id;
    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    const pendingAmount = data.reduce((a, b) => {
      return a + parseFloat(b.current_amount)
    }, 0)

    if(pendingAmount > 0) {
      Globals.showError('El Monto Pendiente debe ser procesado en su totalidad')
      return;
    }

    const paymentsToProcess = data.map(payment => {
      return payment.payments;
    }).flat();
    const bankPayments = data.map(payment => {
      return payment.bank_payments;
    }).flat();

    Globals.setLoading();
    CC.processCashCount({
      enterpriseId: enterpriseId,
      userId: userId,
      paymentsToProcess: paymentsToProcess,
      bankPayments: bankPayments
    })
    .then(response => {

      const data = response;
      let total = 0;
      let pending = 0;
      
      const dataMap = Format.rawMethodPayments(data);
      const dataFormatted = Format.rawCashCounts(dataMap);
      if(dataFormatted.length > 0) {
        total = dataFormatted.reduce((prev, next) => prev + parseFloat((next.amount).toFixed(2)), 0);
        pending = dataFormatted.reduce((prev, next) => prev + parseFloat((next.current_amount).toFixed(2)), 0);
      }

      this.setState({
        data: dataMap,
        total: total,
        pending: pending
      });
      Globals.showSuccess('Caja cerrada exitosamente!')
    })
    .catch(() => Globals.showError())
    .finally(() => Globals.quitLoading());
  };

  deployManage = Item => {
    this.setState({ Item: Item, visible: true });
  };

  processingPayment = (newPayment) => {

    const { Item, data } = this.state;
    const key = data.findIndex(({ id }) => id === Item.id);
    const newData = [...data];

    newData[key] = {
      ...newData[key],
      amount_available: newData[key].amount_available + parseFloat(newPayment.amount)
    };

    newPayment.amount = parseFloat(newPayment.amount);
    newPayment.current_amount = parseFloat(newPayment.current_amount);
    newPayment.type = parseInt(newPayment.type);
    newPayment.status = 0;

    this.setState(state => ({
      visibleCreate: false,
      visibleProcessing: true,
      Item: {
        ...state.Item,
        amount_available: newData[key].amount_available,
        payments: [...state.Item.payments, newPayment]
      },
      data: newData
    }));
  };

  close = Item => {
    if(!Item) {
      this.setState({
        visible: false
      });
      return;
    }
    if(Item.current_amount > 0) {
      Globals.showError('El Monto Pendiente debe ser procesado en su totalidad')
      return;
    }
    let { data } = this.state;
    const index = data.findIndex( method => parseInt(method.method_type) === parseInt(Item.method_type))
    data[index] = Item;
    const pending = data.reduce((prev, next) => prev + next.current_amount, 0);
    this.setState(state => ({
      ...state,
      visible: false,
      Item: null,
      data: data,
      pending: pending
    }));
  };

  render() {
    const {
      banks,
      total,
      pending,
      customers,
      data,
      Item,
      sellers,
      branches,
      visible
    } = this.state;
    const { user, history } = this.props;
    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    return (
      <Menu history={history}>
        {visible && (
          <Modal
            className="modal-delivery"
            title="Administrar Montos"
            onClose={this.close}
            visible
          >
            <ManageCashCount
              enterpriseId={enterpriseId}
              Item={Item}
              onClose={this.close}
              processingPayment={this.processingPayment}
              banks={banks}
            />
          </Modal>
        )}

        <div id="delivery">
          <div className="container-fluid">
            <Filter
              report
              submitted={form => this.filterMap(form)}
              user={user}
              sellers={sellers}
              branches={[...branches, { value: 0, label: "Todos" }].reverse()}
              customers={customers.map(Item => {
                return {
                  value: Item.client_id,
                  label:
                    (Item.client.person || {}).first_name ||
                    (Item.client || {}).name
                };
              })}
            />

            <div className="row">
              <div className="col-md">
                <Table
                  data={data.length}
                  title="Cierre de Caja"
                  header={TABLE_HEADER}
                  right={
                    <Button
                      title="Cerrar Caja"
                      outline="true"
                      onClick={() => this.cashCount()}
                    >
                      CERRAR CAJA
                    </Button>
                  }
                >
                  {data.length > 0 && data.map((Item, index) => {
                    return (
                      <tr key={index}>
                        <td>{Item.label}</td>
                        <td>{Globals.formatMiles(Item.amount)}</td>
                        {(parseFloat(Item.current_amount) === 0 && Item.bank_payments.length === 0) && (
                          <td style={{ color: Colors.green}}>
                            Procesado
                          </td>
                        )}
                        {(parseFloat(Item.current_amount) === 0 && Item.bank_payments.length > 0) && (
                          <td style={{ color: Colors.orange}}>
                            Pendiente de Cierre
                          </td>
                        )}
                        {(parseFloat(Item.current_amount) > 0) && (
                          <td style={{ color: Colors.red}}>
                            Por Procesar
                          </td>
                        )}
                        <td>
                          {Item.amount > 0 && (
                              <Button
                                color="primary"
                                title="Ver mas"
                                small="true"
                                onClick={() => this.deployManage(Item)}
                              >
                                <Icon name="money" />
                              </Button>
                          )}
                        </td>
                      </tr>
                    );
                  })}
                  <tr>
                    <th>MONTO PENDIENTE</th>
                    <td>{Globals.formatMiles(pending)}</td>
                    <td colSpan="2"></td>
                  </tr>
                  <tr>
                    <th>MONTO TOTAL</th>
                    <td>{Globals.formatMiles(total)}</td>
                    <td colSpan="2"></td>
                  </tr>
                </Table>
              </div>
            </div>
          </div>
        </div>
      </Menu>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(CashCount);
