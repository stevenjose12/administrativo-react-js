import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import {
  Table,
  Pagination,
  Button,
  Icon,
  Modal,
  Input
} from "../../components";
import { axios, Globals, Format, Constants } from "../../utils";

// Components
import Menu from "../menu";
import CreateEditProduct from "./create_edit_product";
import ViewProduct from "./view_product";
import RemoveProduct from "./remove_product";

class Products extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      header: ["Nombre", "Código", "Categoría", "Acciones"],
      page: 1,
      last_page: 1,
      data: [],
      warehouses: [],
      companies: [],
      product: null,
      user: null,
      view: false,
      remove: false,
      providers: {},
      list_status: [
        { value: 0, label: "Suspendidos" },
        { value: 1, label: "Activos" }
      ],
      form: {
        status: "",
        search: ""
      }
    };
  }

  abortController = new AbortController();

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  load = () => {
    const { user } = this.props;
    const enterprise_id = Globals.getEnterpriseId(user);
    const warehouse_id = Globals.getAssignedWarehouses(user);
    this.products(enterprise_id, warehouse_id);
    this.warehouses(enterprise_id);
    this.companies();
  };

  products = (enterprise_id, warehouse_id) => {
    let param = this.state.form;
    param.user_id = enterprise_id;
    param.warehouse_id = warehouse_id;

    Globals.setLoading();
    axios
      .post("admin/products/get?page=" + this.state.page, param)
      .then(res => {
        if (res.data.result) {
          const rawProducts = res.data.products.data.map(product => {
            const product_warehouse = product.product_warehouse.map(
              warehouse => {
                let lastMovement;
                if (warehouse.last_movement) {
                  lastMovement =
                    warehouse.last_movement.type === Constants.TYPE_ENTRY
                      ? warehouse.last_movement.current_stock +
                        warehouse.last_movement.amount
                      : warehouse.last_movement.current_stock -
                        warehouse.last_movement.amount;
                }
                else {
                  lastMovement = 0;
                }                

                return {
                  ...warehouse,
                  stock: lastMovement
                };
              }
            );

            return {
              ...product,
              product_warehouse: product_warehouse
            };
          });

          this.setState({
            last_page: res.data.products.last_page,
            data: rawProducts
          });
        }
      })
      .catch(err => {
        console.log(err);
      })
      .then(() => Globals.quitLoading());
  };

  companies = () => {
    const { user } = this.props;
    let param = {
      role: user.role,
      user_id: user.id
    };
    axios
      .post("admin/products/companies", param)
      .then(res => {
        if (res.data.result) {
          let companies = [];
          res.data.companies.forEach((element, i) => {
            companies.push({
              label: element.name,
              value: element.id,
              models: element.models
            });
          });
          this.setState({
            companies: companies
          });
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(() => Globals.showError())
      .then(() => Globals.quitLoading());
  };

  exportExcel = () => {
    Globals.setLoading();
    const { user } = this.props;

    let param = {
      user_id: user.id
    };

    axios({
      url: `admin/products/export/${param.user_id}`,
      method: "GET",
      responseType: "blob" // important,
      // params: { desde: data.desde, hasta:data.hasta, cli:data.cli }
    })
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "Productos-" + user.name + ".xlsx"); //or any other extension
        document.body.appendChild(link);
        link.click();
      })
      .catch(() => Globals.showError())
      .then(() => Globals.quitLoading());
  };

  close = () => {
    this.setState(
      {
        create: false,
        view: false,
        remove: false,
        product: null,
        edit: null,
        user: null
      },
      () => this.load()
    );
  };

  delete = ({ name, id }) => {
    const { user } = this.props;
    const enterpriseId = Globals.getUserId(user);

    Globals.confirm("¿Desea eliminar el producto: " + name + "?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/products/delete", { id: id, enterprise_id: enterpriseId })
        .then(() => {
          Globals.showSuccess("¡Producto eliminado exitosamente!");
          this.load();
        })
        .catch(({ response }) => Globals.showWarning(response.data.message))
        .then(() => Globals.quitLoading());
    });
  };

  warehouses = enterprise_id => {
    const { user } = this.props;

    const Id = enterprise_id;

    axios
      .post("admin/products/warehouses", {
        role: user.role,
        user_id: Id
      })
      .then(({ data }) => {
        if (data.result) {
          const warehouseMap = Format.rawWarehouse(
            data.warehouses,
            user.assigned_warehouses,
            user.role
          ).map(Item => {
            return {
              ...Item,
              id: Item.id,
              name: Item.name,
              stock_min: "",
              stock_max: "",
              price: "",
              location: "",
              percentage_earning: 0,
              percentage_max: 0,
              percentage_offer: 0
            };
          });

          this.setState({
            warehouses: warehouseMap
          });
        } else {
          Globals.showError(data.msg);
        }
      })
      .catch(() => Globals.showError());
  };

  change = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  render() {
    const { view, create, remove, product } = this.state;

    return (
      <Menu history={this.props.history}>
        {view && (
          <Modal title="Ver Producto" onClose={this.close} visible>
            <ViewProduct data={this.state.product} />
          </Modal>
        )}

        {remove && (
          <Modal
            title={`Eliminar ${product.name}`}
            onClose={this.close}
            visible
          >
            <RemoveProduct product={product} onClose={this.close} />
          </Modal>
        )}

        {create && (
          <Modal
            className="modal-product"
            title={this.state.edit ? "Editar Producto" : "Nuevo Producto"}
            onClose={this.close}
            visible
          >
            <CreateEditProduct
              companies={this.state.companies}
              warehouses={this.state.warehouses}
              edit={this.state.edit}
              onClose={this.close}
            />
          </Modal>
        )}

        <div id="products">
          <div className="row">
            {/* <div className="col-md-2">
              <Button color="green" className="btn-align-bottom" >
								Exportar
							</Button>
						</div> */}
            <div className="col-md-8">
              <Input
                color="white"
                name="search"
                label="Búsqueda"
                onChange={this.change}
                value={this.state.search}
                placeholder="Buscar por Nombre, Código, Categoría, Marca o Descripción"
              />
            </div>
            <div className="col-md-2">
              <Button className="btn-align-bottom" onClick={this.load}>
                Filtrar
              </Button>
            </div>
          </div>
          <Table
            data={this.state.data.length}
            title="Productos"
            header={this.state.header}
            right={
              <Button
                title="Agregar Productos"
                outline="true"
                small="true"
                onClick={() => {
                  this.setState({
                    create: true
                  });
                }}
              >
                <Icon name="plus" />
              </Button>
            }
          >
            {this.state.data.map((i, index) => {
              return (
                <tr key={index}>
                  <td>{i.name}</td>
                  <td>{i.code}</td>
                  <td>
                    {i.product_category
                      ? i.product_category.category
                        ? i.product_category.category.name
                        : "Sin categoría"
                      : "Sin categoría"}
                  </td>
                  <td>
                    <Button
                      title="Ver"
                      small="true"
                      onClick={() =>
                        this.setState({
                          product: i,
                          view: true
                        })
                      }
                    >
                      <Icon name="eye" />
                    </Button>
                    <Button
                      title="Editar"
                      color="primary"
                      small="true"
                      onClick={() =>
                        this.setState({
                          create: true,
                          edit: {
                            element: i,
                            index: index
                          }
                        })
                      }
                    >
                      <Icon name="edit" />
                    </Button>
                    <Link to={`products/providers/${i.id}`}>
                      <Button color="orange" title="Proveedores" small="true">
                        <Icon name="user" />
                      </Button>
                    </Link>
                    <Button
                      color="red"
                      title="Eliminar"
                      small="true"
                      onClick={() =>
                        this.setState({ remove: true, product: i })
                      }
                    >
                      <Icon name="trash" />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </Table>

          <Pagination
            pages={this.state.last_page}
            active={this.state.page}
            onChange={page =>
              this.setState(
                {
                  page: page
                },
                () => this.load()
              )
            }
          />
        </div>
      </Menu>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(Products);
