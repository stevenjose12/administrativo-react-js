import React from 'react';
import { List } from '../../components';
import { Globals } from '../../utils';

class ViewWarehouse extends React.Component {

	state = {
		warehouse: this.props.warehouse
	}
		
	render() {
		return (
			<div className="text-center container-view-user">
				{/* <Avatar 
					source={ User } 
					size="130px" /> */}
				<List.Container>
					<List.Item label="Nombre">
						{ this.state.warehouse.name }
					</List.Item>
					<List.Item label="Descripcion">
						{ this.state.warehouse.description }
					</List.Item>
					<List.Item label="Código">
						{ this.state.warehouse.code }
					</List.Item>
					<List.Item label="Ubicacion">
						{ this.state.warehouse.location ? this.state.warehouse.location : 'La dirección no ha sido registrada' }
					</List.Item>
					<List.Item label="Sucursal">
						{ this.state.warehouse.branch ? this.state.warehouse.branch.name : 'No posee sucursal asignada' }
					</List.Item>
					{ !this.state.warehouse.deleted_at &&(
						<List.Item label="Status">
							<span style={ {
								color: Globals.getStatus(this.state.warehouse.status).color
							} }>{ Globals.getStatus(this.state.warehouse.status).text }</span>
						</List.Item>
					)}
					{ this.state.warehouse.deleted_at &&(
						<List.Item label="Status">
							<span style={ {
								color: 'red'
							} }>Eliminado</span>
						</List.Item>
					)}
				</List.Container>
			</div>
		)
	}
}

export default ViewWarehouse;