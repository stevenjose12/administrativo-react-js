import React from "react";
import moment from "moment";

import { connect } from "react-redux";

import { Globals, Format, Constants } from "../../utils";
import { Pagination, Button, Modal, Table, Icon } from "../../components";
import { Request, Seller, Branch, Zones } from "../../services";

// Components
import View from "./view";
import Menu from "../menu";
import Filter from "./filterForm";

const TABLE_HEADER = [
  "Cliente",
  "Vendedor",
  "Nº",
  "Fecha de emisión",
  "Hora",
  "Tipo",
  "Efectivo",
  "Debito",
  "T.Credito",
  "Transferencia",
  "A Credito",
  "Monto",
  "Detalles"
];

class Report extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      form: null,
      calc: null,
      page: 1,
      current_page: 1,
      last_page: 1,
      Item: null,
      visible: false,
      customers: [],
      sellers: [],
      branches: [],
      zones: []
    };
  }

  componentDidMount() {
    this.load();
  }

  load = () => {
      const { user } = this.props;
    const { page } = this.state;
    const Id = user.id;
    const zonesId = Globals.getZones(user);
    const warehouse_id = Globals.getAssignedWarehouses(user);

    const enterprise_id = (user.enterprise_users || {}).enterprise_id || user.id;

    const getCustomers =
      user.role === Constants.ROLE_SELLER
        ? Request.getCustomersBySeller({
            zones_id: zonesId,
            enterprise_id: enterprise_id
          })
        : Request.getCustomersByEnterprise({ Id: enterprise_id });

    Promise.all([
      Request.getRequestsAllStatus(page, {
        Id: enterprise_id,
        user_id: Id,
        warehouse_id: warehouse_id,
        role: user.role
      }),
      getCustomers,
      Seller.getSellersByEnterprise({ user_id: enterprise_id }),
      Branch.getBranchesByEnterprise({ Id: enterprise_id }),
      Zones.getZones()
    ])
      .then(response => {
        const { current_page, data, last_page } = response[0];

        const customersMap = response[1];
        const sellersMap = Format.rawSeller(response[2]);

        const dataMap = Format.rawInventoryAmount(data);
        const calc = calcAmount(dataMap);

        let branches_filtered = response[3].filter(
          ({ deleted_at }) => !deleted_at
        );

        branches_filtered = Format.rawBranches(
          branches_filtered,
          user.assigned_warehouses,
          user.role
        );

        const branchesMap = branches_filtered
          .filter(({ status }) => status === Constants.STATUS_ACTIVE)
          .map(({ id, name }) => ({
            value: id,
            label: name
          }));

        const rawZones = response[4].map(({ id, name }) => ({
          value: id,
          label: name
        }));

        this.setState({
          customers: customersMap,
          sellers: sellersMap,
          branches: branchesMap,
          zones: rawZones,
          data: dataMap,
          calc: calc,
          current_page: current_page,
          last_page: last_page
        });
      })
      .catch(() => Globals.showError());
  };

  loadMore = () => {
    const { user } = this.props;
    const { page, form } = this.state;

    const Id = user.id;
    const enterprise_id = (user.enterprise_users || {}).enterprise_id || user.id;
    const warehouse_id = Globals.getAssignedWarehouses(user);

    Request.getRequestsAllStatus(page, {
      Id: enterprise_id,
      user_id: Id,
      warehouse_id: warehouse_id,
      role: user.role,
      ...form
    })
      .then(response => {
        let { current_page, data, last_page } = response;

        const dataMap = Format.rawInventoryAmount(data);
        const calc = calcAmount(dataMap);
        
        if(data.length === 0) {
          current_page = 1
        }

        this.setState({
          data: dataMap,
          calc: calc,
          current_page: current_page,
          last_page: last_page
        });
      })
      .catch(() => Globals.showError());
  };

  filterMap = form => {
    const { user } = this.props;
    const Id = user.id;
    const enterprise_id = (user.enterprise_users || {}).enterprise_id || user.id;

    Request.getRequestsAllStatus(1, {
      ...form,
      Id: enterprise_id,
      user_id: Id,
      role: user.role,
    })
      .then(response => {
        const { current_page, data, last_page } = response;

        const dataMap = Format.rawInventoryAmount(data);
        const calc = calcAmount(dataMap);

        this.setState({
          page: 1,
          data: dataMap,
          calc: calc,
          form: form,
          current_page: current_page,
          last_page: last_page,
          form: { ...form }
        });
      })
      .catch(() => Globals.showError());
  };

  exportExcel = () => {
    const { form } = this.state;
    const { user } = this.props;

    const Id = user.id;
    const warehouse_id = Globals.getAssignedWarehouses(user);

    Request.exportExcel({
      Id: Id,
      warehouse_id: warehouse_id,
      role: user.role,
      ...form
    })
      .then(url => {
        window.open(url, "_blank").focus();
      })
      .catch(() => Globals.showError());
  };

  deployRequest = Item => {
    this.setState({ Item: Item, visible: true });
  };

  close = () => {
    this.setState({
      visible: false
    });
  };

  render() {
    const {
      calc,
      customers,
      data,
      Item,
      last_page,
      page,
      sellers,
      branches,
      zones,
      visible
    } = this.state;

    const { user, history } = this.props;
    const Id = Globals.getUserId(user);

    return (
      <Menu history={history}>
        {visible && (
          <Modal
            className="modal-delivery"
            title="Visualizar Pedido"
            onClose={this.close}
            visible
          >
            <View Item={Item} />
          </Modal>
        )}

        <div id="delivery">
          <div className="container-fluid">
            <Filter
              report
              submitted={form => this.filterMap(form)}
              Id={Id}
              user={user}
              sellers={sellers}
              zones={[{ value: 0, label: "Todos" }, ...zones]}
              branches={[...branches, { value: 0, label: "Todos" }].reverse()}
              customers={customers.map(Item => {
                return {
                  value: Item.client_id,
                  label:
                    (Item.client.person || {}).first_name ||
                    (Item.client || {}).name
                };
              })}
            />

            <div className="row">
              <div className="col-md">
                <Table
                  data={data.length}
                  title="Reporte de Ventas"
                  header={TABLE_HEADER}
                  right={
                    <Button
                      title="Exportar Ventas"
                      outline="true"
                      small="true"
                      onClick={() => this.exportExcel()}
                    >
                      EXPORTAR EXCEL
                    </Button>
                  }
                >
                  {data.map((Item, index) => {
                    return (
                      <tr key={index}>
                        <td>
                          {(Item.client.person || {}).first_name ||
                            (Item.client || {}).name}
                        </td>
                        <td>
                          {(Item.seller.person || {}).first_name ||
                            (Item.seller || {}).name}
                        </td>
                        <td>{Item.code}</td>
                        <td className="text-center">
                          {moment(Item.date_emission).format("DD/MM/YYYY")}
                        </td>
                        <td className="text-center">
                          {moment(Item.date_emission).format("HH:mm:ss")}
                        </td>
                        <td>{Item.type}</td>
                        <td>{Globals.formatMiles(Item.cash)}</td>
                        <td>{Globals.formatMiles(Item.debit)}</td>
                        <td>{Globals.formatMiles(Item.credit)}</td>
                        <td>{Globals.formatMiles(Item.transfer)}</td>
                        <td>{Globals.formatMiles(Item.onCredit)}</td>
                        <td>{Globals.formatMiles(Item.total)}</td>
                        <td className="text-center">
                          <Button
                            color="primary"
                            title="Ver mas"
                            small="true"
                            onClick={() => this.deployRequest(Item)}
                          >
                            <Icon name="eye" />
                          </Button>
                        </td>
                      </tr>
                    );
                  })}

                  {!!calc && (
                    <tr>
                      <th colSpan="6">MONTOS TOTALES DE REPORTE DE VENTAS</th>
                      <td>{calc.cash}</td>
                      <td>{calc.debit}</td>
                      <td>{calc.credit}</td>
                      <td>{calc.transfer}</td>
                      <td>{calc.onCredit}</td>
                      <td colSpan="2">{calc.total}</td>
                    </tr>
                  )}
                </Table>
              </div>
            </div>
          </div>

          <div className="row my-3">
            <div className="col-md">
              <Pagination
                pages={last_page}
                active={page}
                onChange={page => {
                  this.setState(
                    {
                      page: page
                    },
                    () => this.loadMore()
                  );
                }}
              />
            </div>
          </div>
        </div>
      </Menu>
    );
  }
}

function calcAmount(items) {
  return items.length > 0
    ? {
        cash: Globals.formatMiles(Format.amountMap(items, "cash")),
        debit: Globals.formatMiles(Format.amountMap(items, "debit")),
        credit: Globals.formatMiles(Format.amountMap(items, "credit")),
        transfer: Globals.formatMiles(Format.amountMap(items, "transfer")),
        onCredit: Globals.formatMiles(Format.amountMap(items, "onCredit")),
        total: Globals.formatMiles(Format.amountMap(items, "total"))
      }
    : null;
}

export default connect(state => {
  return {
    user: state.user
  };
})(Report);
