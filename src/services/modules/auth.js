import axios from "../../utils/axios";

class auth {
  static async authenticate(form) {
    return new Promise((resolve, reject) => {
      axios.post("login", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }

  static async refresh(form) {
    return new Promise((resolve, reject) => {
      axios.post("refresh", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }
}

export default auth;
