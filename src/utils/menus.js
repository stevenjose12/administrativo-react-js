export default Object.freeze([
  {
    // Level types start at one. This is just a hole so we can index
    // them directly.
  },
  {
    id: 1,
    name: "Inventario"
  },
  {
    id: 2,
    name: "Ventas"
  },
  {
    id: 3,
    name: "Compras"
  },
  {
    id: 4,
    name: "Utilidades"
  },
  {
    id: 5,
    name: "Reportes"
  },
  {
    id: 6,
    name: "Cuentas por Cobrar"
  },
  {
    id: 7,
    name: "Gastos"
  },
  {
    id: 8,
    name: "Pagos"
  },
  {
    id: 9,
    name: "Bancos"
  }
]);
