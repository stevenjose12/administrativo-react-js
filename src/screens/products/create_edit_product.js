import React from "react";
import { connect } from "react-redux";
import {
  Button,
  Select,
  Input,
  InputFormat,
  Textarea,
  CheckBox,
  Title,
  Icon
} from "../../components";
import File from "../../components/file";
import { axios, Globals } from "../../utils";

class CreateEditProduct extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      form: {
        code: "",
        brand: "",
        name: "",
        brandCode: "",
        brandName: "",
        model: "",
        modelCode: "",
        modelName: "",
        category: "",
        categoryCode: "",
        categoryName: "",
        subcategory: "",
        subcategoryCode: "",
        subcategoryName: "",
        warehouses: [],
        user_id:
          this.props.user.role === 3
            ? this.props.user.id
            : this.props.user.enterprise_users.enterprise_id,
        role: this.props.user.role,
        exempt: 0,
        serialization: 0,
        // brands and category
        codeBrand: "",
        nameBrand: "",
        codeModel: "",
        nameModel: "",
        codeCateg: "",
        nameCateg: "",
        codeSubCateg: "",
        nameSubCateg: "",
        // product providers
        provider_id: "",
        provider_name: "",
        price: "",
        // providers
        nameProv: "",
        codeProv: "",
        fiscal_identification: "",
        phone: "",
        email: "",
        enterprise_id: "",
        //
        providers: [],
        edit_code: 0,
        edit_name: 0,
        time_limit: 0
      },
      page: 1,
      providers: [],
      categories: [],
      subcategories: [],
      models: [],
      brands: [],
      companies: [],
      warehouses: [],
      warehouses_product: [],
      type_identification: [
        {
          label: "V",
          value: "V"
        },
        {
          label: "J",
          value: "J"
        },
        {
          label: "E",
          value: "E"
        }
      ],
      showCateg: false,
      showSubCateg: false,
      showBrand: false,
      showModel: false,
      showProvider: false,
      edit: false,
      textButton: "Crear",
      lastCode: ""
    };
  }

  fiscal_docs = {
    document_type: "V",
    document: ""
  };

  arrWareData = [
    "stock_min",
    "stock_max",
    "location",
    "percentage_earning",
    "percentage_max",
    "percentage_offer"
  ];

  abortController = new AbortController();

  componentDidMount() {
    if (this.props.edit) {
      Globals.setLoading();

      let param = {
        role: this.props.user.role,
        user_id:
          this.props.user.role === 3
            ? this.props.user.id
            : this.props.user.enterprise_users.enterprise_id,
        id: this.props.edit.element.id
      };

      axios
        .post("admin/products/details", param)
        .then(res => {
          let warehouses_product = res.data.product_warehouse.filter(el => {
            return {
              id: el.id
            };
          });

          let providers_product = res.data.product_provider.filter(el => {
            el.name = el.provide.name;
            return el;
          });

          if (res.data.product_model) {
            this.setState(state => {
              state.form = {
                ...state.form,
                model: res.data.product_model.model.id,
                brand: res.data.product_model.model.brand_id
              };
              return state;
            });
          }
          this.setState(state => {
            state.warehouses_product = warehouses_product;
            state.form.providers = providers_product;
            return state;
          }, this.load);
        })
        .catch(() => Globals.showError())
        .finally(() => Globals.quitLoading());
      return;
    }

    this.setState(state => {
      state.form.code = this.getCode();
    });

    this.load();
    this.brands();
    this.categories();
    this.providers();
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  load = () => {
    let form = this.objectWarehouses();
    form = form.filter((el, i) => {
      var wp = this.state.warehouses_product.find(ele => {
        return parseInt(ele.warehouse_id) === parseInt(el.id);
      });
      if (wp) {
        el.stock_min = wp.stock_min;
        el.stock_max = wp.stock_max;
        el.location = wp.location;
        el.percentage_earning = wp.percentage_earning;
        el.percentage_max = wp.percentage_max;
        el.percentage_offer = wp.percentage_offer;
      }
      return el;
    });

    this.setState((state, props) => {
      state.warehouses = props.warehouses;
      state.companies = props.companies;
      state.form = {
        ...state.form,
        warehouses: form
      };
      return state;
    });
    this.category();
    this.providers();
  };

  category = () => {
    let param = {
      role: this.props.user.role,
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id
    };

    Globals.setLoading();
    axios
      .post("admin/products/categories", param)
      .then(res => {
        if (res.data.result) {
          let categories = [];
          res.data.categories.forEach((element, i) => {
            categories.push({
              label: element.name,
              value: element.id,
              second: element.code,
              subcategories: element.subcategories
            });
          });
          this.setState({
            categories: categories
          });
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
        this.brands();
        this.categories();
      });
  };

  objectWarehouses = () => {
    let form = this.props.warehouses.map(el => {
      return {
        id: el.id,
        name: el.name,
        stock_min: "",
        stock_max: "",
        location: "",
        percentage_earning: 0,
        percentage_max: 0,
        percentage_offer: 0
      };
    });
    return form;
  };

  brands = () => {
    let param = {
      role: this.props.user.role,
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id
    };

    axios
      .post("admin/products/brands", param)
      .then(res => {
        if (res.data.result) {
          let brands = [];
          res.data.brands.forEach((element, i) => {
            brands.push({
              label: element.name,
              second: element.code,
              value: element.id,
              models: element.models
            });
          });
          this.setState({
            brands: brands
          });
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(() => Globals.showError())
      .then(() => {
        if (this.props.edit) {
          this.edit();
          return;
        }
        this.models();
        Globals.quitLoading();
      });
  };

  submit = async () => {
    const { form } = this.state;

    let param = form;

    if (param.description == null) {
      param.description = "";
    }

    const warehouseMap = form.warehouses.filter(
      Item =>
        Item.percentage_earning > 0 ||
        Item.percentage_max > 0 ||
        Item.percentage_offer > 0
    );

    if (warehouseMap.length === 0) {
      Globals.showWarning(
        `¡Debe cargar al menos 1 porcentaje en alguno de los almacenes!`
      );

      return;
    }

    param.ware = JSON.stringify(warehouseMap);
    param.prov = JSON.stringify(form.providers);

    this.setState({ submitted: true });
    axios
      .upload(
        this.props.edit ? "admin/products/edit" : "admin/products/create",
        param
      )
      .then(res => {
        if (res.data.result) {
          Globals.showSuccess(res.data.msg);
          this.setState({
            submitted: false,
            form: {
              ...this.state.form,
              brand: "",
              model: "",
              category: "",
              subcategory: "",
              user_id:
                this.props.user.role === 3
                  ? this.props.user.id
                  : this.props.user.enterprise_users.enterprise_id,
              role: this.props.user.role,
              providers: []
            }
          });
          this.props.onClose();
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(err => {
        this.setState({ submitted: false });

        if (err.response.status === 422) {
          Globals.showError(err.response.data.msg);
        } else {
          Globals.showError();
        }
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  providers = () => {
    let param = {
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id
    };
    axios
      .post("admin/products/providers/get", param)
      .then(async res => {
        if (res.data.result) {
          this.setState({
            providers: res.data.providers
          });
        }
      })
      .catch(() => Globals.showError())
      .then(() => Globals.quitLoading());
  };

  addProvider = () => {
    this.setState(state => {
      state.showProvider = this.state.showProvider ? false : true;
      return state;
    });
  };

  assignProvider = () => {
    if (!this.state.form.provider_id) {
      Globals.showError("Debe seleccionar un proveedor para asignarlo");
      return;
    }
    if (!this.state.form.price) {
      Globals.showError("Debe ingresar un precio al proveedor para asignarlo");
      return;
    }
    let exist = this.state.form.providers.find(el => {
      return parseInt(el.provider_id) === parseInt(this.state.form.provider_id);
    });
    if (exist) {
      Globals.showError("Ya ha asignado este proveedor");
      return;
    }
    let newArr = this.state.form.providers;
    newArr.push({
      provider_id: this.state.form.provider_id,
      price: this.state.form.price,
      name: this.state.form.provider_name
    });
    this.setState(state => {
      state.form.providers = newArr;
      state.form.provider_id = "";
      state.form.provider_name = "";
      state.form.price = "";
      return state;
    });
  };

  createProvider = () => {
    let param = {
      name: this.state.form.nameProv,
      code: this.state.form.codeProv,
      phone: this.state.form.phone,
      email: this.state.form.email,
      fiscal_document_type: this.fiscal_docs.document_type,
      fiscal_identification: this.state.form.fiscal_identification,
      time_limit: this.state.form.time_limit
    };
    param.user_id = this.props.user.id;
    if (this.props.user.role === 3) {
      param.enterprise_id = this.props.user.id;
    }
    if (this.props.user.role === 4) {
      param.enterprise_id = this.props.user.enterprise_users.enterprise_id;
    }
    Globals.setLoading();
    axios
      .post("admin/providers/create", param)
      .then(res => {
        if (res.data.result) {
          Globals.showSuccess(res.data.msg);
          this.providers();
          this.setState(state => {
            state.form.nameProv = "";
            state.form.codeProv = "";
            state.form.fiscal_document_type = "";
            state.form.fiscal_identification = "";
            state.form.phone = "";
            state.form.email = "";
            state.showProvider = false;
            return state;
          });
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(err => {
        if (err.response.status === 422) {
          Globals.showError(err.response.data.msg);
          return;
        }
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
        this.fiscal_docs.document_type = "V";
      });
  };

  removeProvider = i => {
    let newArr = this.state.form.providers;
    newArr.splice(i, 1);
    this.setState(state => {
      state.form.providers = newArr;
      return state;
    });
  };

  addSubCategory = () => {
    this.setState(state => {
      state.showSubCateg = this.state.showSubCateg ? false : true;
      return state;
    });
  };

  createSubCategory = () => {
    let param = {
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id,
      category_id: this.state.form.category,
      creator_id: this.props.user.id,
      name: this.state.form.nameSubCateg,
      code: this.state.form.codeSubCateg.toUpperCase()
    };
    Globals.setLoading();
    axios
      .upload("admin/subcategories/create", param)
      .then(res => {
        if (res.data.result) {
          this.category();
          Globals.showSuccess(res.data.msg);
          this.setState(state => {
            state.form.nameSubCateg = "";
            state.form.codeSubCateg = "";
            state.showSubCateg = false;
            return state;
          });
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(() => Globals.showError())
      .then(() => Globals.quitLoading());
  };

  addModel = () => {
    this.setState(state => {
      state.showModel = this.state.showModel ? false : true;
      return state;
    });
  };

  createModel = () => {
    let param = {
      name: this.state.form.nameModel,
      brand_id: this.state.form.brand,
      creator_id: this.props.user.id,
      code: this.state.form.codeModel.toUpperCase()
    };
    Globals.setLoading();
    axios
      .upload("admin/models/create", param)
      .then(res => {
        this.brands();
        Globals.showSuccess(res.data.msg);
        this.setState(state => {
          state.form.nameModel = "";
          state.form.codeModel = "";
          state.showModel = false;
          return state;
        });
      })
      .catch(err => {
        if (err.response.status === 422) {
          Globals.showError(err.response.data.msg);
          return;
        }
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  addCategory = () => {
    this.setState(state => {
      state.showCateg = this.state.showCateg ? false : true;
      return state;
    });
  };

  createCategory = () => {
    let param = {
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id,
      creator_id: this.props.user.id,
      name: this.state.form.nameCateg,
      code: this.state.form.codeCateg.toUpperCase()
    };
    Globals.setLoading();
    axios
      .upload("admin/categories/create", param)
      .then(res => {
        if (res.data.result) {
          this.category();
          Globals.showSuccess(res.data.msg);
          this.setState(state => {
            state.form.nameCateg = "";
            state.form.codeCateg = "";
            state.showCateg = false;
            return state;
          });
          this.categories();
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  addBrand = () => {
    this.setState(state => {
      state.showBrand = this.state.showBrand ? false : true;
      return state;
    });
  };

  createBrand = () => {
    let param = {
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id,
      creator_id: this.props.user.id,
      name: this.state.form.nameBrand,
      code: this.state.form.codeBrand.toUpperCase()
    };
    Globals.setLoading();
    axios
      .upload("admin/brands/create", param)
      .then(res => {
        this.brands();
        Globals.showSuccess(res.data.msg);
        this.setState(state => {
          state.form.nameBrand = "";
          state.form.codeBrand = "";
          state.showBrand = false;
          return state;
        });
      })
      .catch(err => {
        if (err.response.status === 422) {
          Globals.showError(err.response.data.msg);
          return;
        }
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  getCode = () => {
    let code = "";
    if (!this.state.form.edit_code && !this.props.edit) {
      code += this.state.form.categoryCode;
      code += this.state.form.subcategoryCode;
      code += this.state.form.brandCode;
      code += this.state.form.modelCode;
    }
    code += this.state.form.code;

    if (!this.state.form.edit_code && !this.props.edit) {
      let param = {
        user_id:
          this.props.user.role === 3
            ? this.props.user.id
            : this.props.user.enterprise_users.enterprise_id
      };
      axios
        .post("admin/products/code", param)
        .then(res => {
          this.setState(state => {
            state.form.code = "";
            state.form.code += state.form.categoryCode;
            state.form.code += state.form.subcategoryCode;
            state.form.code += state.form.brandCode;
            state.form.code += state.form.modelCode;
            state.form.code += res.data.last;
            return state;
          });
        })
        .catch()
        .then();
    }
    return code;
  };

  getName = () => {
    if (!this.state.form.edit_name && !this.props.edit) {
      // let name = this.state.form.name;
      this.setState(state => {
        state.form.name = "";
        state.form.name += state.form.categoryName + " ";
        state.form.name += state.form.subcategoryName + " ";
        state.form.name += state.form.brandName + " ";
        state.form.name += state.form.modelName + " ";
        return state;
      });
    }
  };

  edit = async () => {
    await this.setState({
      form: {
        ...this.state.form,
        id: this.props.edit.element.id,
        code: this.props.edit.element.code,
        name: this.props.edit.element.name,
        description: this.props.edit.element.description,
        category: this.props.edit.element.product_category
          ? this.props.edit.element.product_category.category_id
          : "",
        subcategory: this.props.edit.element.product_subcategory
          ? this.props.edit.element.product_subcategory.subcategory_id
          : "",
        brand: this.props.edit.element.product_brand
          ? this.props.edit.element.product_brand.brand_id
          : "",
        model: this.props.edit.element.product_model
          ? this.props.edit.element.product_model.model_id
          : "",
        percentage_commission: this.props.edit.element.percentage_commission,
        percentage_earning: this.props.edit.element.percentage_earning,
        exempt: this.props.edit.element.exempt,
        serialization: this.props.edit.element.serialization
      },
      edit: true,
      textButton: "Editar"
    });
    Globals.quitLoading();
    this.models();
    this.categories();
  };

  categories = (name = null) => {
    const { form, categories } = this.state;

    const categoryMap = categories.find(
      Item => Item.value === parseInt(form.category)
    );

    if (categoryMap && categoryMap.subcategories) {
      const subcategoriesMap = categoryMap.subcategories.map(
        ({ name, code, id }) => ({
          label: name,
          second: code,
          element: code,
          value: id
        })
      );

      this.setState({
        subcategories: subcategoriesMap
      });
    }

    if (name === "category") {
      this.setState(
        state => {
          state.form = {
            ...state.form,
            subcategory: ""
          };
          return state;
        },
        () => {
          this.getCode();
          this.getName();
        }
      );
    }
    if (name === "subcategory") {
      this.getCode();
      this.getName();
    }
  };

  models = (name = null) => {
    const { form, brands } = this.state;

    const brandMap = brands.find(Item => Item.value === parseInt(form.brand));

    if (brandMap && brandMap.models) {
      const modelsMap = brandMap.models.map(({ name, code, id }) => ({
        label: name,
        second: code,
        value: id
      }));

      this.setState({ models: modelsMap });
    }

    if (name === "brand") {
      this.setState(
        state => {
          state.form.model = "";
          return state;
        },
        () => {
          this.getCode();
          this.getName();
        }
      );
    }

    if (name === "model") {
      this.getCode();
      this.getName();
    }
  };

  setSelectedOption = async (value, key) => {
    this.setState({
      form: {
        ...this.state.form,
        [key]: value
      }
    });
    if (key === "fiscal_document_type") this.fiscal_docs.document_type = value;
  };

  change = async (e, i) => {
    let nam = e.target.name;
    if (
      nam === "exempt" ||
      nam === "serialization" ||
      nam === "edit_code" ||
      nam === "edit_name"
    ) {
      let target = e.target;
      let val = target.checked ? 1 : 0;
      this.setState(
        {
          form: {
            ...this.state.form,
            [nam]: val
          }
        },
        () => {
          if (nam.toString() === "edit_code" && !val) {
            this.getCode();
          } else if (nam.toString() === "edit_name" && !val) {
            this.getName();
          }
        }
      );
    } else if (nam === "provider_id") {
      let val = e.target.value;
      let name = "";
      for (let i = 0; i < e.target.options.length; i++) {
        if (e.target.options[i].value === val && val) {
          name = e.target.options[i].text;
        }
      }
      this.setState(state => {
        state.form.provider_id = val;
        state.form.provider_name = name;
        return state;
      });
    } else {
      let name = nam,
        second = null,
        text = null;

      let valid =
        name === "subcategory" ||
        name === "category" ||
        name === "brand" ||
        name === "model";

      if (valid) {
        for (let i = 0; i < e.target.options.length; i++) {
          if (e.target.options[i].value === e.target.value && e.target.value) {
            second = e.target.options[i].getAttribute("second");
            text = e.target.options[i].text;
          }
        }
      }

      let val = e.target.value;
      this.setState(
        state => {
          state.form[nam] = val;
          if (valid) {
            state.form[name + "Code"] = second;
            state.form[name + "Name"] = text;
          }
          return state;
        },
        () => {
          this.models(name);
          this.categories(name);
        }
      );
    }
  };

  setWarehouseDetails = (e, key) => {
    const { name, value } = e.target;
    const { form } = this.state;

    let warehouses = [...form.warehouses];

    warehouses[key] = {
      ...warehouses[key],
      [name]: value
    };

    this.setState(state => ({
      form: {
        ...state.form,
        warehouses: warehouses
      }
    }));
  };

  handlePercentage = (values, name, key) => {
    const { floatValue } = values;
    const { form } = this.state;

    let warehouses = [...form.warehouses];

    warehouses[key] = {
      ...warehouses[key],
      [name]: floatValue
    };

    this.setState(state => ({
      form: {
        ...state.form,
        warehouses: warehouses
      }
    }));
  };

  handleValueChange = (values, name) => {
    const { floatValue } = values;

    this.setState(state => ({
      form: {
        ...state.form,
        [name]: floatValue
      }
    }));
  };

  render() {
    const { submitted, warehouses } = this.state;

    return (
      <div className="text-center container-create-edit-user">
        <p className="text-center text-danger">
          <strong>Importante</strong>: El código y el nombre del producto se
          generan automáticamente al configurar la categoría, subcategoría,
          marca y modelo. Al igual que se puede editar de forma manual
          seleccionando el check correspondiente.
        </p>
        {this.props.user.role === 1 ? (
          <div className="row">
            <div className="col-md-12">
              <Select
                color="gray"
                label="Empresa"
                name="company"
                defaultname="Seleccione"
                value={this.state.form.company ? this.state.form.company : ""}
                options={this.state.companies}
                onChange={this.change}
              />
            </div>
          </div>
        ) : (
          ""
        )}
        <div className="row">
          <div className="col-md-6">
            <Input
              color="gray"
              value={this.state.form.code}
              name="code"
              label="Código"
              onChange={this.change}
              disabled={this.state.form.edit_code ? "" : "disabled"}
            />
          </div>
          <div className="col-md-3">
            <CheckBox
              classdiv="mt-4"
              label="Editar Código"
              name="edit_code"
              checked={this.state.form.edit_code}
              value={this.state.form.edit_code ? 1 : 0}
              onChange={this.change}
            />
          </div>
          <div className="col-md-3">
            <CheckBox
              classdiv="mt-4"
              label="Editar Nombre"
              name="edit_name"
              checked={this.state.form.edit_name}
              value={this.state.form.edit_name ? 1 : 0}
              onChange={this.change}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Input
              color="gray"
              value={this.state.form.name}
              name="name"
              label="Nombre"
              onChange={this.change}
              disabled={this.state.form.edit_name ? "" : "disabled"}
            />
          </div>
          <div className="col-md-6">
            <Input
              color="gray"
              value={this.state.form.percentage_commission}
              name="percentage_commission"
              label="Porcentaje de comisión"
              onKeyPress={e => {
                Globals.soloNumeros(e);
              }}
              onChange={this.change}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Textarea
              label="Descripción"
              name="description"
              value={this.state.form.description}
              onChange={this.change}
              rows={3}
            />
          </div>
          <div className="col-md-6">
            <div className="col-md-12">
              <CheckBox
                label="Exento"
                name="exempt"
                checked={this.state.form.exempt}
                value={this.state.form.exempt ? 1 : 0}
                onChange={this.change}
              />
            </div>
            <div className="col-md-12">
              <CheckBox
                label="Serialización"
                name="serialization"
                checked={this.state.form.serialization}
                value={this.state.form.serialization ? 1 : 0}
                onChange={this.change}
              />
            </div>
            <div className="col-md-12">
              <File
                placeholder={
                  this.state.edit ? "Cambiar imagen" : "Agregar Imagen"
                }
                placeholdersuccess="Imagen Agregada"
                showcheck={true.toString()}
                onChange={this.change}
                name="image"
                value={this.state.form.image}
                inputstyle={{
                  display: "contents"
                }}
                className="btn-product"
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3">
            <Select
              color="gray"
              label="Categoría"
              name="category"
              defaultname="Seleccione"
              value={this.state.form.category ? this.state.form.category : ""}
              options={this.state.categories}
              onChange={this.change}
              icon={
                this.props.sidebar.find(({ name }) => name === "Categorías") ? (
                  <Button
                    color="blue"
                    title="Agregar otra"
                    small="true"
                    onClick={() => this.addCategory()}
                  >
                    <Icon name={this.state.showCateg ? "minus" : "plus"} />
                  </Button>
                ) : null
              }
            />
          </div>
          <div className="col-md-3">
            <Select
              color="gray"
              label="Subcategoría"
              name="subcategory"
              defaultname="Seleccione"
              value={
                this.state.form.subcategory ? this.state.form.subcategory : ""
              }
              options={this.state.subcategories}
              onChange={this.change}
              icon={
                this.state.form.category &&
                this.props.sidebar.find(({ name }) => name === "Categorías") ? (
                  <Button
                    color="blue"
                    title="Agregar otra"
                    small="true"
                    onClick={() => this.addSubCategory()}
                  >
                    <Icon name={this.state.showSubCateg ? "minus" : "plus"} />
                  </Button>
                ) : null
              }
            />
          </div>
          <div className="col-md-3">
            <Select
              color="gray"
              label="Marca"
              name="brand"
              defaultname="Seleccione"
              value={this.state.form.brand ? this.state.form.brand : ""}
              options={this.state.brands}
              onChange={this.change}
              icon={
                this.props.sidebar.find(({ name }) => name === "Marcas") ? (
                  <Button
                    color="blue"
                    title="Agregar otra"
                    small="true"
                    onClick={() => this.addBrand()}
                  >
                    <Icon name={this.state.showBrand ? "minus" : "plus"} />
                  </Button>
                ) : null
              }
            />
          </div>
          <div className="col-md-3">
            <Select
              color="gray"
              label="Modelo"
              name="model"
              defaultname="Seleccione"
              value={this.state.form.model ? this.state.form.model : ""}
              options={this.state.models}
              onChange={this.change}
              icon={
                this.state.form.brand &&
                this.props.sidebar.find(({ name }) => name === "Marcas") ? (
                  <Button
                    color="blue"
                    title="Agregar otra"
                    small="true"
                    onClick={() => this.addModel()}
                  >
                    <Icon name={this.state.showModel ? "minus" : "plus"} />
                  </Button>
                ) : null
              }
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-3">
            {this.state.showCateg ? (
              <React.Fragment>
                <div className="col-md-12">
                  <Input
                    color="gray"
                    value={this.state.form.codeCateg.toUpperCase()}
                    name="codeCateg"
                    label="Código de Categoría"
                    onChange={this.change}
                  />
                </div>
                <div className="col-md-12">
                  <Input
                    color="gray"
                    value={this.state.form.nameCateg}
                    name="nameCateg"
                    label="Nombre de Categoría"
                    onChange={this.change}
                  />
                </div>
                <div className="col-md-12">
                  <div id="button">
                    <Button
                      block="true"
                      type="button"
                      onClick={() => this.createCategory()}
                    >
                      {this.state.textButton}
                    </Button>
                  </div>
                </div>
              </React.Fragment>
            ) : null}
          </div>
          <div className="col-md-3">
            {this.state.showSubCateg ? (
              <React.Fragment>
                <div className="col-md-12">
                  <Input
                    color="gray"
                    value={this.state.form.codeSubCateg.toUpperCase()}
                    name="codeSubCateg"
                    label="Código de Subcategoría"
                    onChange={this.change}
                  />
                </div>
                <div className="col-md-12">
                  <Input
                    color="gray"
                    value={this.state.form.nameSubCateg}
                    name="nameSubCateg"
                    label="Nombre de Subcategoría"
                    onChange={this.change}
                  />
                </div>
                <div className="col-md-12">
                  <div id="button">
                    <Button
                      block="true"
                      type="button"
                      onClick={() => this.createSubCategory()}
                    >
                      {this.state.textButton}
                    </Button>
                  </div>
                </div>
              </React.Fragment>
            ) : null}
          </div>
          <div className="col-md-3">
            {this.state.showBrand ? (
              <React.Fragment>
                <div className="col-md-12">
                  <Input
                    color="gray"
                    value={this.state.form.codeBrand.toUpperCase()}
                    name="codeBrand"
                    label="Código de la marca"
                    onChange={this.change}
                  />
                </div>
                <div className="col-md-12">
                  <Input
                    color="gray"
                    value={this.state.form.nameBrand}
                    name="nameBrand"
                    label="Nombre de la marca"
                    onChange={this.change}
                  />
                </div>
                <div className="col-md-12">
                  <div id="button">
                    <Button
                      block="true"
                      type="button"
                      onClick={() => this.createBrand()}
                    >
                      {this.state.textButton}
                    </Button>
                  </div>
                </div>
              </React.Fragment>
            ) : null}
          </div>
          <div className="col-md-3">
            {this.state.showModel ? (
              <React.Fragment>
                <div className="col-md-12">
                  <Input
                    color="gray"
                    value={this.state.form.codeModel.toUpperCase()}
                    name="codeModel"
                    label="Código del modelo"
                    onChange={this.change}
                  />
                </div>
                <div className="col-md-12">
                  <Input
                    color="gray"
                    value={this.state.form.nameModel}
                    name="nameModel"
                    label="Nombre del modelo"
                    onChange={this.change}
                  />
                </div>
                <div className="col-md-12">
                  <div id="button">
                    <Button
                      block="true"
                      type="button"
                      onClick={() => this.createModel()}
                    >
                      {this.state.textButton}
                    </Button>
                  </div>
                </div>
              </React.Fragment>
            ) : null}
          </div>
        </div>
        <div className="dropdown-divider"></div>
        <Title name={"Proveedores del Producto"} />
        <div className="row">
          <div className="col-md-4 mb-1">
            <Select
              color="gray"
              name="provider_id"
              label="Proveedores"
              defaultname="Seleccione"
              disabledfirst={"false"}
              onChange={this.change}
              value={this.state.form.provider_id}
              options={this.state.providers.map(i => {
                return {
                  value: i.id,
                  label: i.name
                };
              })}
              icon={
                this.props.sidebar.find(
                  ({ name }) => name === "Proveedores"
                ) ? (
                  <Button
                    color="blue"
                    title="Agregar otro"
                    small="true"
                    onClick={() => this.addProvider()}
                  >
                    <Icon name={this.state.showProvider ? "minus" : "plus"} />
                  </Button>
                ) : null
              }
            />
            <InputFormat
              color="gray"
              name="price"
              thousandSeparator={true}
              allowNegative={false}
              isNumericString={true}
              decimalScale="2"
              label="Costo del Proveedor"
              maxLength={10}
              onValueChange={values => this.handleValueChange(values, "price")}
              value={this.state.form.price}
            />
            <div id="button">
              <Button
                block="true"
                disabled={
                  this.state.form.price && this.state.form.provider_id
                    ? ""
                    : "disabled"
                }
                type="button"
                onClick={() => this.assignProvider()}
              >
                Asignar
              </Button>
            </div>
          </div>
          <div className="col-md-4 mb-1">
            {this.state.showProvider ? (
              <React.Fragment>
                <div className="row">
                  <div className="col-md-6">
                    <Input
                      color="gray"
                      value={this.state.form.nameProv}
                      name="nameProv"
                      label="Nombre del Proveedor"
                      onChange={this.change}
                    />
                  </div>
                  <div className="col-md-6">
                    <Input
                      color="grahyi7y88yyuy"
                      value={this.state.form.codeProv}
                      name="codeProv"
                      label="Código del proveedor"
                      onChange={this.change}
                    />
                  </div>
                  <div style={{ display: "flex", margin: "auto" }}>
                    <Select
                      color="gray"
                      label="Tipo"
                      name="fiscal_document_type"
                      defaultname="Seleccione"
                      onChange={e =>
                        this.setSelectedOption(e.target.value, e.target.name)
                      }
                      value={
                        this.state.form.fiscal_document_type
                          ? this.state.form.fiscal_document_type
                          : "V"
                      }
                      options={this.state.type_identification}
                    />
                    <Input
                      color="gray"
                      value={this.state.form.fiscal_identification}
                      name="fiscal_identification"
                      label="RIF"
                      onKeyPress={e => {
                        Globals.soloNumeros(e);
                      }}
                      maxLength={
                        this.state.form.document_type === "J" ? 11 : 10
                      }
                      onChange={this.change}
                    />
                  </div>
                  <div className="col-md-6">
                    <Input
                      color="gray"
                      value={this.state.form.phone}
                      name="phone"
                      label="Teléfono"
                      maxLength={11}
                      onKeyPress={e => {
                        Globals.soloNumeros(e);
                      }}
                      onChange={this.change}
                    />
                  </div>
                  <div className="col-md-6">
                    <Input
                      color="gray"
                      value={this.state.form.email}
                      name="email"
                      type="email"
                      label="E-Mail"
                      onChange={this.change}
                    />
                  </div>
                  <div id="button" class="margin-auto text-provider">
                    <Input
                      color="gray"
                      value={this.state.form.time_limit}
                      name="time_limit"
                      type="number"
                      label="Plazo de Credito"
                      onChange={this.change}
                    />
                    <Button
                      block="true"
                      type="button"
                      onClick={() => this.createProvider()}
                    >
                      Crear
                    </Button>
                  </div>
                </div>
              </React.Fragment>
            ) : null}
          </div>
          <div className="col-md-4 mb-1 height-225-scroll">
            <ul className="list-group">
              {this.state.form.providers.map((el, indice) => {
                return (
                  <li
                    key={indice}
                    className="list-group-item d-flex justify-content-between align-items-center"
                  >
                    {el.name}
                    <span
                      className="badge badge-danger badge-pill word-break"
                      onClick={() => {
                        this.removeProvider(indice);
                      }}
                    >
                      X
                    </span>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
        <Title name={"Productos por Almacen"} />
        {warehouses.map((i, index) => {
          return (
            <div key={index} className="row col-md-12 mt-1">
              <div className="row col-md-12 mt-1">
                <div className="col-md-12">
                  <label> {i.name} </label>
                </div>
              </div>
              <div className="row col-md-12 mt-1">
                <div className="col-md-2">
                  <Input
                    color="gray"
                    name="stock_min"
                    i={index}
                    label="Stock Mín"
                    value={
                      this.state.form.warehouses[index].stock_min
                        ? this.state.form.warehouses[index].stock_min
                        : ""
                    }
                    onKeyPress={e => {
                      Globals.soloNumeros(e);
                    }}
                    maxLength={10}
                    onChange={e => {
                      this.setWarehouseDetails(e, index);
                    }}
                  />
                </div>
                <div className="col-md-2">
                  <Input
                    color="gray"
                    value={
                      this.state.form.warehouses[index].stock_max
                        ? this.state.form.warehouses[index].stock_max
                        : ""
                    }
                    name="stock_max"
                    i={index}
                    label="Stock Máx"
                    onKeyPress={e => {
                      Globals.soloNumeros(e);
                    }}
                    maxLength={10}
                    onChange={e => {
                      this.setWarehouseDetails(e, index);
                    }}
                  />
                </div>
                <div className="col-md-2">
                  <Input
                    color="gray"
                    value={
                      this.state.form.warehouses[index].location
                        ? this.state.form.warehouses[index].location
                        : ""
                    }
                    name="location"
                    i={index}
                    label="Ubicación"
                    maxLength={100}
                    onChange={e => {
                      this.setWarehouseDetails(e, index);
                    }}
                  />
                </div>
                <div className="col-md-2">
                  <InputFormat
                    color="gray"
                    label="% Utilidad Min."
                    name="percentage_earning"
                    thousandSeparator={true}
                    allowNegative={false}
                    isNumericString={true}
                    decimalScale="2"
                    maxLength={5}
                    i={index}
                    onValueChange={values =>
                      this.handlePercentage(values, "percentage_earning", index)
                    }
                    value={
                      this.state.form.warehouses[index].percentage_earning
                        ? this.state.form.warehouses[index].percentage_earning
                        : 0
                    }
                  />
                </div>
                <div className="col-md-2">
                  <InputFormat
                    color="gray"
                    label="% Utilidad Max."
                    name="percentage_max"
                    thousandSeparator={true}
                    allowNegative={false}
                    isNumericString={true}
                    decimalScale="2"
                    i={index}
                    maxLength={5}
                    onValueChange={values =>
                      this.handlePercentage(values, "percentage_max", index)
                    }
                    value={
                      this.state.form.warehouses[index].percentage_max
                        ? this.state.form.warehouses[index].percentage_max
                        : 0
                    }
                  />
                </div>
                <div className="col-md-2">
                  <InputFormat
                    color="gray"
                    label="% Utilidad Oferta"
                    name="percentage_offer"
                    thousandSeparator={true}
                    allowNegative={false}
                    isNumericString={true}
                    decimalScale="2"
                    i={index}
                    maxLength={5}
                    onValueChange={values =>
                      this.handlePercentage(values, "percentage_offer", index)
                    }
                    value={
                      this.state.form.warehouses[index].percentage_offer
                        ? this.state.form.warehouses[index].percentage_offer
                        : 0
                    }
                  />
                </div>
              </div>
            </div>
          );
        })}
        <div className="col-md-4 offset-md-4" id="button">
          <Button
            submitted={submitted ? submitted : undefined}
            block="true"
            type="button"
            onClick={() => this.submit()}
          >
            {this.state.textButton}
          </Button>
        </div>
      </div>
    );
  }
}

export default connect(state => {
  return {
    user: state.user,
    sidebar: state.sidebar
  };
})(CreateEditProduct);
