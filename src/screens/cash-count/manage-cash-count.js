import React from "react";
import { Constants, Globals } from "../../utils";
import { Button, Icon, InputFormat, Select } from "../../components";
import { Bank } from "../../services"

class ManageCashCount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            current_amount: props.Item.current_amount,
            form: {
                amount: 0,
                bank_id: '',
                bank_name: '',
                bank_account_id: '',
                bank_account_number: ''
            },
            bank_payments: props.Item.bank_payments,
            banks: this.filterBanks(props.banks, props.Item.method_type),
            bank_accounts: [],
            errors: []
        };
    }

    handleChange = emitter => {
        const { name, value } = emitter.target;
        const { banks, bank_accounts } = this.state
    
        switch (name) {
            case 'bank_id':
                const { label:bankName } = banks.find(bank => parseInt(bank.value) === parseInt(value))
                this.setState(state => ({
                    form: {
                        ...state.form,
                        [name]: parseInt(value),
                        bank_name: bankName,
                        bank_account_id: "",
                        bank_account_number: ""
                    }
                }), () => this.loadBankAccounts());
                break;
            case 'bank_account_id':
                const { label:bankAccountNumer } = bank_accounts.find(bank_account => parseInt(bank_account.value) === parseInt(value))
                this.setState(state => ({
                    form: {
                        ...state.form,
                        [name]: parseInt(value),
                        bank_account_number: bankAccountNumer
                    }
                }));
                break;
            default:
                this.setState(state => ({
                form: {
                    ...state.form,
                    [name]: parseInt(value)
                }
                }));
                break;
        }
    };
    
    handleValueChange = (values, name) => {
        const { floatValue } = values;
    
        this.setState(state => ({
            form: {
                ...state.form,
                [name]: floatValue
            }
        }));
    };

    validateDestiny = form => {

        const { bank_payments, current_amount } = this.state;

        this.setState(state => ({
            ...state,
            errors: []
        }))

        if(!form.amount){
            this.setState(state => ({
                ...state,
               errors: {amount: ['Debe ingresar un monto para continuar']}
            }));
            return false;
        }

        if(form.id){
            const index = bank_payments.findIndex( bank_payment => parseInt(bank_payment.id) === parseInt(form.id))
            const { amount:prevAmount } = bank_payments[index] = form
            if((parseFloat(form.amount) - parseFloat(prevAmount)) > current_amount){
                this.setState(state => ({
                    ...state,
                   errors: {amount: ['El monto a enviar no puede superar el monto restante']}
                }));
                return false;
            }
        } else {
            if(form.amount > current_amount){
                this.setState(state => ({
                    ...state,
                   errors: {amount: ['El monto a enviar no puede superar el monto restante']}
                }));
                return false;
            }
        }

        if(!form.bank_id){
            this.setState(state => ({
                ...state,
               errors: {bank_id: ['Debe seleccionar un banco para continuar']}
            }));
            return false;
        }

        if(!form.bank_account_id){
            this.setState(state => ({
                ...state,
               errors: {bank_account_id: ['Debe seleccionar una cuenta bancaria para continuar']}
            }));
            return false;
        }

        // const paymentBankAccount = bank_payments.find(bank_account => parseInt(bank_account.bank_account_id) === parseInt(form.bank_account_id))
        // if(paymentBankAccount) {
        //     this.setState(state => ({
        //         ...state,
        //        errors: {bank_account_id: ['Esta cuenta bancaria ya esta agregada a la lista']}
        //     }));
        //     return false;
        // }

        return true;
    }

    addDestiny = form => {
        if(this.validateDestiny(form)) {
            const { Item } = this.props
            let { bank_payments } = this.state;
            if(form.id) {
                const index = bank_payments.findIndex( bank_payment => parseInt(bank_payment.id) === parseInt(form.id))
                bank_payments[index] = form
            } else {
                form.id = (bank_payments.length + 1)
                bank_payments = [...bank_payments, form]
            }
            const currentAmount = Item.current_amount - bank_payments.reduce((prev,next) => {
                return prev + parseFloat(parseFloat(next.amount).toFixed(2))
            }, 0);
            console.log(currentAmount, 'currentAmount', Item.current_amount, 'Item.current_amount', form.amount, 'form')
            this.setState(state => ({
                ...state,
                current_amount: currentAmount,
                form: {
                    amount: 0,
                    bank_id: '',
                    bank_name: '',
                    bank_account_id: '',
                    bank_account_number: ''
                },
                bank_payments: bank_payments
            }));
        }

    }

    editDestiny = Item => {
        this.setState(state => ({
            ...state,
            form: {
                ...Item
            }
        }));
    }

    removeDestiny = Index => {
        const { Item } = this.props
        let { bank_payments } = this.state;
        bank_payments = bank_payments.splice(Index, 1);
        const currentAmount = Item.current_amount - bank_payments.reduce((prev,next) => {
            return prev + parseFloat(parseFloat(next.amount).toFixed(2))
        }, 0);
        this.setState(state => ({
            ...state,
            current_amount: currentAmount,
            form: {
                amount: 0,
                bank_id: '',
                bank_name: '',
                bank_account_id: '',
                bank_account_number: ''
            },
            bank_payments: bank_payments
        }));
    }

    handleSubmit = () => {
    
        const { current_amount, submitted, bank_payments } = this.state;
        let { onClose, Item } = this.props;
        
        if (submitted) {
          return;
        }
        if(current_amount > 0) {
            this.setState(state => ({
                ...state,
               errors: {current_amount: ['El Monto Pendiente debe ser procesado en su totalidad']}
            }));
            return false;
        }
    
        // Vaciando los campos de errores al enviar solicitud
        this.setState({ submitted: true, errors: [] });

        Item.bank_payments = bank_payments;
        Item.current_amount = current_amount;
        onClose(Item)
      };

    loadBankAccounts = () => {
        const { Item } = this.props
        const { form } = this.state
        Bank.getBankAccounts({bank_id: form.bank_id})
            .then(response => {
        
                let bankAccountsMapped = [];
        
                if(Item.type && parseInt(Item.type) === Constants.EXPENSES_INGRESS_METHOD_TYPE_LIST.CASH) {
                    bankAccountsMapped = response
                    .filter(bank_account => bank_account.status === Constants.STATUS_ACTIVE)
                    .filter(bank_account => bank_account.type === Constants.TYPE_BANK_ACCOUNTS_LIST.CASH_ONLY)
                    .map((bank_account, key) => {
                        return {
                            value: bank_account.id,
                            label: bank_account.account_number
                        };
                    })
                } else {
                bankAccountsMapped = response
                    .filter(bank_account => bank_account.status === Constants.STATUS_ACTIVE)
                    .map((bank_account, key) => {
                        return {
                            value: bank_account.id,
                            label: bank_account.account_number
                        };
                    })
                }
                
                this.setState(state => ({
                    form: {
                        ...state.form,
                        bank_account_id: ""
                    },
                    bank_accounts: bankAccountsMapped
                }));
        
            })
        .catch(() => Globals.showError());
    }

    filterBanks = (banks, methodType) => {
        let bankMapped = banks;
        if(methodType && parseInt(methodType) === Constants.EXPENSES_INGRESS_METHOD_TYPE_LIST.CASH) {
            bankMapped = banks
            .filter(bank => bank.type === Constants.TYPE_BANKS_LIST.CASH_ONLY)
        } else {
        bankMapped = banks
            .filter(bank => bank.type !== Constants.TYPE_BANKS_LIST.CASH_ONLY)
        }
        return bankMapped
    }

    hasErrorFor(field) {
        return !!this.state.errors[field];
    }
    
    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            return (
            <span className="invalid-feedback my-2 text-left">
                <strong>{this.state.errors[field][0]}</strong>
            </span>
            );
        }
    }

    render() {
        const { banks, current_amount, submitted, form, bank_accounts, bank_payments } = this.state;
    
        return (
            <div className="container-create-edit-payment">
                <div className="row">
                    <div className="col-md">
                        <InputFormat
                            color="gray"
                            name="current_amount"
                            thousandSeparator={true}
                            allowNegative={false}
                            isNumericString={true}
                            decimalScale="2"
                            label="Monto Pendiente"
                            value={current_amount}
                            readOnly={true}
                            disabled={true}
                            error={`${this.hasErrorFor("current_amount") ? "is-invalid" : ""}`}
                            invalidfeedback={this.renderErrorFor("current_amount")}
                            onValueChange={values =>
                                this.handleValueChange(values, "current_amount")
                            }
                        />
                    </div>
                    <div className="col-md">
                        <InputFormat
                            color="gray"
                            name="amount"
                            thousandSeparator={true}
                            allowNegative={false}
                            isNumericString={true}
                            decimalScale="2"
                            label="Monto"
                            value={form.amount}
                            error={`${this.hasErrorFor("amount") ? "is-invalid" : ""}`}
                            invalidfeedback={this.renderErrorFor("amount")}
                            onValueChange={values =>
                                this.handleValueChange(values, "amount")
                            }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md">
                        <Select
                        color="gray"
                        label="Banco Destino"
                        name="bank_id"
                        options={banks}
                        value={form.bank_id}
                        error={`${
                            this.hasErrorFor("bank_id") ? "is-invalid" : ""
                        }`}
                        invalidfeedback={this.renderErrorFor("bank_id")}
                        onChange={this.handleChange}
                        />
                    </div>
                    <div className="col-md">
                        <Select
                        color="gray"
                        label="Cuenta Bancaria"
                        name="bank_account_id"
                        options={bank_accounts}
                        value={form.bank_account_id}
                        error={`${
                            this.hasErrorFor("bank_account_id") ? "is-invalid" : ""
                        }`}
                        invalidfeedback={this.renderErrorFor("bank_account_id")}
                        onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md">
                    {bank_payments.length > 0 && (
                        <table className="table table-bordered table-products table-sm">
                            <thead>
                                <tr>
                                    <th className="text-center">
                                        Banco
                                    </th>
                                    <th className="text-center">
                                        Cuenta Bancaria
                                    </th>
                                    <th className="text-center">
                                        Monto
                                    </th>
                                    <th className="text-center">
                                        Acciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            {bank_payments.map((Item, key) => {
                                return (
                                <tr key={key}>
                                    <td className="text-center">{Item.bank_name}</td>
                                    <td className="text-center">{Item.bank_account_number}</td>
                                    <td className="text-center">{Item.amount}</td>
                                    <td>
                                        <Button
                                            color="primary"
                                            title="Editar"
                                            small="true"
                                            onClick={() => this.editDestiny(Item)}
                                        >
                                            <Icon name="edit" />
                                        </Button>
                                        <Button
                                            color="danger"
                                            title="Eliminar"
                                            small="true"
                                            onClick={() => this.removeDestiny(key)}
                                        >
                                            <Icon name="remove" />
                                        </Button>
                                    </td>
                                </tr>
                                );
                            })}
                            </tbody>
                        </table>
                    )}
                    </div>
                </div>
                <div className="row text-center">
                    { current_amount > 0 && (
                        <div className="col-md">
                            <div id="button">
                                <Button 
                                type="button" 
                                block="true"
                                onClick={() => this.addDestiny(form)}
                                >
                                    Agregar Destino
                                </Button>
                            </div>
                        </div>
                    )}
                    <div className="col-md">
                        <div id="button">
                            <Button
                                submitted={submitted ? submitted : undefined}
                                type="submit"
                                block="true"
                                disabled={submitted}
                                onClick={this.handleSubmit}
                            >
                                Procesar
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        );
      }
}

export default ManageCashCount;