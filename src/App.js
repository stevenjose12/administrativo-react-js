import React from "react";
import { Provider } from "react-redux";
import { store, persistor } from "./store";
import { PersistGate } from "redux-persist/integration/react";
import {
  //BrowserRouter,
  HashRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import { RouteAuth } from "./components";
import { connect } from "react-redux";
import moment from "moment";
import { Globals, Scanner } from "./utils";
//import { emit } from 'jetemit';
import "moment/locale/es";

// Routes
import Login from "./screens/login/login";
//import Home from './screens/home/home';
import RoleEnterprises from "./screens/roles/enterprises/role_enterprises";
import ProductProviders from "./screens/product-providers/product-providers";
import InventoryAdjustment from "./screens/inventory-adjustment/inventory";
import ProductCompound from "./screens/product-compound/product_compound";
import Enterprises from "./screens/enterprises/enterprises";
import Subcategory from "./screens/subcategory/subcategory";
import Currency from "./screens/currencies/currencies";
import Providers from "./screens/providers/providers";
import Warehouse from "./screens/warehouse/warehouse";
import Category from "./screens/category/category";
import Products from "./screens/products/products";
import Branches from "./screens/branches/branches";
import Clients from "./screens/clients/clients";
import Modules from "./screens/modules/modules";
import Brands from "./screens/brands/brands";
import Models from "./screens/models/models";
import Prices from "./screens/prices/prices";
import Order from "./screens/orders/orders";
import Users from "./screens/users/users";
import Conversions from "./screens/conversions/conversions";
import Requests from "./screens/requests/requests";
import Reports from "./screens/reports/reports";
import TransferWarehouses from "./screens/transfer-warehouses/transfer-warehouses";
import DeliveryNotes from "./screens/delivery-notes/delivery-notes";
import Sales from "./screens/sales/sales";
import SalesReport from "./screens/requests/report";
import Movements from "./screens/movements/movements";
import Payments from "./screens/payments/payments";
import Sellers from "./screens/sellers/sellers";
import ReportPayment from "./screens/payments/report";
import Banks from "./screens/banks/banks";
import BankingTransactions from "./screens/banks/banking-transactions/banking-transactions";
import BankAccounts from "./screens/banks/accounts/bank_accounts";
import ExpensesIngress from "./screens/expenses-ingress/expenses-ingress"; 
import ExpensesReportPayments from "./screens/expenses-ingress/report-payments/report";
import DebtsToPay from "./screens/expenses-ingress/debts-to-pay/debts-to-pay";
import CashCount from "./screens/cash-count/cash-count";


// Styles
import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.min.css";
import "./scss/main.scss";

// Scripts
import "jquery/dist/jquery.min.js";
import "bootstrap/dist/js/bootstrap.min.js";
import $ from "jquery";

class _Container extends React.Component {
  constructor(props) {
    super(props);

    moment.locale("es");

    this.state = {
      tooltip: false
    };

    Scanner.init();
  }

  abortController = new AbortController();

  componentDidMount() {
    $(window).focus(() => {
      if (!this.state.tooltip) Globals.hideTooltip();
    });
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  render() {
    const { sidebar, user } = this.props;

    let route = "/login";

    if (user) {
      if (sidebar.length > 0) {
        route = sidebar[0].path;
      }
    }

    return (
      <Router>
        <Redirect path="/" to={route} />
        <Switch>
          <Route exact path="/login" component={Login} />

          {hasPermission(sidebar, "/users") && (
            <RouteAuth exact path="/users" component={Users} />
          )}

          {hasPermission(sidebar, "/enterprises") && (
            <RouteAuth exact path="/enterprises" component={Enterprises} />
          )}

          {hasPermission(sidebar, "/warehouses") && (
            <RouteAuth exact path="/warehouses" component={Warehouse} />
          )}

          {hasPermission(sidebar, "/currencies") && (
            <RouteAuth exact path="/currencies" component={Currency} />
          )}

          {hasPermission(sidebar, "/categories") && (
            <RouteAuth exact path="/categories" component={Category} />
          )}

          {hasPermission(sidebar, "/orders") && (
            <RouteAuth exact path="/orders" component={Order} />
          )}

          {hasPermission(sidebar, "/categories") && (
            <RouteAuth
              exact
              path="/subcategories/:category_id"
              component={props => <Subcategory {...props} />}
            />
          )}

          {hasPermission(sidebar, "/products") && (
            <RouteAuth
              exact
              path="/products/providers/:product_id"
              component={props => <ProductProviders {...props} />}
            />
          )}

          {hasPermission(sidebar, "/users") && (
            <RouteAuth exact path="/users" component={Users} />
          )}

          {hasPermission(sidebar, "/roles_enterprises") && (
            <RouteAuth
              exact
              path="/roles_enterprises"
              component={RoleEnterprises}
            />
          )}

          {hasPermission(sidebar, "/brands") && (
            <RouteAuth exact path="/brands" component={Brands} />
          )}

          {hasPermission(sidebar, "/brands") && (
            <RouteAuth
              exact
              path="/models/:brand_id"
              component={props => <Models {...props} />}
            />
          )}

          {hasPermission(sidebar, "/prices") && (
            <RouteAuth exact path="/prices" component={Prices} />
          )}

          {hasPermission(sidebar, "/clients") && (
            <RouteAuth exact path="/clients" component={Clients} />
          )}

          {hasPermission(sidebar, "/modules") && (
            <RouteAuth exact path="/modules" component={Modules} />
          )}

          {hasPermission(sidebar, "/products") && (
            <RouteAuth exact path="/products" component={Products} />
          )}

          {hasPermission(sidebar, "/branches") && (
            <RouteAuth exact path="/branches" component={Branches} />
          )}

          {hasPermission(sidebar, "/providers") && (
            <RouteAuth exact path="/providers" component={Providers} />
          )}

          {hasPermission(sidebar, "/products-compounds") && (
            <RouteAuth
              exact
              path="/products-compounds"
              component={ProductCompound}
            />
          )}

          {hasPermission(sidebar, "/inventory-adjustment") && (
            <RouteAuth
              exact
              path="/inventory-adjustment"
              component={InventoryAdjustment}
            />
          )}

          {hasPermission(sidebar, "/prices") && (
            <RouteAuth exact path="/prices" component={Prices} />
          )}

          {hasPermission(sidebar, "/conversions") && (
            <RouteAuth exact path="/conversions" component={Conversions} />
          )}

          {hasPermission(sidebar, "/requests") && (
            <RouteAuth exact path="/requests" component={Requests} />
          )}

          {hasPermission(sidebar, "/report-inventory") && (
            <RouteAuth exact path="/report-inventory" component={Reports} />
          )}

          {hasPermission(sidebar, "/transfer-warehouses") && (
            <RouteAuth
              exact
              path="/transfer-warehouses"
              component={TransferWarehouses}
            />
          )}

          {hasPermission(sidebar, "/delivery-notes") && (
            <RouteAuth exact path="/delivery-notes" component={DeliveryNotes} />
          )}
          {hasPermission(sidebar, "/sales") && (
            <RouteAuth exact path="/sales" component={Sales} />
          )}

          {hasPermission(sidebar, "/report-sale") && (
            <RouteAuth exact path="/report-sale" component={SalesReport} />
          )}

          {hasPermission(sidebar, "/report-movement") && (
            <RouteAuth exact path="/report-movement" component={Movements} />
          )}

          {hasPermission(sidebar, "/payments") && (
            <RouteAuth exact path="/payments" component={Payments} />
          )}

          {hasPermission(sidebar, "/sellers") && (
            <RouteAuth exact path="/sellers" component={Sellers} />
          )}

          {hasPermission(sidebar, "/report-payment") && (
            <RouteAuth exact path="/report-payment" component={ReportPayment} />
          )}

          {hasPermission(sidebar, "/banks") && (
            <RouteAuth exact path="/banks" component={Banks} />
          )}

          {hasPermission(sidebar, "/banks") && (
            <RouteAuth
              exact
              path="/bank-accounts/:bank_id"
              component={props => <BankAccounts {...props} />}
            />
          )}

          {hasPermission(sidebar, "/expenses-ingress") && (
            <RouteAuth exact path="/expenses-ingress" component={ExpensesIngress} />
          )}

          {hasPermission(sidebar, "/expenses-ingress") && (
            <RouteAuth exact path="/expenses-payments-report" component={ExpensesReportPayments} />
          )}

          {hasPermission(sidebar, "/debts-to-pay") && (
            <RouteAuth exact path="/debts-to-pay" component={DebtsToPay} />
          )}

          {hasPermission(sidebar, "/banking-transactions") && (
            <RouteAuth exact path="/banking-transactions" component={BankingTransactions} />
          )}

          {hasPermission(sidebar, "/cash-count") && (
            <RouteAuth exact path="/cash-count" component={CashCount} />
          )}

          <Route path="*">
            <Redirect to={route} />
          </Route>
        </Switch>
      </Router>
    );
  }
}

function hasPermission(sidebar, role) {
  return !!sidebar.find(({ path }) => path === role);
}

const NoMatch = () => {
  return (
    <div>
      <h3>
        Pagina no encontrada<code>404</code>
      </h3>
    </div>
  );
};

const Container = connect(state => {
  return {
    support: state.support,
    user: state.user,
    sidebar: state.sidebar
  };
})(_Container);

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Container />
    </PersistGate>
  </Provider>
);

export default App;
