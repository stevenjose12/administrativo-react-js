import React from "react";
import moment from "moment";

import { Textarea, ViewSerial } from "../../components";
import { Globals } from "../../utils";

class View extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      view_serials: false,
      item_serials: null,
      serials: null
    };
  }

  render() {
    const { Item } = this.props;
    const { view_serials, item_serials, serials } = this.state;
    const _Item = Item;

    return (
      <React.Fragment>
        {view_serials && (
          <ViewSerial
            onBack={() => {
              this.setState({
                item_serials: null,
                view_serials: false,
                serials: null
              });
            }}
            product={item_serials}
            serials={serials}
          />
        )}

        {!view_serials && (
          <React.Fragment>
            <div className="row">
              <div className="col-md">
                <div className="form-group">
                  <div className="table-responsive">
                    <table className="table table-bordered">
                      <thead>
                        <tr style={{ whiteSpace: "nowrap" }}>
                          <th>Nro. de Nota de Entrega:</th>
                          <td>{Item.code}</td>
                          <th>Fecha:</th>
                          <td>{parseFormat(Item.date_emission)}</td>
                        </tr>
                        {(Item || {}).request_order && (
                          <tr style={{ whiteSpace: "nowrap" }}>
                            <th>Nro. de Pedido:</th>
                            <td>{Item.request_order.code}</td>
                            <th colSpan="2"></th>
                          </tr>
                        )}
                        <tr>
                          <th>Cliente:</th>
                          <td>
                            {(Item.client.person || {}).first_name ||
                              (Item.client || {}).name}
                          </td>
                          <th>Hora:</th>
                          <td>{timeFormat(Item.date_emission)}</td>
                        </tr>
                        <tr>
                          <th>Rif Cliente:</th>
                          <td>
                            {(Item.client.person || {}).fiscal_identification ||
                              (Item.client.person || {}).identity_document}
                          </td>
                        </tr>
                        <tr>
                          <th>Vendedor:</th>
                          <td>
                            {(Item.seller.person || {}).first_name ||
                              (Item.seller || {}).name}
                          </td>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md">
                <div className="form-group">
                  <div className="table-responsive">
                    <table className="table table-bordered view-delivery">
                      <thead>
                        <tr>
                          <th colSpan="6" className="text-center">
                            PRODUCTOS DE LA NOTA DE ENTREGA
                          </th>
                        </tr>
                        <tr style={{ whiteSpace: "nowrap" }}>
                          <th>Código</th>
                          <th>Nombre</th>
                          <th>Descripción</th>
                          <th>Precio</th>
                          <th>Cantidad</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        {_Item.details.map((Item, key) => {
                          const serials = _Item.operation.filter(
                            i => i.product_warehouse.product_id == Item.id
                          );

                          return (
                            <tr key={key}>
                              <td>
                                <p>{Item.code}</p>
                                {serials.length > 0 && (
                                  <p
                                    className="view-serials"
                                    onClick={() => {
                                      this.setState({
                                        view_serials: true,
                                        item_serials: Item,
                                        serials: serials
                                      });
                                    }}
                                  >
                                    Ver Seriales
                                  </p>
                                )}
                              </td>
                              <td>{Item.name}</td>
                              <td>{Item.description ? Item.description : 'Sin descripción'}</td>
                              <td>{Globals.formatMiles(Amount(Item))}</td>
                              <td>{Item.pivot.quantity}</td>
                              <td>
                                {Globals.formatMiles(Item.pivot.subtotal)}
                              </td>
                            </tr>
                          );
                        })}
                        <tr>
                          <td colSpan="3"></td>
                          <th>Subtotal/Cantidad</th>
                          <td>{getQuantity(Item)}</td>
                          <td>{Globals.formatMiles(getAmount(Item))}</td>
                        </tr>
                        <tr>
                          <td colSpan="4"></td>
                          <th>Descuento</th>
                          <td>
                            {Globals.formatMiles(Item.discount_percentage)}
                          </td>
                        </tr>
                        <tr>
                          <td colSpan="4"></td>
                          <th>Exento</th>
                          <td>{Globals.formatMiles(Item.exempt)}</td>
                        </tr>
                        <tr>
                          <td colSpan="4"></td>
                          <th>Base Imponible</th>
                          <td>{Globals.formatMiles(Item.taxable)}</td>
                        </tr>
                        <tr>
                          <td colSpan="4"></td>
                          <th>IVA</th>
                          <td>{Globals.formatMiles(Item.vat)}</td>
                        </tr>
                        <tr>
                          <td colSpan="4"></td>
                          <th>Total</th>
                          <td>{Globals.formatMiles(Item.total)}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

            <div className="row text-center">
              <div className="col-md">
                <div className="form-group">
                  <Textarea
                    color="gray"
                    value={Item.observations}
                    name="observations"
                    label="OBSERVACIONES"
                    readOnly
                  />
                </div>
              </div>
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

function getQuantity({ details: Products }) {
  return Products.reduce(function(Quantity, Item) {
    return parseInt(Item.pivot.quantity + Quantity);
  }, 0);
}

function getAmount({ details: Products }) {
  return Products.reduce(function(Amount, Item) {
    return parseFloat(Item.pivot.subtotal + Amount);
  }, 0);
}

function Amount({ pivot: Amount }) {
  return parseFloat(Amount.subtotal / Amount.quantity);
}

function parseFormat(Date) {
  return moment(Date).format("DD/MM/YYYY");
}

function timeFormat(Time) {
  return moment(Time).format("HH:mm:ss");
}

export default View;
