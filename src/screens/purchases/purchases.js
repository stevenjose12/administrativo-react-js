import React from "react";
import Menu from "../menu";
import {
  Table,
  Pagination,
  Button,
  Icon,
  Modal,
  Select,
  DatePicker
} from "../../components";
//import { axios, Globals } from "../../utils";
import moment from "moment";
import ViewPurchase from "./view_purchase";

const TABLE_HEADER = ["Código", "Met. de Pago", "Cliente", "Vendedor"];

class Purchases extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      last_page: 1,
      data: [],
      sellers: [],
      user: null,
      view: false,
      form: {
        year: 2019,
        month: "",
        since: "",
        until: "",
        seller: ""
      },
      dates: {
        minDate: "",
        maxDate: ""
      },
      firstLoad: false,
      years: [],
      months: []
    };
  }

  componentDidMount() {
    this.yearsAvailable();
  }

  yearsAvailable = () => {
    let years = [];

    const currentYear = parseInt(moment().format("YYYY"));
    const lastYear = parseInt(
      moment()
        .subtract(4, "years")
        .format("YYYY")
    );

    for (let i = lastYear; i <= currentYear; i++) {
      const month = i;
      years.push({
        label: month,
        value: month
      });
    }

    this.setState(
      {
        years: years,
        form: {
          ...this.state.form,
          year: currentYear
        }
      },
      () => {
        this.monthsAvailable();
      }
    );
  };

  monthsAvailable = () => {
    let months = [];

    if (this.state.form.year === parseInt(moment().format("YYYY"))) {
      const currentMonth = parseInt(moment().format("MM"));
      for (let i = 0 + 1; i <= currentMonth; i++) {
        months.push({
          label: moment(i.toString()).format("MMMM"),
          value: i
        });
      }
    } else {
      months = [
        { value: 1, label: "Enero" },
        { value: 2, label: "Febrero" },
        { value: 3, label: "Marzo" },
        { value: 4, label: "Abril" },
        { value: 5, label: "Mayo" },
        { value: 6, label: "Junio" },
        { value: 7, label: "Julio" },
        { value: 8, label: "Agosto" },
        { value: 9, label: "Septiembre" },
        { value: 10, label: "Octubre" },
        { value: 11, label: "Noviembre" },
        { value: 12, label: "Diciembre" }
      ];
    }

    this.setState(
      {
        months: months,
        form: {
          ...this.state.form,
          month: months[months.length - 1].value
        }
      },
      () => {
        this.daysAvailable();
      }
    );
  };

  daysAvailable = async () => {
    let since = moment([this.state.form.year, this.state.form.month - 1])
      .startOf("month")
      .toDate();
    let until = moment([this.state.form.year, this.state.form.month - 1])
      .endOf("month")
      .toDate();

    this.setState(
      {
        dates: {
          minDate: since,
          maxDate: until
        },
        form: {
          ...this.state.form,
          since: since,
          until: until
        }
      },
      () => {
        this.load();
      }
    );

    // if (this.state.firstLoad) {
    //   this.load();
    //   setTimeout(() => {
    //     this.setState({
    //       firstLoad: false
    //     });
    //   }, 300);
    // }
  };

  load = () => {
    // let promises = [
    //   axios.post("admin/purchases/sellers"),
    //   axios.post("admin/purchases/get?page=" + this.state.page, this.state.form)
    // ];
    // Globals.setLoading();
    // let param = {};
    // axios
    //   .all(promises)
    //   .then(
    //     axios.spread((...args) => {
    //       args.forEach((element, i) => {
    //         if (element.data.result) {
    //           param = {
    //             // ... param,
    //             last_page:
    //               !param.last_page && element.data.purchases
    //                 ? element.data.purchases.last_page
    //                 : null,
    //             data:
    //               !param.data && element.data.purchases
    //                 ? element.data.purchases.data
    //                 : null,
    //             sellers:
    //               !param.sellers && element.data.sellers
    //                 ? element.data.sellers
    //                 : param.sellers
    //           };
    //         }
    //       });
    //     })
    //   )
    //   .catch(err => {
    //     Globals.showError();
    //   })
    //   .then(() => {
    //     if (param.sellers) {
    //       let sellers = [{ label: "Todos", value: "" }];
    //       param.sellers.forEach(element => {
    //         sellers.push({
    //           label: element.person.name + " " + element.person.lastname,
    //           value: element.id
    //         });
    //       });
    //       param = {
    //         ...param,
    //         sellers: sellers
    //       };
    //     }
    //     this.setState({
    //       ...this.state,
    //       ...param
    //     });
    //     Globals.quitLoading();
    //   });
  };

  close = () => {
    this.setState(
      {
        create: false,
        view: false,
        edit: null,
        user: null
      },
      () => {
        this.load();
      }
    );
  };

  handleChange = (emitter, string) => {
    const { name, value } = emitter.target;

    this.setState({
      ...this.state.form,
      [name]: parseInt(value)
    });

    // if (string === "year") this.monthsAvailable();
    // else if (string === "month") this.daysAvailable();
  };

  render() {
    return (
      <Menu history={this.props.history}>
        <Modal
          className="modal-purchase"
          title="Ver Informe de Visita"
          onClose={this.close}
          visible={this.state.view}
        >
          <ViewPurchase purchase={this.state.purchase} />
        </Modal>
        <div id="home">
          <div className="row">
            <div className="col-md-2">
              <Select
                color="white"
                name="year"
                label="Año"
                style={{ textTransform: "capitalize" }}
                defaultname="Todos"
                onChange={emitter => this.handleChange(emitter, "year")}
                value={this.state.form.year}
                options={this.state.years}
              />
            </div>
            <div className="col-md-2">
              <Select
                color="white"
                name="month"
                label="Mes"
                style={{ textTransform: "capitalize" }}
                defaultname="Todos"
                onChange={emitter => this.handleChange(emitter, "month")}
                value={this.state.form.month}
                options={this.state.months}
              />
            </div>
            <div className="col-md-2">
              <DatePicker
                color="white"
                label="Desde"
                minDate={this.state.dates.minDate}
                value={this.state.form.since}
                onChange={value => {
                  this.setState({
                    ...this.state.form,
                    since: value
                  });
                }}
              />
            </div>
            <div className="col-md-2">
              <DatePicker
                color="white"
                label="Hasta"
                maxDate={this.state.dates.maxDate}
                value={this.state.form.until}
                onChange={value => {
                  this.setState({
                    ...this.state.form,
                    until: value
                  });
                }}
              />
            </div>
            <div className="col-md-2">
              <Select
                color="white"
                name="seller"
                label="Vendedores"
                style={{ textTransform: "capitalize" }}
                defaultname="Todos"
                onChange={e => this.change(e, null)}
                value={this.state.form.seller}
                options={this.state.sellers}
              />
            </div>
            <div className="col-md-2">
              <Button className="btn-align-bottom" onClick={this.load}>
                Filtrar
              </Button>
            </div>
          </div>

          <Table
            data={this.state.data.length}
            title="Informe de Visitas"
            header={TABLE_HEADER}
          >
            {this.state.data.map((i, index) => {
              return (
                <tr key={index}>
                  <td>{i.code}</td>
                  <td>{i.method.name}</td>
                  <td>{i.client.name}</td>
                  <td>{i.user.person.name + " " + i.user.person.lastname}</td>
                  <td>
                    <Button
                      title="Ver"
                      small="true"
                      onClick={() => {
                        this.setState({
                          purchase: i,
                          view: true
                        });
                      }}
                    >
                      <Icon name="eye" />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </Table>

          <Pagination
            pages={this.state.last_page}
            active={this.state.page}
            onChange={async page => {
              await this.setState({
                page: page
              });
              this.load();
            }}
          />
        </div>
      </Menu>
    );
  }
}

export default Purchases;
