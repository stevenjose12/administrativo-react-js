import axios from "../../utils/axios";

class CashCount {
    static async getNotProcessedRequests(form) {
      return new Promise((resolve, reject) => {
        axios.post(`admin/cash-count/get`, form).then(
          ({ data }) => {
            // http success
            resolve(data);
          },
          ({ response }) => {
            const { data } = response;
            // http failed
            reject(data);
          }
        );
      });
    }

    static async processCashCount(form) {
      return new Promise((resolve, reject) => {
        axios.post(`admin/cash-count/process`, form).then(
          ({ data }) => {
            // http success
            resolve(data);
          },
          ({ response }) => {
            const { data } = response;
            // http failed
            reject(data);
          }
        );
      });
    }
}

export default CashCount;