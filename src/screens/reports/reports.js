import React from "react";
import { connect } from "react-redux";
import Menu from "../menu";
import {
  Table,
  Pagination,
  Button,
  Input,
  Select,
  CheckBox
} from "../../components";
import { axios, Constants, Globals, Format } from "../../utils";
import { Branch, Warehouse, Category, Brand } from "../../services";
import NumberFormat from "react-number-format";

class Reports extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      thead: [],
      trows: [],
      header: [
        {
          value: "code",
          label: "Código",
          type: 1,
          section: "required",
          order: 0
        },
        {
          value: "name",
          label: "Nombre",
          type: 1,
          section: "required",
          order: 1
        },
        {
          value: "branch",
          label: "Sucursal",
          type: 1,
          section: "required",
          order: 2
        },
        {
          value: "warehouse",
          label: "Almacen",
          type: 1,
          section: "required",
          order: 3
        },
        {
          value: "current_stock",
          label: "Existencia",
          type: 1,
          section: "required",
          order: 4
        },
        {
          value: "committed",
          label: "Comprometido",
          type: 2,
          section: "stock",
          order: 5
        },
        {
          value: "available",
          label: "Disponible",
          type: 3,
          section: "conditional",
          order: 6
        },
        { value: "cost", label: "Costo", type: 2, section: "price", order: 7 },
        {
          value: "price",
          label: "Precio Min.",
          type: 2,
          section: "price",
          order: 8
        },
        {
          value: "price_max",
          label: "Precio Max.",
          type: 2,
          section: "price",
          order: 9
        },
        {
          value: "price_offer",
          label: "Precio Oferta",
          type: 2,
          section: "price",
          order: 10
        },
        {
          value: "total",
          label: "Total",
          type: 3,
          section: "conditional",
          order: 11
        }
      ],
      optionals: [
        { value: "code", label: "Código", show: true },
        { value: "name", label: "Nombre", show: true },
        { value: "branch", label: "Almacen", show: true },
        { value: "warehouse", label: "Almacen", show: true },
        { value: "current_stock", label: "Existencia", show: true },
        { value: "committed", label: "Comprometido", show: false },
        { value: "available", label: "Disponible", show: true },
        { value: "total", label: "Total", show: true },
        { value: "cost", label: "Costo", show: false },
        { value: "price", label: "Precio Min.", show: false },
        { value: "price_max", label: "Precio Max.", show: false },
        { value: "price_offer", label: "Precio Oferta", show: false }
      ],
      page: 1,
      last_page: 1,
      data: [],
      branches: [],
      warehouses: [],
      user: null,
      view: false,
      providers: {},
      form: {
        status: "",
        search: "",
        branch_id: "",
        warehouse_id: "",
        category_id: "",
        subcategory_id: "",
        brand_id: "",
        model_id: "",
        user_id: "",
        code: true,
        name: true,
        current_stock: true,
        total: false,
        price: false,
        price_max: false,
        price_offer: false,
        cost: false,
        committed: false,
        available: false
      },
      categories: [],
      subcategories: [],
      brands: [],
      models: [],
      countPrice: 0,
      user_id:
        this.props.user.role === 4
          ? this.props.user.enterprise_users.enterprise_id
          : this.props.user.id,
      role: this.props.user.role,
      showProductsFilters: true
    };
  }

  abortController = new AbortController();

  componentDidMount() {
    this.load();
    this.getThead();
    this.getBranches();
    this.getCategories();
    this.getBrands();
  }
  // componentDidUpdate(_, prevState) {
  //   if (prevState.form.branch_id !== this.state.form.branch_id && this.state.form.branch_id) {
  //     this.getWarehouses();
  //     this.load();
  //   }
  // }
  componentWillUnmount() {
    this.abortController.abort();
  }

  load = () => {
    this.products();
  };

  products = (resetPage = null) => {
    let { page, form } = this.state;
    const { user } = this.props;

    if (typeof resetPage === "number") {
      page = resetPage;
    }

    form.user_id =
      this.props.user.role === 3
        ? this.props.user.id
        : this.props.user.enterprise_users.enterprise_id;

    if (user.assigned_warehouses.length > 0) {
      if (!form.warehouse_id) {
        form.warehouse_id = user.assigned_warehouses.map(Item => {
          return Item.warehouse_id;
        });
      }
    }
    axios
      .post("admin/report-inventory/get?page=" + page, form)
      .then(res => {
        if (res.data.products_warehouse.last_page > 0) {
          if (res.data.products_warehouse.data.length > 0) {
            this.setState({
              last_page: res.data.products_warehouse.last_page,
              data: res.data.products_warehouse.data,
              page: page
            });
          } else {
            this.products(1);
          }
        } else {
          this.setState({
            last_page: res.data.products_warehouse.last_page,
            data: res.data.products_warehouse.data
          });
        }
      })
      .catch(err => {
        Globals.showError();
      });
  };

  warehouses = () => {
    let param = {
      role: this.props.user.role,
      user_id: this.props.user.id
    };
    axios
      .post("admin/products/warehouses", param)
      .then(res => {
        if (res.data.result) {
          let form = [];
          res.data.warehouses.forEach((el, i) => {
            form.push({
              id: el.id,
              name: el.name,
              stock_min: "",
              stock_max: "",
              price: "",
              location: ""
            });
          });
          this.setState({
            warehouses: form
          });
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  getWarehouses = id => {
    if (!id) {
      let warehouses_filtered = [];
      this.setState(state => ({
        ...state,
        warehouses: warehouses_filtered,
        form: {
          ...state.form,
          warehouse_id: ""
        }
      }));
      return;
    }

    let { user } = this.props;

    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    Warehouse.getWarehouses({
      role: user.role,
      user_id: enterpriseId,
      branch_id: id
    }).then(response => {
      if (response.result) {
        let warehouses_filtered = [];
        if (user.role === 4) {
          warehouses_filtered = response.warehouses.data.filter(el => {
            return user.assigned_warehouses.some(f => {
              return parseInt(f.warehouse_id) === parseInt(el.id);
            });
          });
        } else {
          warehouses_filtered = response.warehouses.data;
        }

        this.setState(state => ({
          warehouses: warehouses_filtered,
          form: {
            ...state.form,
            warehouse_id: ""
          }
        }));
      }
    });
  };

  getCategories = () => {
    let param = {
      role: this.props.user.role,
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id,
      select: true
    };

    const { form } = this.state;

    Globals.setLoading();
    axios
      .post("admin/categories/get", param)
      .then(async res => {
        if (res.data.result) {
          this.setState({
            categories: res.data.categories,
            form: {
              ...form,
              subcategory_id: ""
            }
          });
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  getSubcategories = categoryId => {
    let param = {
      category_id: categoryId,
      select: true
    };
    Category.getSubcategories(param).then(response => {
      if (response.result) {
        this.setState(state => ({
          ...state,
          subcategories: response.subcategories,
          form: {
            ...state.form,
            subcategory_id: ""
          }
        }));
      }
    });
  };

  getBrands = () => {
    let param = {
      role: this.props.user.role,
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id,
      select: true
    };
    const { form } = this.state;
    Brand.getBrands(param).then(response => {
      if (response.result) {
        this.setState({
          brands: response.brands,
          form: {
            ...form,
            model_id: "",
            brand_id: ""
          }
        });
      }
    });
  };

  getModels = brandId => {
    let param = {
      brand_id: brandId,
      select: true,
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id
    };
    const { form } = this.state;
    Brand.getModels(param).then(response => {
      if (response.result) {
        this.setState({
          models: response.models,
          form: {
            ...form,
            model_id: ""
          }
        });
      }
    });
  };

  warehouses = () => {
    let param = {
      role: this.props.user.role,
      user_id:
        this.props.user.role === 3
          ? this.props.user.id
          : this.props.user.enterprise_users.enterprise_id
    };
    axios
      .post("admin/products/warehouses", param)
      .then(res => {
        if (res.data.result) {
          let form = [];
          res.data.warehouses.forEach((el, i) => {
            form.push({
              id: el.id,
              name: el.name,
              stock_min: "",
              stock_max: "",
              price: "",
              location: ""
            });
          });
          this.setState({
            warehouses: form
          });
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };
  openProductsDetails = e => {
    this.change(e);
    if (e.target.value === "")
      this.setState({
        showProductsFilters: false
      });
    else
      this.setState({
        showProductsFilters: true
      });
  };

  change = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  handleCheck = async e => {
    var { name, checked } = e.target;
    let { form, header, trows } = this.state;

    let sectionPrices = header
      .filter(Item => {
        return Item.section === "price";
      })
      .map(Item => {
        return Item.value;
      });

    let sectionStock = header
      .filter(Item => {
        return Item.section === "stock";
      })
      .map(Item => {
        return Item.value;
      });

    if (trows.length === 0) {
      trows = header.filter(Item => {
        return Item.type === 1;
      });
    } else {
      trows = trows.filter(Item => {
        return Item.type;
      });
    }

    let find = header.find(Item => Item.value === name);

    if (sectionPrices.includes(name)) {
      if (!trows.find(Item => Item.value === find.value)) {
        let countSecPrice = trows.filter(Item => {
          return Item.section === "price";
        });

        trows.push(find);

        if (countSecPrice.length === 0) {
          let totalRow = header.find(Item => Item.value === "total");
          totalRow.multiplier = name;
          trows.push(totalRow);
        } else {
          let indexRow = trows.findIndex(Item => Item.value === "total");
          if (indexRow > -1) {
            trows.splice(indexRow, 1);
          }
        }
      } else {
        let index2 = trows.findIndex(Item => Item.value === name);
        trows.splice(index2, 1);

        let countSecPrice = trows.filter(Item => {
          return Item.section === "price";
        });

        if (countSecPrice.length === 1) {
          let totalRow = header.find(Item => Item.value === "total");
          totalRow.multiplier = countSecPrice[0].value;
          trows.push(totalRow);
        } else {
          let indexRow = trows.findIndex(Item => Item.value === "total");
          if (indexRow > -1) {
            trows.splice(indexRow, 1);
          }
        }
      }
    }

    if (sectionStock.includes(name)) {
      if (!trows.find(Item => Item.value === find.value)) {
        let countSecStock = trows.filter(Item => {
          return Item.section === "stock";
        });

        trows.push(find);

        if (countSecStock.length === 0) {
          let totalAvailable = header.find(Item => Item.value === "available");
          trows.push(totalAvailable);
        } else {
          let indexRow = trows.findIndex(Item => Item.value === "available");
          if (indexRow > -1) {
            trows.splice(indexRow, 1);
          }
        }
      } else {
        let index2 = trows.findIndex(Item => Item.value === name);
        trows.splice(index2, 1);

        let countSecStock = trows.filter(Item => {
          return Item.section === "stock";
        });

        if (countSecStock.length === 1) {
          let totalAvailable = header.find(Item => Item.value === "available");
          trows.push(totalAvailable);
        } else {
          let indexRow = trows.findIndex(Item => Item.value === "available");
          if (indexRow > -1) {
            trows.splice(indexRow, 1);
          }
        }
      }
    }

    trows.sort((a, b) => parseInt(a.order) - parseInt(b.order));

    let thead = trows.map(Item => {
      return Item.label;
    });

    this.setState({
      form: {
        ...form,
        [name]: checked
      }
    });

    this.getThead(thead, trows);
    return;
  };

  getThead = async (arr = [], arr2 = []) => {
    const { header } = this.state;

    let newHeader = arr;

    if (newHeader.length === 0) {
      newHeader = header
        .filter(Item => {
          return Item.type === 1;
        })
        .map(Item => {
          return Item.label;
        });
    }

    await this.setState(state => {
      state.thead = newHeader;
      state.trows = arr2;
    });
  };

  getBranches = () => {
    let { user } = this.props;
    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    Branch.getBranchesByEnterprise({ Id: enterpriseId })
    .then(response => {
      let branches_filtered = response.filter(
        ({ deleted_at }) => !deleted_at
      );

      branches_filtered = Format.rawBranches(
        branches_filtered,
        user.assigned_warehouses,
        user.role
      );

      const branchesMap = branches_filtered
        .filter(({ status }) => status === Constants.STATUS_ACTIVE)
        .map(({ id, name }) => ({
          value: id,
          label: name
        }));

      this.setState({
        branches: branchesMap
      });
    })
    .catch(() => Globals.showError());
  };

  returnState = async () => {
    await this.setState(state => ({
      ...state,
      search: '',
      form: {
        status: "",
        search: "",
        branch_id: "",
        warehouse_id: "",
        category_id: "",
        subcategory_id: "",
        brand_id: "",
        model_id: "",
        user_id: ""
      },
      thead: [],
      trows: [],
      optionals: [
        { value: "code", label: "Código", show: true },
        { value: "name", label: "Nombre", show: true },
        { value: "branch", label: "Almacen", show: true },
        { value: "warehouse", label: "Almacen", show: true },
        { value: "current_stock", label: "Existencia", show: true },
        { value: "committed", label: "Comprometido", show: false },
        { value: "available", label: "Disponible", show: true },
        { value: "total", label: "Total", show: true },
        { value: "cost", label: "Costo", show: false },
        { value: "price", label: "Precio Min.", show: false },
        { value: "price_max", label: "Precio Max.", show: false },
        { value: "price_offer", label: "Precio Oferta", show: false }
      ],
    }));
    await this.getThead();
    await this.getBranches();
    await this.getCategories();
    await this.getBrands();
    await this.load();
  };

  render() {
    const { trows } = this.state;

    return (
      <Menu history={this.props.history}>
        <div id="products">
          <div className="row">
            <div className="col-md-2">
              <Select
                color="white"
                name="branch_id"
                label="Sucursales"
                defaultname="Todos"
                disabledFirst="false"
                onChange={async e => {
                  await this.change(e);
                  await this.getWarehouses(this.state.form.branch_id);
                }}
                value={this.state.form.branch_id}
                options={this.state.branches}
              />
            </div>
            <div className="col-md-2">
              <Select
                color="white"
                name="warehouse_id"
                label="Almacenes"
                defaultname="Todos"
                disabledFirst="false"
                onChange={this.openProductsDetails}
                value={this.state.form.warehouse_id}
                options={this.state.warehouses.map(Item => {
                  return {
                    value: Item.id,
                    label: Item.deleted_at
                      ? Item.name + " - Eliminado"
                      : Item.name
                  };
                })}
              />
            </div>
            <div className="col-md-4 placeholder1">
              <Input
                color="white"
                name="search"
                label="Búsqueda"
                onChange={this.change}
                value={this.state.form.search}
                placeholder="Buscar por Nombre o código"
              />
            </div>
            {/*<div className="col-sm-4 d-flex justify-content-between align-items-center">
              {this.state.optionals.map(el => {
                if (!el.show) {
                  return (
                    <div className="col col-sm-4">
                      <CheckBox
                        label={el.label}
                        name={el.value}
                        checked={this.state.form[el.value] ? true : false}
                        value={this.state.form[el.value]}
                        onChange={this.handleCheck}
                      />
                    </div>
                  );
                }
              })}
            </div>*/}
          </div>
          <div className="row">
            <div className="col-md-3">
              <Select
                color="white"
                name="category_id"
                label="Categorias"
                defaultname="Todos"
                disabledFirst="false"
                value={this.state.form.category_id}
                onChange={async e => {
                  await this.change(e);
                  await this.getSubcategories(this.state.form.category_id);
                }}
                options={this.state.categories.map(Item => {
                  return {
                    value: Item.id,
                    label: Item.deleted_at
                      ? Item.name + " - Eliminado"
                      : Item.name
                  };
                })}
              />
            </div>
            <div className="col-md-3">
              <Select
                color="white"
                name="subcategory_id"
                label="Subcategorias"
                defaultname="Todos"
                disabledFirst="false"
                onChange={this.change}
                value={this.state.form.subcategory_id}
                options={this.state.subcategories.map(Item => {
                  return {
                    value: Item.id,
                    label: Item.deleted_at
                      ? Item.name + " - Eliminado"
                      : Item.name
                  };
                })}
              />
            </div>
            <div className="col-md-3">
              <Select
                color="white"
                name="brand_id"
                label="Marcas"
                defaultname="Todos"
                disabledFirst="false"
                onChange={async e => {
                  await this.change(e);
                  await this.getModels(this.state.form.brand_id);
                }}
                value={this.state.form.brand_id}
                options={this.state.brands.map(Item => {
                  return {
                    value: Item.id,
                    label: Item.deleted_at
                      ? Item.name + " - Eliminado"
                      : Item.name
                  };
                })}
              />
            </div>
            <div className="col-md-3">
              <Select
                color="white"
                name="model_id"
                label="Modelo"
                defaultname="Todos"
                disabledFirst="false"
                onChange={this.change}
                value={this.state.form.model_id}
                options={this.state.models.map(Item => {
                  return {
                    value: Item.id,
                    label: Item.deleted_at
                      ? Item.name + " - Eliminado"
                      : Item.name
                  };
                })}
              />
            </div>
          </div>
          <div className="row" id="row1">
            <div className="col-md-2">
              <Button className="btn-align-bottom" onClick={this.load}>
                Filtrar
              </Button>
            </div>
            <div className="col-md-2">
              <Button className="btn-align-bottom" onClick={this.returnState}>
                Limpiar
              </Button>
            </div>

            <div className="col col-md d-flex flex-wrap justify-content-between align-items-center check">
              {this.state.optionals.map(el => {
                if (!el.show) {
                  return (
                    <div className="px-3 py-3 check1">
                      <CheckBox
                        label={el.label}
                        name={el.value}
                        checked={this.state.form[el.value] ? true : false}
                        value={this.state.form[el.value]}
                        onChange={this.handleCheck}
                      />
                    </div>
                  );
                }
              })}
            </div>
          </div>
          <Table
            data={this.state.data.length}
            title="Productos"
            header={this.state.thead}
          >
            {this.state.data.map((i, index) => {
              /* calculo del stock basado en el tipo de ultimo movimiento */
              function stock(x) {
                return x.last_movement.type == 1
                  ? x.last_movement.current_stock + x.last_movement.amount
                  : x.last_movement.current_stock - x.last_movement.amount;
              }
              /* calculo de la disponibilidad basado en la tabla  */
              function disponible(x) {
                let committed = 0;
                if (x.committed_stock) {
                  x.committed_stock.map(Item => {
                    committed += Item.amount;
                  });
                }
                return committed;
              }
              if (i.last_movement)
                return (
                  <tr key={index}>
                    <td>{i.product.code}</td>
                    <td>
                      {i.product.deleted_at
                        ? i.product.name + " Eliminado"
                        : i.product.name}
                    </td>
                    <td>
                      {i.warehouse.branch.deleted_at
                        ? i.warehouse.branch.name + " - Eliminado"
                        : i.warehouse.branch.name}
                    </td>
                    <td>
                      {i.warehouse.deleted_at
                        ? i.warehouse.name + " - Eliminado"
                        : i.warehouse.name}
                    </td>
                    <td>{stock(i)}</td>
                    {trows.map(row => {
                      if (
                        row.value === "committed" ||
                        row.value === "available"
                      ) {
                        return (
                          <td>
                            {row.value === "committed"
                              ? disponible(i)
                              : stock(i) - disponible(i) >= 0
                              ? stock(i) - disponible(i)
                              : 0}
                          </td>
                        );
                      } else if (
                        row.type === 2 ||
                        (row.type === 3 && row.value === "total")
                      ) {
                        return (
                          <td>
                            <NumberFormat
                              value={
                                row.value === "total"
                                  ? parseFloat(
                                      (i[row.multiplier] * stock(i)).toFixed(2)
                                    )
                                  : parseFloat(i[row.value].toFixed(2))
                              }
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                          </td>
                        );
                      }
                    })}
                  </tr>
                );
            })}
          </Table>

          <Pagination
            pages={this.state.last_page}
            active={this.state.page}
            onChange={async page => {
              await this.setState({
                page: page
              });
              this.load();
            }}
          />
        </div>
      </Menu>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(Reports);
