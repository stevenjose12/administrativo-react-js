import axios from "../../utils/axios";

class BankingTransactions {

  static async getBankingTransactions(page, form) {
    return new Promise((resolve, reject) => {
      axios.post(`admin/banking-transactions/get?page=${page}`, form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }

}



export default BankingTransactions;