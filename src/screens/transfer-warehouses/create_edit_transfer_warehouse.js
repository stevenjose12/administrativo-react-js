import React from "react";
import moment from "moment";
import {
  Button,
  CheckBox,
  Icon,
  ViewSerial
} from "../../components";
import { Globals, Colors } from "../../utils";
import { Transfer } from "../../services";
import NumberFormat from "react-number-format";

class CreateEditTransferWarehouse extends React.Component {
    constructor(props) {
		super(props);

		this.state = {
			submitted: false,
			create: props.Item ? false : true,
			form: {
				id: "",
				code: "",
				origin_branch_id: "",
				origin_warehouse_id: "",
				destiny_branch_id: "",
				destiny_warehouse_id: "",
				date_sent: "",
				date_recieved: "",
				enterprise_id: "",
				details: []
			},
			dates: {
				minRecievedDate: moment.now()
			},
			textButton: "Procesar",
			viewSerials: false,
			itemSerials: null
		}
	}
	
	componentDidMount() {
		const { Id } = this.props;
	
		this.setState(state => ({
			form: {
				...state.form,
				creator_id: Id
			}
		}));
	
		if (this.props.Item) {
			const { Item } = this.props;
			this.maybeLoadData(Item);
		}
	}

	maybeLoadData = Item => {
		const { details, serial } = Item;
	
		const rawDetails = arrayTransform(details, serial);
	
		this.setState(state => ({
			form: {
				...state.form,
				id: Item.id,
				enterprise_id: Item.enterprise_id,
				code: Item.code,
				origin_branch_id: Item.origin_warehouse.branch.id,
				origin_warehouse_id: Item.origin_warehouse_id,
				destiny_branch_id: Item.destiny_warehouse.branch.id,
				destiny_warehouse_id: Item.destiny_warehouse_id,
				date_sent: Item.date_sent,
				date_recieved: Item.date_recieved,
				details: rawDetails
			},
			textButton: "Procesar"
		}));
	};

	handleNum = (emitter, key) => {
		const { name, value, type, checked } = emitter.target;
		const { form } = this.state;
	
		let newValue = value ? parseInt(value) : value;
		let rawDetails = form.details;
	
		switch (name) {
			case "change_price":
				newValue = type === 'checkbox' ? checked : value
				rawDetails[key] = {
				  ...rawDetails[key],
				  change_price: newValue
				}
				break;
			default:
				break;
		}
	
		this.setState(state => ({
		  form: {
			...state.form,
			details: rawDetails
		  }
		}));
	};

	handleSubmit = emitter => {
		emitter.preventDefault();
	
		const { form, submitted } = this.state;
		let fireConfirm = false;
		
		if (submitted) {
		  return;
		}

		const findUpdatePrice = form.details.filter(
			Item => !Item.change_price
		);

		if (findUpdatePrice.length > 0) {
			fireConfirm = true;
		}

		this.setState({ submitted: true});
		
		if(!fireConfirm){
			Transfer.processTransfer(form).then(response => {
				const { code } = response;
				Globals.showSuccess(`Traslado Nº ${code} procesado exitosamente!`);
				this.props.onClose();
			})
			.catch(err => {
				Globals.showError(err.msg)
			})
		} else {
			Globals.confirm('Uno o mas productos no tienen seleccionada la opcion de actualizar los precios', () => {
				Transfer.processTransfer(form).then(response => {
					this.setState({
						submitted: false
					});
					const { code } = response;
					Globals.showSuccess(`Traslado Nº ${code} procesado exitosamente!`);
					this.props.onClose();
				})
				.catch(err => {
					Globals.showError(err.msg)
					this.setState({
						submitted: false
					});
				})
				.then(() => {
					this.setState({
						submitted: false
					});
				})
			}, '¿Seguro que desea continuar?')
			this.setState({ submitted: false});
		}
	};

	getStatus = status => {
        let respuesta = "";
        switch (status) {
            case 1:
                respuesta = {
                    text: "Enviado",
                    color: Colors.orange
                };
                break;
        
            case 2:
                respuesta = {
                    text: "Recibido",
                    color: Colors.green
                };
                break;
        
            case 3:
                respuesta = {
                    text: "Anulado",
                    color: Colors.red
                };
                break;
            default:
                break;
        }
        return respuesta;
    };

	render() {

		const { submitted, form, viewSerials, itemSerials } = this.state;
		const { edit, Item } = this.props
	
		return (
			<React.Fragment>
				{viewSerials && (
					<ViewSerial onBack={ () => {
						this.setState({
							viewSerials: false,
						});
					} } 
					product={ itemSerials } 
					serials={ itemSerials.serials } />
				)}
				{ !viewSerials && (
					<React.Fragment>
					<div className="row">
						<div className="col-md">
							<div className="form-group">
								<div className="table-responsive">
									<table className="table table-bordered">.
										<thead>
											<tr style={{ whiteSpace: "nowrap" }}>
													<th>Codigo:</th>
													<td>{Item.code}</td>
													<th>Fecha de Envio:</th>
													<td>{parseFormat(Item.date_sent)}</td>
											</tr>
											<tr>
												<th>Sucursal Origen:</th>
												<td>{Item.origin_warehouse.branch.name}</td>
												<th>Fecha de Recepción:</th>
												<td>{ Item.date_recieved ? parseFormat(Item.date_recieved) : '-'}</td>
											</tr>
											<tr>
												<th>Almacen Origen:</th>
												<td>{Item.origin_warehouse.name}</td>
												<th>Status:</th>
												<td style={{ color: this.getStatus(Item.status).color }}>
													{this.getStatus(Item.status).text}
												</td>
											</tr>
											<tr>
												<th>Sucursal Destino:</th>
												<td>{Item.destiny_warehouse.branch.name}</td>
											</tr>
											<tr>
												<th>Almacen Destino:</th>
												<td>{Item.destiny_warehouse.name}</td>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
					<form onSubmit={this.handleSubmit}>
						{(
						<div className="text-center container-create-edit-order">	
							{form.details.length > 0 && (
							<div className="row">
								<div className="col-md-12">
									<div className="form-group">
										<div className="table-responsive">
										<table className="table table-bordered">
											<thead>
												<tr>
													<td
													colSpan="10"
													>
													PRODUCTOS DEL TRASLADO ENTRE ALMACENES
													</td>
												</tr>
												<tr style={{ whiteSpace: "nowrap" }}>
													<th>Código</th>
													<th>Nombre</th>
													<th>Cant.</th>
													<th>Cost x Ud.</th>
													<th>Acción</th>
												</tr>
											</thead>
											<tbody>
											{form.details.map((Item, key) => {
												return (
												<tr key={key}>
													<td>{Item.code}</td>
													<td>{Item.name}</td>
													<td>{Item.amount}</td>
													<td><NumberFormat value={Item.cost} displayType={"text"} thousandSeparator={true}/></td>	
													{ this.props.edit && <th>
														<div className="col-md-12 col-md-pull-4">
															{edit && (
																<label>
																	Actualizar Precio:
																	<CheckBox
																	name="change_price"
																	checked={Item.change_price}
																	onChange={emitter => {
																		this.handleNum(emitter, key);
																	}}
																	/>
																</label>
															)}
														</div>
													</th> }
													{ Item.serials.length > 0 && <th>
														<Button
															color={"green"}
															small="true"
															title={"Ver Seriales"}
															type="button"
															className="btn-actions-orders"
															onClick={() => {
																this.setState({
																	viewSerials: true,
																	itemSerials: Item
																});
															}}
														>
															<Icon name="list" />
														</Button>
													</th>}
												</tr>
												);
											})}
											</tbody>
										</table>
										</div>
									</div>
								</div>
								<div className="col-md-12">
									<div className="form-group">
										<div className="table-responsive">
											<table className="table table-bordered">
												<thead>
													<tr>
														<td
														colSpan="10"
														>
														OBSERVACIONES
														</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td colSpan="10">{Item.description}</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							)}
				
							<div className="row">
								{edit && (
									<div className="col-md">
										{submitted ? (
										<div className="spinner-border text-primary" role="status">
											<span className="sr-only">Procesando...</span>
										</div>
										) : (
										<div id="button">
											<Button block="true" type="submit" disabled={submitted}>
											{this.state.textButton}
											</Button>
										</div>
										)}
									</div>
								)}
							</div>
						</div>
						)}
					</form>
				</React.Fragment> )}
			</React.Fragment>
		);
	  }
}

function arrayTransform(Items, Serials) {
	return Items.map((Product) => {
		const serials_products = getSerials(Serials);
		Product.serials = serials_products.map((serialProduct) => {
			if(serialProduct.product_id === Product.id) {
				return {serial: serialProduct.serial}
			}
		})
		return {
			id: Product.id,
			pivot_id: Product.pivot.id,
			code: Product.code,
			name: Product.name,
			cost: Product.pivot.cost,
			amount: Product.pivot.amount,
			serials: Product.serials,
			change_price: false
		};
	});
}

function getSerials(Serial) {
	let serials = [];
	if(Serial.length > 0) {
		serials = Serial.map((Item) => {
			if(Item.product_warehouse)
				return {
					serial: Item.serial,
					product_id: Item.product_warehouse.product_id
				}
		})
	}
	return serials;
}

function parseFormat(Date) {
	return moment(Date).format("DD/MM/YYYY");
}

export default CreateEditTransferWarehouse;