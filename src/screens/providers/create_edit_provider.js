import React from "react";
import { connect } from "react-redux";

import { axios, Globals, Constants } from "../../utils";
import { Button, Select, Input, Textarea } from "../../components";

// Components
import SelectCustom from "../../components/select";

class CreateEditProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {},
      textButton: "Crear",
      retention_types: [],
      enterprises: []
    };
  }

  fiscal_docs = {
    document_type: "V",
    document: ""
  };

  componentDidMount() {
    this.setState({
      ...this.state,
      retention_types: this.props.retentions,
      enterprises: this.props.enterprises
    });
    if (this.props.edit) {
      this.edit();
    }
  }

  submit = async () => {
    let param = { ...this.state.form };
    param.user_id = this.props.user.id;
    if (this.props.user.role === 3) {
      param.enterprise_id = this.props.user.id;
    }
    if (this.props.user.role === 4) {
      param.enterprise_id = this.props.user.enterprise_users.enterprise_id;
    }
    Globals.setLoading();
    axios
      .post(
        this.props.edit ? "admin/providers/edit" : "admin/providers/create",
        param
      )
      .then(res => {
        if (res.data.result) {
          this.setState({
            form: {}
          });
          Globals.showSuccess(res.data.msg);
          this.props.onClose();
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(err => {
        if (err.response.status === 422) {
          Globals.showError(err.response.data.msg);
          return;
        }
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  edit = async () => {
    if (this.props.edit.element.identity_document) {
      if (
        !this.props.edit.element.identity_document.indexOf("-") ||
        parseInt(this.props.edit.element.identity_document)
      ) {
        this.ident_docs = {
          document_type: "V",
          document: this.props.edit.element.identity_document
        };
      } else {
        let division = this.props.edit.element.identity_document.split("-");
        this.ident_docs = {
          document_type: division[0],
          document: division[1]
        };
      }
    }
    if (this.props.edit.element.fiscal_identification) {
      if (
        !this.props.edit.element.fiscal_identification.indexOf("-") ||
        parseInt(this.props.edit.element.fiscal_identification)
      ) {
        this.fiscal_docs = {
          document_type: "V",
          document: this.props.edit.element.fiscal_identification
        };
      } else {
        let division = this.props.edit.element.fiscal_identification.split("-");
        this.fiscal_docs = {
          document_type: division[0],
          document: division[1]
        };
      }
    }

    let enterprise_id = "";
    if (this.props.user.role === 3) {
      enterprise_id = this.props.user.id;
    }
    if (this.props.user.role === 4) {
      enterprise_id = this.props.user.enterprise_users.enterprise_id;
    }

    await this.setState({
      form: {
        id: this.props.edit.element.id,
        name: this.props.edit.element.name,
        email: this.props.edit.element.email,
        phone: this.props.edit.element.phone,
        direction: this.props.edit.element.direction,
        code: this.props.edit.element.code,
        fiscal_document_type: this.fiscal_docs.document_type,
        fiscal_identification: this.fiscal_docs.document,
        enterprise_id: enterprise_id,
        person_type: this.props.edit.element.person_type,
        retention_type_id: this.props.edit.element.user_retention_types
          ? this.props.edit.element.user_retention_types.retention_type_id
          : null,
        time_limit: this.props.edit.element.configuration_provider.days_deadline || 0
      },
      textButton: "Editar"
    });
  };

  change = e => {
    this.setState({
      form: {
        fiscal_document_type: this.fiscal_docs.document_type,
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  showSelect = () => {
    var select = null;
    if (this.props.user.role === 1 || this.props.user.role === 2) {
      select = (
        <SelectCustom
          color="gray"
          label="Empresa"
          name="enterprise_id"
          defaultname="Seleccione"
          onChange={this.change}
          value={
            this.state.form.enterprise_id ? this.state.form.enterprise_id : ""
          }
          options={this.state.enterprises.map(el => {
            return {
              value: el.id,
              label: el.name
            };
          })}
        />
      );
    }

    return select;
  };

  setSelectedOption = async (value, key) => {
    this.setState({
      form: {
        ...this.state.form,
        [key]: value
      }
    });
    if (key === "fiscal_document_type") this.fiscal_docs.document_type = value;
  };

  render() {
    return (
      <div className="text-center container-create-edit-user">
        {this.showSelect()}
        <div className="row">
          <div className="col-md-12">
            <Input
              color="gray"
              value={this.state.form.name}
              name="name"
              label="Nombre"
              onChange={this.change}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Input
              color="gray"
              value={this.state.form.code}
              name="code"
              label="Código"
              onChange={this.change}
            />
          </div>
          <div className="col-md-6">
            <div style={{ display: "flex" }}>
              <Select
                color="gray"
                label="Tipo"
                name="fiscal_document_type"
                defaultname="Seleccione"
                onChange={e =>
                  this.setSelectedOption(e.target.value, e.target.name)
                }
                value={
                  this.state.form.fiscal_document_type
                    ? this.state.form.fiscal_document_type
                    : "V"
                }
                options={[
                  {
                    label: "V",
                    value: "V"
                  },
                  {
                    label: "J",
                    value: "J"
                  },
                  {
                    label: "E",
                    value: "E"
                  }
                ]}
              />
              <Input
                color="gray"
                value={this.state.form.fiscal_identification}
                name="fiscal_identification"
                label="RIF"
                onKeyPress={e => {
                  Globals.soloNumeros(e);
                }}
                maxLength={this.state.form.document_type === "J" ? 11 : 10}
                onChange={this.change}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Select
              color="gray"
              label="Tipo de personalidad"
              name="person_type"
              defaultname="Seleccione"
              onChange={e =>
                this.setSelectedOption(e.target.value, e.target.name)
              }
              value={
                this.state.form.person_type ? this.state.form.person_type : ""
              }
              options={Constants.TYPE_PERSON}
            />
          </div>
          <div className="col-md-6">
            <Select
              color="gray"
              label="Tipo de Retención IVA"
              name="retention_type_id"
              defaultname="Seleccione"
              onChange={e =>
                this.setSelectedOption(e.target.value, e.target.name)
              }
              value={
                this.state.form.retention_type_id
                  ? this.state.form.retention_type_id
                  : ""
              }
              options={
                this.state.retention_types
                  ? this.state.retention_types.map(i => {
                      return {
                        value: i.id,
                        label: i.percentage
                      };
                    })
                  : ""
              }
            />
          </div>
        </div>
        <div className="row">
          <div className="col col-md">
            <Textarea
              label="Direccion"
              name="direction"
              value={this.state.form.direction}
              onChange={this.change}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Input
              color="gray"
              value={this.state.form.phone}
              name="phone"
              label="Teléfono"
              maxLength={11}
              onKeyPress={e => {
                Globals.soloNumeros(e);
              }}
              onChange={this.change}
            />
          </div>
          <div className="col-md-6">
            <Input
              color="gray"
              value={this.state.form.email}
              name="email"
              type="email"
              label="E-Mail"
              onChange={this.change}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Input
              type="number"
              color="gray"
              value={this.state.form.time_limit}
              name="time_limit"
              label="Plazo de Credito"
              onChange={this.change}
            />
          </div>
        </div>
        <div id="button">
          <Button block="true" type="button" onClick={() => this.submit()}>
            {this.state.textButton}
          </Button>
        </div>
      </div>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(CreateEditProvider);
