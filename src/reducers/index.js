import { combineReducers } from "redux";
import user from "./user";
import support from "./support";
import sidebar from "./sidebar";
import admin from "./admin";
// import token from "./token";

export default combineReducers({
  user: user,
  sidebar: sidebar,
  admin: admin,
  support: support
  // token: token
});
