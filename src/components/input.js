import React from "react";

const Input = props => (
  <div className="form-group">
    {props.label && <label htmlFor={props.name}>{props.label}</label>}
    <input
      {...props}
      type={props.type ? props.type : "text"}
      className={`form-control${props.size === "small" ? "-sm" : ""} ${
        props.color ? "input-" + props.color : "input-white"
      } ${props.error ? props.error : ""}`}
      name={props.name}
    />
    {props.invalidfeedback}
  </div>
);

export default Input;
