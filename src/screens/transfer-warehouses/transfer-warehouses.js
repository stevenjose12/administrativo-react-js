import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import Menu from "../menu";
import {
  Table,
  Pagination,
  Button,
  Icon,
  Modal,
  Select
} from "../../components";
import { Globals, Format, Colors } from "../../utils";
import { Branch, Warehouse, Transfer } from "../../services";
import CreateEditTransferWarehouse from "./create_edit_transfer_warehouse";

const HEADER_TABLE = [
  "Codigo",
  "Sucursal Origen",
  "Almacen Origen",
  "Sucursal Destino",
  "Almacen Destino",
  "Fecha de Envio",
  "Fecha de Recepcion",
  "Status",
  "Accion"
];

const TYPE_LIST = {
  PENDIENTE: 1,
  PROCESADO: 2,
  ANULADO: 3
};

const StatusList = [
  {
    label: 'Pendiente',
    value: 1
  },
  {
    label: 'Procesado',
    value: 2
  },
  {
    label: 'Anulado',
    value: 3
  },
]

class TransferWarehouses extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      last_page: 1,
      data: [],
      warehouses_origin: [],
      warehouses_destiny: [],
      branches_origin: [],
      branches_destiny: [],
      user: null,
      Item: {},
      visibleProcess: false,
      view: false,
      showLg: true,
      form: {
        user_id: this.props.user.id,
        role: this.props.user.role,
        warehouse_id: Globals.getAssignedWarehouses(props.user),
        status: "",
        search: "",
        origin_branch_id: "",
        destiny_branch_id: "",
        origin_warehouse_id: "",
        destiny_warehouse_id: ""
      }
    };
  }

  componentDidMount() {
    this.load();
  }

  load = () => {
    const { page, form } = this.state;

    Transfer.getTransfers(page, form)
      .then(transfers => {
        this.setState({
          last_page: transfers.last_page,
          data: transfers.data
        });
      })
      .catch(error => {
        Globals.showError();
      })
      .finally(() => {
        this.getBranches();
      });
  };

  close = () => {
    this.setState(
      {
        visibleProcess: false,
        view: false,
        edit: false,
        user: ""
      },
      () => {
        this.load();
      }
    );
  };

  change = emitter => {
    const { name, value } = emitter.target;

    this.setState({
      form: {
        ...this.state.form,
        [name]: value
      }
    });
  };

  getBranches = () => {
    const { user } = this.props;
    const EnterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    Branch.getBranches({
      role: user.role,
      enterprise_id: EnterpriseId
    }).then(({ branches }) => {
      const { data } = branches;

      let branches_filtered = data.filter(function(branch) {
        return !branch.deleted_at;
      });

      // branches_filtered = Format.rawBranches(
      //   data,
      //   user.assigned_warehouses,
      //   user.role
      // );

      this.setState({
        branches_origin: branches_filtered,
        branches_destiny: branches_filtered
      });
    });
  };

  getWarehouses = (id, origin = false) => {
    const { user } = this.props;
    const { form } = this.state;
    const EnterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    const arr = origin ? "warehouses_origin" : "warehouses_destiny";

    if (!id) {
      let warehouses_filtered = [];
      this.setState(state => ({
        ...state,
        [arr]: warehouses_filtered
      }));
      return;
    }

    const name = origin ? "origin_warehouse_id" : "destiny_warehouse_id";
    const inverse = origin ? "destiny_warehouse_id" : "origin_warehouse_id";


    Warehouse.getWarehouses({
      role: user.role,
      user_id: EnterpriseId,
      branch_id: id
    }).then(({ warehouses }) => {
      const { data } = warehouses;

      let warehouses_filtered = data.filter(function(warehouse) {
        return warehouse.id !== parseInt(form[inverse]);
      });

      // warehouses_filtered = Format.rawWarehouse(
      //   warehouses_filtered,
      //   user.assigned_warehouses,
      //   user.role
      // );

      this.setState(state => ({
        ...state,
        [arr]: warehouses_filtered,
        form: {
          ...state.form,
          [name]: ""
        }
      }));
    });
  };

  checkWarehouses = (origin = false) => {
    const { user } = this.props;
    const { form } = this.state;

    if (parseInt(form.origin_branch_id) === parseInt(form.destiny_branch_id)) {
      const filter = origin ? "origin_warehouse_id" : "destiny_warehouse_id";
      const filter2 = origin ? "destiny_warehouse_id" : "origin_warehouse_id";

      const arr = origin ? "warehouses_destiny" : "warehouses_origin";

      Warehouse.getWarehouses({
        role: user.role,
        user_id: user.enterprise_id,
        branch_id: form.origin_branch_id
      }).then(response => {
        if (response.result) {
          let warehouses_filtered = response.warehouses.data.filter(function(
            warehouse
          ) {
            return warehouse.id !== parseInt(form[filter]);
          });
          this.setState(state => ({
            ...state,
            [arr]: warehouses_filtered,
            form: {
              ...state.form,
              [filter2]: form[filter] === form[filter2] ? "" : form[filter2]
            }
          }));
        }
      });
    }
  };

  deployProcessing = Item => {
    this.setState({ Item: Item, visibleProcess: true, edit: true });
  };

  deployDetails = Item => {
    this.setState({ Item: Item, visibleProcess: true, edit: false });
  };

  abortTransfer = Item => {
    Globals.confirm("¿Desea cancelar el traslado: " + Item.code + "?", () => {
      const { form } = this.state;
      Transfer.abortTransfer({ id: Item.id, user_id: form.user_id })
        .then(response => {
          Globals.showSuccess(response.msg);
        })
        .catch(err => {
          Globals.showError(err.msg);
        })
        .then(() => {
          this.load();
        });
    });
  };

  getStatus = status => {
    let respuesta = "";
    switch (status) {
      case TYPE_LIST.PENDIENTE:
        respuesta = {
          text: "Pendiente",
          color: Colors.orange
        };
        break;
      case TYPE_LIST.PROCESADO:
        respuesta = {
          text: "Procesado",
          color: Colors.green
        };
        break;
      case TYPE_LIST.ANULADO:
        respuesta = {
          text: "Anulado",
          color: Colors.red
        };
        break;
    }
    return respuesta;
  };

  render() {
    const {
      visibleProcess,
      Item,
      branches_origin,
      branches_destiny,
      showLg
    } = this.state;

    const { user } = this.props;
    const Id = (user.enterprise_users || {}).enterprise_id || user.id;

    return (
      <Menu history={this.props.history}>
        {visibleProcess && (
          <Modal
            className={showLg ? "modal-delivery" : ""}
            title={"Traslado"}
            onClose={this.close}
            visible
          >
            <CreateEditTransferWarehouse
              edit={this.state.edit}
              onClose={this.close}
              Item={Item}
              User={user}
              Id={Id}
            />
          </Modal>
        )}

        <div id="products">
          {
            <div className="row">
              <div className="col-sm-12 col-md">
                <Select
                  color="white"
                  name="origin_branch_id"
                  label="Sucursales Origen"
                  defaultname="Todos"
                  disabledFirst="false"
                  onChange={async e => {
                    await this.change(e);
                    await this.getWarehouses(
                      this.state.form.origin_branch_id,
                      true
                    );
                  }}
                  value={this.state.form.origin_branch_id}
                  options={branches_origin.map(Item => {
                    return {
                      value: Item.id,
                      label: Item.deleted_at
                        ? Item.name + " - Eliminado"
                        : Item.name
                    };
                  })}
                />
              </div>
              <div className="col-sm-12 col-md">
                <Select
                  color="white"
                  name="origin_warehouse_id"
                  label="Almacenes Origen"
                  defaultname="Todos"
                  disabledFirst="false"
                  onChange={async e => {
                    await this.change(e);
                    await this.checkWarehouses(true);
                  }}
                  value={this.state.form.origin_warehouse_id}
                  options={this.state.warehouses_origin.map(Item => {
                    return {
                      value: Item.id,
                      label: Item.deleted_at
                        ? Item.name + " - Eliminado"
                        : Item.name
                    };
                  })}
                />
              </div>
              <div className="col-sm-12 col-md">
                <Select
                  color="white"
                  name="destiny_branch_id"
                  label="Sucursales Destino"
                  defaultname="Todos"
                  disabledFirst="false"
                  onChange={async e => {
                    await this.change(e);
                    await this.getWarehouses(this.state.form.destiny_branch_id);
                  }}
                  value={this.state.form.destiny_branch_id}
                  options={branches_destiny.map(Item => {
                    return {
                      value: Item.id,
                      label: Item.deleted_at
                        ? Item.name + " - Eliminado"
                        : Item.name
                    };
                  })}
                />
              </div>
              <div className="col-sm-12 col-md">
                <Select
                  color="white"
                  name="destiny_warehouse_id"
                  label="Almacenes Destino"
                  defaultname="Todos"
                  disabledFirst="false"
                  onChange={async e => {
                    await this.change(e);
                    await this.checkWarehouses();
                  }}
                  value={this.state.form.destiny_warehouse_id}
                  options={this.state.warehouses_destiny.map(Item => {
                    return {
                      value: Item.id,
                      label: Item.deleted_at
                        ? Item.name + " - Eliminado"
                        : Item.name
                    };
                  })}
                />
              </div>
              <div className="col-sm-12 col-md">
                <Select
                  color="white"
                  name="status"
                  label="Estatus"
                  defaultname="Todos"
                  disabledFirst="false"
                  onChange={this.change}
                  value={this.state.form.status}
                  options={StatusList}
                />
              </div>
              <div className="col-md-2">
                <Button className="btn-align-bottom" onClick={this.load}>
                  Filtrar
                </Button>
              </div>
            </div>
          }
          <Table
            data={this.state.data.length}
            title="Traslados"
            header={HEADER_TABLE}
          >
            {this.state.data.map((Item, key) => {
              return (
                <tr key={key}>
                  <td>{Item.code}</td>
                  <td>
                    {Item.origin_warehouse.branch.deleted_at
                      ? Item.origin_warehouse.branch.name + " - Eliminado"
                      : Item.origin_warehouse.branch.name}
                  </td>
                  <td>
                    {Item.origin_warehouse.deleted_at
                      ? Item.origin_warehouse.name + " - Eliminado"
                      : Item.origin_warehouse.name}
                  </td>
                  <td>
                    {Item.destiny_warehouse.branch.deleted_at
                      ? Item.destiny_warehouse.branch.name + " - Eliminado"
                      : Item.destiny_warehouse.branch.name}
                  </td>
                  <td>
                    {Item.destiny_warehouse.deleted_at
                      ? Item.destiny_warehouse.name + " - Eliminado"
                      : Item.destiny_warehouse.name}
                  </td>
                  <td>{moment(Item.date_sent).format("DD-MM-YYYY")}</td>
                  <td>
                    {Item.date_recieved
                      ? moment(Item.date_recieved).format("DD-MM-YYYY")
                      : "Sin procesar"}
                  </td>
                  <td style={{ color: this.getStatus(Item.status).color }}>
                    {this.getStatus(Item.status).text}
                  </td>
                  <td>
                    <Button
                      color="primary"
                      title="Ver mas"
                      small="true"
                      onClick={() => this.deployDetails(Item)}
                    >
                      <Icon name="eye" />
                    </Button>
                    {Item.status === 1 && (
                      <Button
                        color="info"
                        title="Procesar"
                        small="true"
                        onClick={() => this.deployProcessing(Item)}
                      >
                        <Icon name="cogs" />
                      </Button>
                    )}
                    {Item.status === 1 && (
                      <Button
                        color="danger"
                        title="Anular"
                        small="true"
                        onClick={() => this.abortTransfer(Item)}
                      >
                        <Icon name="trash" />
                      </Button>
                    )}
                  </td>
                </tr>
              );
            })}
          </Table>

          <Pagination
            pages={this.state.last_page}
            active={this.state.page}
            onChange={page => {
              this.setState(
                {
                  page: page
                },
                () => {
                  this.load();
                }
              );
            }}
          />
        </div>
      </Menu>
    );
  }
}
export default connect(state => {
  return {
    user: state.user
  };
})(TransferWarehouses);
