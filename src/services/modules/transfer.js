import axios from "../../utils/axios";

class Transfer {

    static async getTransfers(page, param) {
        return new Promise((resolve, reject) => {
            axios.post(`admin/transfer-warehouses/get?page=${page}`, param).then(
                ({ data }) => {
                    // http success
                    resolve(data);
                },
                ({ response }) => {
                    const { data } = response;
                    // http failed
                    reject(data);
                }
            );
        });
    }
    
    static async processTransfer(param) {
        return new Promise((resolve, reject) => {
            axios.post(`admin/transfer-warehouses/process`, param).then(
                ({ data }) => {
                    // http success
                    resolve(data);
                },
                ({ response }) => {
                    const { data } = response;
                    // http failed
                    reject(data);
                }
            );
        })
    }

    static async abortTransfer(param) {
        return new Promise((resolve, reject) => {
            axios.post(`admin/transfer-warehouses/abort`, param).then(
                ({ data }) => {
                    // http success
                    resolve(data);
                },
                ({ response }) => {
                    const { data } = response;
                    // http failed
                    reject(data);
                }
            );
        })
    }

}

export default Transfer;