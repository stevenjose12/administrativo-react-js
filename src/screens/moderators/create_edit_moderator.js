import React from 'react';
import {Button, Input } from '../../components';
import File from '../../components/file';
import { axios, Globals } from '../../utils';
import Select from 'react-select'
import SelectCustom from '../../components/select';


class CreateEditModerator extends React.Component{
    state={
        form:{},
        categories:[],
        edit: false,
        textButton: 'Crear'
    }
    componentDidMount(){
        if (this.props.edit) this.edit()
        this.load();
    }

    load = () => {
        Globals.setLoading();
        axios.post('admin/moderators/modules')
            .then(res => {
                if (res.data.result) {
                    let modules = [];
                    res.data.modules.forEach((element, i) => {
                        modules.push({
                            label: element.name,
                            value: element.id
                        })
                    })
                    this.setState({
                        modules: modules
                    })
                }
                else {
                    Globals.showError(res.data.msg);
                }
            })
        .catch(err => {
            Globals.showError();
        })
        .then(() => {
            Globals.quitLoading();
        });
    }
    
    submit = async () => {
        let param = { ...this.state.form }
        if(!this.state.form.document_type) {
            param ={
                ...this.state.form,
                document_type: 'V',
                document: 'V-'+this.state.form.document
            };
        }else if(!this.state.form.document_type.indexOf('-') || this.state.form.document_type){
            param={
                ...this.state.form,
                document: this.state.form.document_type+'-'+this.state.form.document
            }
        }
        if(!param.modules || param.modules.length == 0){
            Globals.showError('Los Administradores requieren al menos 1 permiso');
            return false
        }
        param.modules = JSON.stringify({modules: param.modules});
        Globals.setLoading();
        axios.upload(this.props.edit ? 'admin/moderators/edit' : 'admin/moderators/create', param)
            .then(res => {
                if (res.data.result) {
                    this.setState({
                        form:{}
                    })
                    Globals.showSuccess(res.data.msg)
                    this.props.onClose()
                }
                else {
                    Globals.showError(res.data.msg);
                }
            })
        .catch(err => {
            Globals.showError();
        })
        .then(() => {
            Globals.quitLoading();
        });
    }
    
    edit = async () => {
        let documents = {};
        if(!this.props.edit.element.person.document.indexOf('-') || parseInt(this.props.edit.element.person.document)){
            documents={
                document_type: 'V',
                document: this.props.edit.element.person.document
            }
        }else{
            let division = this.props.edit.element.person.document.split('-');
            documents={
                document_type: division[0],
                document: division[1]
            }
        }
        let permissions = [];
        this.props.edit.element.permissions.forEach(element => {
            permissions.push({
                label: element.name,
                value: element.id,
            })
        });
        this.setState({
            form:{
                name: this.props.edit.element.person.name,
                lastname: this.props.edit.element.person.lastname,
                email: this.props.edit.element.email,
                phone: this.props.edit.element.person.phone,
                document: documents.document,
                document_type: documents.document_type,
                image: this.props.edit.element.person.image,
                modules: permissions,
                id: this.props.edit.element.id,
            },
            edit: true,
            textButton: 'Editar'
        });
    }

    change = e => {
		this.setState({
			form: {
				...this.state.form,
				[e.target.name]: e.target.value
			}
        });
    }
    
    selectChange = async (selectedOption, name) => {
        this.setState({
			form: {
				...this.state.form,
				[name]: selectedOption ? selectedOption : []
			}
        });
    };

    render(){
        return(
            <div className="text-center container-create-edit-user">
                <div className="row">
                    <div className="col-md-6">
                        <Input
                            color="gray"
                            value={this.state.form.name}
                            name="name" 
                            label="Nombre"
                            onChange={this.change} />
                    </div>
                    <div className="col-md-6">
                        <Input
                            color="gray"
                            value={this.state.form.lastname}
                            name="lastname" 
                            label="Apellido"
                            onChange={this.change} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <Input
                            color="gray"
                            value={this.state.form.phone}
                            name="phone" 
                            type="number"
                            label="Teléfono"
                            onChange={this.change} />
                    </div>
                    <div className="col-md-6">
                        <div style={{display: 'flex'}}>
                            <SelectCustom
                                color="gray"
                                label="Tipo"
                                name="document_type"
                                defaultname="document_type"
                                onChange={ this.change }
                                value={ this.state.form.document_type ? this.state.form.document_type : 'V' }
                                options={[
                                    {
                                        label: 'V',
                                        value: 'V'
                                    },
                                    {
                                        label: 'J',
                                        value: 'J'
                                    },
                                    {
                                        label: 'E',
                                        value: 'E'
                                    }
                            ]} />
                            <Input
                                color="gray"
                                value={this.state.form.document}
                                type="number"
                                name="document" 
                                label="Cédula"
                                onChange={this.change} />
                        </div>
                    </div>
                </div>
                <div style={{paddingBottom: '5px'}}>
                    <label>Permisos</label>
                    <Select
                        isMulti
                        color="gray"
                        name="modules"
                        onChange={e => this.selectChange(e, 'modules')}
                        value={ this.state.form.modules }
                        placeholder="Seleccione los modulos para el Administrador"
                        noOptionsMessage={() => 'Sin opciones...'}
                        options={this.state.modules} />
                </div>
                <Input
                    color="gray"
                    value={this.state.form.email}
                    name="email" 
                    label="Email"
                    onChange={this.change} />
                <div className="row">
                    <div className="col-md-6">
                        <Input
                            color="gray"
                            value={this.state.form.password}
                            name="password" 
                            type="password"
                            autoComplete="off"
                            label="Contraseña"
                            onChange={this.change} />
                    </div>
                    <div className="col-md-6">
                        <Input
                            color="gray"
                            value={this.state.form.password_confirmation}
                            name="password_confirmation" 
                            type="password"
                            autoComplete="off"
                            label="Confirmar Contraseña"
                            onChange={this.change} />
                    </div>
                </div>
                <File
                    placeholder={this.state.edit ? 'Cambiar imagen de Perfil (opcional)' : "Agregar Imagen de Perfil (opcional)"}
                    placeholdersuccess={typeof this.state.form.image == 'string' ? 'Cambiar imagen (Opcional)' : "Imagen Agregada"}
                    showcheck={ true.toString() }
                    onChange={ this.change }
                    name="image"
                    value={ this.state.form.image }
                    inputstyle={{
                        display: 'contents'
                    }}
                    className="btn-product" />
                <div id="button">
                    <Button block="true" type="button" onClick={() => this.submit()} >
                        {this.state.textButton}
                    </Button>
                </div>
			</div>
        )
    }
}
export default CreateEditModerator;