import React from "react";

import { Button, Select, Input } from "../../components";

const TYPE_LIST = [
  { value: 0, label: "Todos" },
  { value: 1, label: "Activo" },
  { value: 2, label: "Suspendido" }
];

class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        search: "",
        status: ''
      }
    };
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  render() {
    return (
      <form
        onSubmit={emitter => {
          emitter.preventDefault();
          this.props.onSubmit(this.state.form);
        }}
      >
        <div className="row">
          <div className="col col-md-5">
            <Input
              color="white"
              name="search"
              label="Búsqueda"
              onChange={this.handleChange}
              value={this.state.search}
              placeholder="Buscar por Nombre/Razón social, E-mail o Cédula"
            />
          </div>
          <div className="col col-md-5">
            <Select
              color="white"
              name="status"
              label="Status"
              defaultname="Seleccione"
              onChange={this.handleChange}
              value={this.state.status}
              options={TYPE_LIST.map(({ value, label }) => ({
                value: value,
                label: label
              }))}
            />
          </div>
          <div className="col col-md">
            <Button className="btn-align-bottom" onClick={this.load}>
              Filtrar
            </Button>
          </div>
        </div>
      </form>
    );
  }
}

export default Filter;
