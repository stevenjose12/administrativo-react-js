import React from 'react';
import { Avatar, Icon, List } from '../../components';
import { ENV, Colors } from '../../utils';
import User from '../../assets/img/user.jpg';

class ViewModerator extends React.Component {

	state = {
        user: this.props.data,
        permissions: null,
	}

    componentDidMount(){
        this.load()
    }
    load = () => {
        let permissions = []
        this.state.user.permissions.forEach(element => {
            permissions.push(element.name);
        });
        permissions = permissions.join(' | ');
        this.setState({
            permissions: permissions
        });
    }
	getStatus = (status = this.state.user.status) => {
		let respuesta = "";
		switch(status) {
			case 0:
				respuesta = {
					text: "Nuevo",
					color: Colors.red
				}
				break;
				
			case 1:
				respuesta = {
					text: "Activo",
					color: Colors.green
				}
				break;

			case 2:
				respuesta = {
					text: "Suspendido",
					color: Colors.orange
				}
				break;
		}
		return respuesta;
	}
		
	render() {
		return (
			<div className="text-center container-view-user">
				<Avatar 
					source={ this.state.user.person.image ? ENV.BasePublic  + this.state.user.person.image : User } 
					size="130px" />
				<List.Container>
					<List.Item label="Nombre">
						{ this.state.user.person.name + ' ' + this.state.user.person.lastname }
					</List.Item>
					<List.Item label="Email">
						{ this.state.user.email }
					</List.Item>
					<List.Item label="Cédula de identidad">
						{ this.state.user.person.document }
					</List.Item>
					<List.Item label="Teléfono">
						{ this.state.user.person.phone }
					</List.Item>
					<List.Item label="Permisos">
						{ this.state.permissions }
					</List.Item>
					<List.Item label="Status">
						<span style={ {
							color: this.getStatus().color
						} }>{ this.getStatus().text }</span>
					</List.Item>
				</List.Container>
			</div>
		)
	}
}

export default ViewModerator;