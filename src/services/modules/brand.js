import axios from "../../utils/axios";

class Brand {
  static async getBrands(form) {
    return new Promise((resolve, reject) => {
      axios.post("admin/brands/get", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }

  static async getModels(form) {
    return new Promise((resolve, reject) => {
      axios.post("admin/models/get", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }

  static async createBrand(form) {
    return new Promise((resolve, reject) => {
      axios.post("admin/brands/brand", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }
}

export default Brand;
