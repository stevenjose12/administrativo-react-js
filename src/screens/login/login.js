import React from "react";
import { Card, Input, Button } from "../../components";

import { Globals, Constants, Format } from "../../utils";
import { Auth, Sidebar } from "../../services";

import { connect } from "react-redux";

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      form: {
        name: "",
        password: ""
      }
    };
  }

  componentDidMount() {
    const { user, history } = this.props;

    if (!user) {
      return;
    }

    history.push("/users");
  }

  /**
   * Event that captures form submission.
   * @param {object} event emitted when submitting the form.
   */

  submit = emitter => {
    emitter.preventDefault();

    const { form } = this.state;

    this.setState({ submitted: true });

    Auth.authenticate(form)
      .then(({ user, token }) => {
        const { role: roleId } = user;
        const booleaneVal = Globals.getEvaluation(roleId);
        const rawUser = Format.rawUser(user);
        if(roleId === 4) {
          if (booleaneVal && !rawUser.role_enterprise_subuser) {
            Globals.showWarning(
              "¡Disculpe, su usuario no posee un rol asignado!"
            );
            return;
          }
        }

        this.props.dispatch({
          type: "SET_USER",
          payload: rawUser
        });

        this.props.dispatch({
          type: "SET_ADMIN",
          payload: Globals.setTypeRole(rawUser)
        });

        localStorage.setItem("token", JSON.stringify(token));

        return user;
      })
      .then(user => {
        if (!user) {
          return;
        }

        const { role: roleId } = user;

        Sidebar.getModules(roleId)
          .then(modules => this.setModules(roleId, user, modules))
          .catch(error => console.log({ error: error }))
          .finally(() => this.setState({ submitted: false }));
      })
      .catch(error => {
        const { message } = error;

        this.setState({ submitted: false });

        if ((error || {}).message) {
          Globals.showError(message);
          return;
        }

        Globals.showError();
      })
      .finally(() => this.setState({ submitted: false }));
  };

  /**
   * Set the menu depending on the permissions.
   * @param {int} user role identifier.
   * @param {object} object containing user properties.
   * @param {object} object that contains the module properties.
   */

  setModules = (roleId, user, modules) => {
    const { id: userId } = user;

    switch (roleId) {
      case Constants.ROLE_SUPER:
        this.props.dispatch({
          type: "SET_SIDEBAR",
          payload: modules
        });

        this.props.history.push("/users");

        return;

      case Constants.ROLE_ADMIN:
        this.props.dispatch({
          type: "SET_SIDEBAR",
          payload: modules
        });

        this.props.history.push("/enterprises");

        return;

      default:
        Sidebar.getModuleByUser(userId).then(response => {
          let Menus = [];

          const {
            modules_seller,
            enterprise_modules,
            role_enterprise_subuser
          } = response;

          let array_modules =
            roleId === Constants.ROLE_SELLER
              ? modules_seller
              : enterprise_modules;

          if (roleId === Constants.ROLE_USER) {
            array_modules = role_enterprise_subuser.role_modules;
          }

          if (roleId === Constants.ROLE_USER) {
            modules.forEach(module => {
              const { id } = module;
              array_modules.forEach(({ module_id: moduleId }) => {
                if (moduleId === id) {
                  Menus.push(module);
                }
              });
            });
          } else {
            modules.forEach(module => {
              const { id: moduleId } = module;
              array_modules.forEach(({ id }) => {
                if (id === moduleId) {
                  Menus.push(module);
                }
              });
            });
          }

          this.props.dispatch({
            type: "SET_SIDEBAR",
            payload: Menus
          });

          this.props.history.push(Menus[0].path);
        });
    }
  };

  handleDisabled = () => {
    return this.state.form.email === "" || this.state.form.password === "";
  };

  handleChange = ({ target }) => {
    const { name, value } = target;

    this.setState(state => ({
      form: {
        ...state.form,
        [name]: value
      }
    }));
  };

  render() {
    const { submitted } = this.state;

    return (
      <div id="login">
        <Card>
          <form onSubmit={this.submit}>
            <div className="container-icon">
              <i className="fa fa-user"></i>
            </div>
            <div className="row">
              <div className="col col-md">
                <Input
                  value={this.state.name}
                  name="name"
                  placeholder="Código y Nombre de Usuario (Ej: COD-username)"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-md">
                <Input
                  autoComplete="off"
                  value={this.state.password}
                  type="password"
                  name="password"
                  placeholder="Contraseña"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row text-center">
              <div className="col-md">
                <Button
                  submitted={submitted ? submitted : undefined}
                  block="true"
                  type="submit"
                  disabled={this.handleDisabled()}
                >
                  Iniciar Sesión
                </Button>
              </div>
            </div>
          </form>
        </Card>
      </div>
    );
  }
}

export default connect(state => {
  return {
    user: state.user,
    admin: state.admin
  };
})(Login);
