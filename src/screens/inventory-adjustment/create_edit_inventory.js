import React from "react";
import { connect } from "react-redux";

import { Globals, Constants, Format } from "../../utils";
import { Warehouse, Inventory } from "../../services";

import {
  Button,
  Select,
  Input,
  InputFormat,
  Textarea,
  Icon,
  CheckBox,
  ModalScan
} from "../../components";

import NumberFormat from "react-number-format";

class CreateEditInventoryAdjustment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      search: "",
      form: {
        branch_id: "",
        warehouse_id: "",
        type: 1,
        description: "",
        products: [],
        warehouse_destination_id: "",
        branch_destination_id: "",
        enterprise_id: props.Id,
        creator_id: props.user.id,
        final_cost: 0
      },
      branches: props.branches,
      warehouses: [],
      branches_destination: props.branches,
      warehouses_destination: [],
      suggestions: [],
      errors: [],
      create: true,
      showTranslate: false,
      textButton: "Guardar",
      edit_serial: false,
      item_serial: null
    };
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.form.branch_id !== this.state.form.branch_id &&
      this.state.form.branch_id
    ) {
      this.maybeLoadWarehouses();
    }
    if (
      prevState.form.branch_destination_id !==
        this.state.form.branch_destination_id &&
      this.state.form.branch_destination_id
    ) {
      this.maybeLoadWarehousesDestination(this.state.form.warehouse_id);
    }
  }

  handleSubmit = emitter => {
    emitter.preventDefault();

    const { form, submitted, create } = this.state;
    let fireConfirm = false;

    if (submitted) {
      return;
    }

    // Filtrando productos para valores nulos
    const findValuesNull = form.products.filter(
      Item => Item.quantity <= 0 || Item.quantity === ""
    );

    // Filtrando productos sin costo cargado
    const findCostNull = form.products.filter(
      Item => Item.cost <= 0 || Item.cost === ""
    );

    // Verificando si se seleccione actualizar los precios de los productos
    const findUpdatePrice = form.products.filter(Item => !Item.change_price);

    if (form.products.length === 0) {
      Globals.showError("¡La operación debe tener productos agregados!");
      return;
    } else if (findValuesNull.length > 0) {
      Globals.showError("¡Los productos deben tener un valor mayor a 0!");
      return;
    } else if (findCostNull.length > 0) {
      Globals.showError("¡Los productos deben tener un costo mayor a 0!");
      return;
    } else if (
      findUpdatePrice.length > 0 &&
      ((parseInt(form.type) === 3 &&
        parseInt(form.branch_destination_id) === parseInt(form.branch_id)) ||
        parseInt(form.type) === 1)
    ) {
      fireConfirm = true;
    }

    // Validando si hay productos por serializar
    const cant = form.products.filter(
      i =>
        parseInt(i.serialization) === Constants.SERIALIZATION.ACTIVE && i.serials == null
    ).length;
    if (cant > 0) {
      Globals.showError("¡Hay productos que necesitan serializarse!");
      return;
    }

    // Vaciando los campos de errores al enviar solicitud
    this.setState({ submitted: true, errors: [] });

    switch (create) {
      case true:
        if (!fireConfirm) {
          Inventory.createMovement(form)
            .then(response => {
              Globals.showSuccess("¡Movimiento creado exitosamente!");
              this.props.onClose();
            })
            .catch(error => {
              if (error.hasOwnProperty("msg")) {
                Globals.showError(error.msg);
              }
              this.setState({
                submitted: false,
                errors: error
              });
            });
        } else {
          Globals.confirm(
            "¡Uno o mas productos no tienen seleccionada la opción de actualizar los precios!",
            () => {
              Inventory.createMovement(form)
                .then(response => {
                  Globals.showSuccess("¡Movimiento creado exitosamente!");
                  this.props.onClose();
                })
                .catch(error => {
                  if (error.hasOwnProperty("msg")) {
                    Globals.showError(error.msg);
                  }
                  this.setState({
                    submitted: false,
                    errors: error
                  });
                });
            },
            "¿Seguro que desea continuar?"
          );
          this.setState({ submitted: false, errors: [] });
        }
        break;
      case false:
        break;
      default:
        break;
    }
  };

  AutoComplete = ({ search }) => {
    const { products } = this.props;
    const { form } = this.state;

    let finalProducts = [];

    if (!search) {
      this.setState({ suggestions: [] });
      return;
    }

    let searchLowerCase = search.toLowerCase();

    const filterProducts = products.filter(Item => {
      let nameLowerCase = Item.name.toLowerCase();

      switch (parseInt(form.type)) {
        case 1:
          return (
            nameLowerCase.indexOf(searchLowerCase) !== -1 &&
            parseInt(this.state.form.warehouse_id) === Item.warehouse_id
          );
        case 2:
          return (
            nameLowerCase.indexOf(searchLowerCase) !== -1 &&
            parseInt(this.state.form.warehouse_id) === Item.warehouse_id &&
            Item.last_movement > 0
          );
        case 3:
          return (
            nameLowerCase.indexOf(searchLowerCase) !== -1 &&
            parseInt(this.state.form.warehouse_id) === Item.warehouse_id &&
            Item.last_movement > 0
          );
        default:
          return Item;
      }
    });

    const notChosenProducts = filterProducts.filter(el => {
      return form.products.every(f => {
        return parseInt(f.id) !== parseInt(el.id);
      });
    });

    if (form.products.length > 0) {
      finalProducts = notChosenProducts;
    } else {
      finalProducts = filterProducts;
    }

    this.setState({ suggestions: finalProducts });
  };

  handleChange = emitter => {
    const { name, value } = emitter.target;

    switch (name) {
      case "search":
        this.setState({ [name]: value }, () => {
          this.AutoComplete(this.state);
        });
        break;
      case "type":
        this.setState(state => {
          state.showTranslate = parseInt(value) === 3;
          state.form = {
            ...state.form,
            [name]: value
          };
          return state;
        });
        break;
      case "warehouse_id":
        if (this.state.form.products.length > 0) {
          Globals.sure(
            "¡Perderá su información de productos al cambiar de almacén! ¿Desea cambiar de almacén?",
            confirm => {
              if (confirm.value) {
                this.setState(state => ({
                  form: {
                    ...state.form,
                    products: []
                  },
                  suggestions: [],
                  search: ''
                }));
              }
            }
          );
        } else {
          this.setState(state => ({
            form: {
              ...state.form,
              [name]: value
            },
            suggestions: [],
            search: ''
          }));
        }
        break;
      case "branch_id":
        if (this.state.form.products.length > 0) {
          Globals.sure(
            "¡Perderá su información de productos al cambiar de sucursal! ¿Desea cambiar de sucursal?",
            confirm => {
              if (confirm.value) {
                this.setState(state => ({
                  form: {
                    ...state.form,
                    products: []
                  },
                  suggestions: [],
                  search: ''
                }));
              }
            }
          );
        } else {
          this.setState(state => ({
            form: {
              ...state.form,
              [name]: value
            },
            suggestions: [],
            search: ''
          }));
        }
        break;
      default:
        this.setState(state => ({
          form: {
            ...state.form,
            [name]: value
          }
        }));
        break;
    }
  };

  maybeLoadWarehouses = () => {
    const { form } = this.state;
    const { user } = this.props;

    const Id = user.role === 4 ? user.enterprise_users.enterprise_id : user.id;

    Warehouse.getWarehouseByBranch({
      user_id: Id,
      branch_id: form.branch_id
    }).then(response => {

      const warehouses_filtered = response.filter(
        ({ deleted_at }) => !deleted_at
      );


      const warehousesMap = Format.rawWarehouse(
        warehouses_filtered,
        user.assigned_warehouses,
        user.role
      )
        .filter(({ status }) => status === Constants.STATUS_ACTIVE)
        .map(({ id, name }) => ({
          value: id,
          label: name
        }));

      this.setState(state => ({
        form: {
          ...state.form,
          warehouse_id: ""
        },
        warehouses: warehousesMap
      }));
    });
  };

  maybeLoadWarehousesDestination = origin_warehouse_id => {
    const { form } = this.state;
    const { user } = this.props;

    const Id = user.role === 4 ? user.enterprise_users.enterprise_id : user.id;

    Warehouse.getWarehouseByBranch({
      user_id: Id,
      branch_id: form.branch_destination_id
    }).then(response => {
      let warehouses_destination = response.filter(
        Item => Item.id !== parseInt(origin_warehouse_id)
      );
      this.setState(state => ({
        form: {
          ...state.form,
          warehouse_destination_id: ""
        },
        warehouses_destination: warehouses_destination
      }));
    });
  };

  handleNum = (emitter, key) => {
    const { name, value, type, checked } = emitter.target;
    const { form } = this.state;

    let newValue = value ? parseInt(value) : value;
    let rawProducts = form.products;
    let quantityValue;
    let cost_total = 0;
    let text_Type = "descargar";
    if (parseInt(form.type) === 3) {
      text_Type = "trasladar";
    }
    switch (name) {
      case "quantity":
        quantityValue =
          newValue > 0 && newValue !== ""
            ? parseInt(form.type) === Constants.TYPE_ADJUSTMENT[0].value
              ? rawProducts[key].last_movement + newValue
              : rawProducts[key].last_movement - newValue
            : 0;
        cost_total = newValue * rawProducts[key]["cost"];

        if (Math.sign(quantityValue) === -1) {
          Globals.showError(
            // `La cantidad a descargar supera el stock actual del producto: ${rawProducts[key].name}`
            `La cantidad a ${text_Type} de este producto supera su stock actual`
          );
          return;
        }

        rawProducts[key] = {
          ...rawProducts[key],
          quantity: newValue,
          stock: quantityValue,
          cost_total: cost_total
        };
        break;
      case "cost":
        if (Math.sign(quantityValue) === -1) {
          Globals.showError(
            `El costo del producto debe ser mayor a cero para el producto: ${rawProducts[key].name}`
          );
          return;
        }

        cost_total = newValue * rawProducts[key]["quantity"];

        rawProducts[key] = {
          ...rawProducts[key],
          cost: newValue,
          cost_total: cost_total
        };
        break;
      case "change_price":
        const set = type === "checkbox" ? checked : value;
        rawProducts[key] = {
          ...rawProducts[key],
          change_price: set
        };
        break;
      default:
        break;
    }

    this.setState(state => ({
      form: {
        ...state.form,
        products: rawProducts
      }
    }));
  };

  getTotalCost = () => {
    const { form } = this.state;

    const cost = form.products.reduce((count, product) => {
      return count + product.cost_total;
    }, 0);

    this.setState(state => ({
      form: {
        ...state.form,
        final_cost: cost
      }
    }));
  };

  handleValueChange = (values, key) => {
    const { floatValue } = values;
    const { form } = this.state;

    let rawProducts = form.products;
    let cost_total = floatValue * rawProducts[key]["quantity"];

    rawProducts[key] = {
      ...rawProducts[key],
      cost: floatValue,
      cost_total: cost_total
    };

    this.setState(
      state => ({
        form: {
          ...state.form,
          products: rawProducts
        }
      }),
      () => this.getTotalCost()
    );
  };

  takeProduct = Product => {
    const { form } = this.state;
    const exists = this.state.form.products.find(Item => {
      return Item.id === Product.id;
    });

    if (Product.quantity == "") {
      Product.quantity = 0;
    }

    if (!exists) {
      if (
        Product.stock_min > Product.last_movement &&
        (parseInt(form.type) === 2 || parseInt(form.type) === 3)
      ) {
        Globals.showInformation(
          "El producto seleccionado se encuentra debajo de su stock minimo"
        );
      }
      this.setState(state => ({
        form: {
          ...state.form,
          products: [...state.form.products, Product]
        },
        suggestions: [],
        search: ""
      }));
    }
  };

  handleClick = Product => {
    const { form } = this.state;

    const filterMap = form.products.filter(Item => Item.id !== Product.id);

    this.setState(
      state => ({
        form: {
          ...state.form,
          products: filterMap
        }
      }),
      () => {
        Globals.showSuccess("¡Producto eliminado de la orden!");
      }
    );
  };

  showAlertInfo = (warehouse_id, branch_id) => {
    Globals.confirm(
      "¡Perderá su información de productos al cambiar de almacén!",
      "¿Desea cambiar de almacén?",
      () => {
        this.setState(state => ({
          form: {
            ...state.form,
            products: []
          }
        }));
      }
    );
  };

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback my-2 text-left">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  clearDestiny = () => {
    this.setState(state => ({
      form: {
        ...state.form,
        branch_destination_id: "",
        warehouse_id: "",
        warehouse_destination_id: ""
      },
      warehouses: [],
      warehouses_destination: []
    }));
  };

  handleSerial = item => {
    this.setState({
      edit_serial: true,
      item_serial: item
    });
  };

  onSuccess = async item => {
    let products = [...this.state.form.products];
    const index = products.findIndex(i => parseInt(i.id) === parseInt(this.state.item_serial.id));
    products[index].serials = item;
    products[index].quantity = item.length;
    await this.setState({
      edit_serial: false,
      form: {
        ...this.state.form,
        products: products
      }
    });
    await this.handleNum(
      {
        target: {
          value: item.length,
          name: "quantity"
        }
      },
      index
    );
    await this.getTotalCost();
  };

  onCancel = () => {
    this.setState({
      edit_serial: false
    });
  };

  render() {
    const {
      branches,
      warehouses,
      branches_destination,
      warehouses_destination,
      suggestions,
      form,
      submitted,
      item_serial
    } = this.state;
    const { user } = this.props;

    const multipleWarehouses = user.role === Constants.ROLE_ENTERPRISE ? true : 
      Globals.getAssignedWarehouses(user).length > 1 ? true : false;

    return (
      <React.Fragment>
        {this.state.edit_serial ? (
          <React.Fragment>
            {/*<EditSerial
              onSuccess={ this.onSuccess }
              onCancel={ this.onCancel }
              product={ this.state.item_serial } />*/}

            <ModalScan
              onSuccess={this.onSuccess}
              onCancel={this.onCancel}
              serials={item_serial.serials}
            />
          </React.Fragment>
        ) : (
          <form onSubmit={this.handleSubmit}>
            <div className="text-center container-create-edit-user">
              <div className="row">
                <div className="col-md-6">
                  <Select
                    color="gray"
                    name="branch_id"
                    label="Sucursal"
                    onChange={async e => {
                      await this.handleChange(e);
                      await this.clearDestiny();
                    }}
                    disabled={
                      this.state.form.products.length > 0 ? true : false
                    }
                    value={this.state.form.branch_id}
                    options={branches}
                    error={`${
                      this.hasErrorFor("branch_id") ? "is-invalid" : ""
                    }`}
                    invalidfeedback={this.renderErrorFor("branch_id")}
                  />
                </div>
                <div className="col-md-6">
                  <Select
                    color="gray"
                    name="warehouse_id"
                    label="Almacen"
                    onChange={async e => {
                      await this.handleChange(e);
                      await this.maybeLoadWarehousesDestination(
                        this.state.form.warehouse_id
                      );
                    }}
                    disabled={
                      this.state.form.products.length > 0 ? true : false
                    }
                    value={this.state.form.warehouse_id}
                    options={warehouses}
                    error={`${
                      this.hasErrorFor("warehouse_id") ? "is-invalid" : ""
                    }`}
                    invalidfeedback={this.renderErrorFor("warehouse_id")}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <Select
                    color="gray"
                    name="type"
                    label="Tipo de Operación"
                    onChange={this.handleChange}
                    disabled={
                      this.state.form.products.length > 0 ? true : false
                    }
                    value={this.state.form.type}
                    options={ multipleWarehouses ? Constants.TYPE_ADJUSTMENT : Constants.TYPE_ADJUSTMENT_LIMITED}
                    error={`${this.hasErrorFor("type") ? "is-invalid" : ""}`}
                    invalidfeedback={this.renderErrorFor("type")}
                  />
                </div>

                <div className="col-md-6">
                  <Input
                    color="gray"
                    name="search"
                    placeholder="Introduce el nombre"
                    label="Buscar Productos"
                    value={this.state.search}
                    onChange={this.handleChange}
                    error={`${
                      this.hasErrorFor("products") ? "is-invalid" : ""
                    }`}
                    invalidfeedback={this.renderErrorFor("products")}
                  />
                  <div id="suggestion">
                    {suggestions.length > 0
                      ? suggestions.map((Item, key) => {
                          return (
                            <div
                              key={key}
                              onClick={() => {
                                this.takeProduct(Item);
                              }}
                            >
                              {Item.name}
                            </div>
                          );
                        })
                      : null}
                  </div>
                </div>
              </div>
              {this.state.showTranslate ? (
                <div className="row">
                  <div className="col-md-6">
                    <Select
                      color="gray"
                      name="branch_destination_id"
                      label="Sucursal destino"
                      onChange={this.handleChange}
                      value={this.state.form.branch_destination_id}
                      options={branches_destination}
                      error={`${
                        this.hasErrorFor("branch_destination_id")
                          ? "is-invalid"
                          : ""
                      }`}
                      invalidfeedback={this.renderErrorFor(
                        "branch_destination_id"
                      )}
                    />
                  </div>
                  <div className="col-md-6">
                    <Select
                      color="gray"
                      name="warehouse_destination_id"
                      label="Almacen destino"
                      onChange={this.handleChange}
                      value={this.state.form.warehouse_destination_id}
                      options={warehouses_destination.map(Item => {
                        return {
                          value: Item.id,
                          label: Item.name
                        };
                      })}
                      error={`${
                        this.hasErrorFor("warehouse_destination_id")
                          ? "is-invalid"
                          : ""
                      }`}
                      invalidfeedback={this.renderErrorFor(
                        "warehouse_destination_id"
                      )}
                    />
                  </div>
                </div>
              ) : null}

              {form.products.length > 0 && (
                <div className="row">
                  <div className="col-sm-12">
                    <div className="form-group">
                      <div className="table-responsive">
                        <table className="table table-sm table-bordered">
                          <thead>
                            <tr>
                              <td colSpan="9">PRODUCTOS</td>
                            </tr>
                            <tr style={{ whiteSpace: "nowrap" }}>
                              <th>Codigo</th>
                              <th>Nombre</th>
                              <th>S. Actual</th>
                              <th>S. Mínimo</th>
                              <th>Cant. a Modificar</th>
                              <th>Costo</th>
                              <th>S. Final</th>
                              <th>C. Total</th>
                              <th>Acción</th>
                            </tr>
                          </thead>
                          <tbody className="tbody">
                            {form.products.map((Item, key) => {
                              return (
                                <tr key={key}>
                                  <td>{Item.code}</td>
                                  <td className="name1">{Item.name}</td>
                                  <td>{Item.last_movement}</td>
                                  <td>{Item.stock_min}</td>
                                  <th>
                                    <Input
                                      type="text"
                                      name="quantity"
                                      defaultname="quantity"
                                      color="gray"
                                      value={Item.quantity}
                                      disabled={
                                        Item.serialization ==
                                        Constants.SERIALIZATION.ACTIVE
                                      }
                                      maxLength={20}
                                      onKeyPress={e => {
                                        Globals.soloNumeros(e);
                                      }}
                                      onChange={async emitter => {
                                        await this.handleNum(emitter, key);
                                        await this.getTotalCost();
                                      }}
                                    />
                                  </th>
                                  <th>
                                    {parseInt(this.state.form.type) === 1 ? (
                                      <InputFormat
                                        color="gray"
                                        name="cost"
                                        thousandSeparator={true}
                                        allowNegative={false}
                                        isNumericString={true}
                                        decimalScale="2"
                                        maxLength={15}
                                        onValueChange={values =>
                                          this.handleValueChange(values, key)
                                        }
                                        value={Item.cost}
                                      />
                                    ) : parseInt(this.state.form.type) === 3 &&
                                      parseInt(this.state.form.branch_id) ===
                                        parseInt(
                                          this.state.form.branch_destination_id
                                        ) ? (
                                      <InputFormat
                                        color="gray"
                                        name="cost"
                                        thousandSeparator={true}
                                        allowNegative={false}
                                        isNumericString={true}
                                        decimalScale="2"
                                        maxLength={15}
                                        onValueChange={values =>
                                          this.handleValueChange(values, key)
                                        }
                                        value={Item.cost}
                                      />
                                    ) : (
                                      <NumberFormat
                                        value={Item.cost}
                                        displayType={"text"}
                                        thousandSeparator={true}
                                      />
                                    )}
                                  </th>
                                  <td>{Item.stock}</td>
                                  <td>
                                    <NumberFormat
                                      value={Item.cost_total}
                                      displayType={"text"}
                                      thousandSeparator={true}
                                    />
                                  </td>

                                  <td>
                                    <div className="col-md-12 .col-md-pull-3 p-0">
                                      <div className="container-btn-actions">
                                        <Button
                                          style={{}}
                                          color="red"
                                          small="true"
                                          title="Remover"
                                          type="button"
                                          onClick={() => {
                                            this.handleClick(Item);
                                          }}
                                        >
                                          <Icon name="minus" />
                                        </Button>

                                        {/*
                                              Boton de serialización, se muestra solo si la cantidad es mayor a 0 y se puede serializar el producto
                                            */}

                                        {parseInt(Item.serialization) ===
                                          Constants.SERIALIZATION.ACTIVE && (
                                          <Button
                                            color={
                                              !Item.serials ? "yellow" : "green"
                                            }
                                            small="true"
                                            title={
                                              !Item.serials
                                                ? "Serializar"
                                                : "Serializado"
                                            }
                                            type="button"
                                            className="btn-actions-orders"
                                            onClick={() => {
                                              this.handleSerial(Item);
                                            }}
                                          >
                                            <Icon name="list" />
                                          </Button>
                                        )}
                                      </div>
                                      {parseInt(this.state.form.type) === 1 ? (
                                        <label className="mr-2">
                                          Actualizar Precio:
                                          <CheckBox
                                            style={{
                                              marginLeft: -3,
                                              marginTop: -3
                                            }}
                                            name="change_price"
                                            checked={Item.change_price}
                                            onChange={emitter => {
                                              this.handleNum(emitter, key);
                                            }}
                                          />
                                        </label>
                                      ) : parseInt(this.state.form.type) ===
                                          3 &&
                                        parseInt(this.state.form.branch_id) ===
                                          parseInt(
                                            this.state.form
                                              .branch_destination_id
                                          ) ? (
                                        <label>
                                          Actualizar Precio:
                                          <CheckBox
                                            name="change_price"
                                            checked={Item.change_price}
                                            onChange={emitter => {
                                              this.handleNum(emitter, key);
                                            }}
                                          />
                                        </label>
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                  </td>
                                </tr>
                              );
                            })}
                            <tr>
                              <th colSpan="7">Costo total del movimiento: </th>
                              <th>
                                <NumberFormat
                                  value={this.state.form.final_cost}
                                  displayType={"text"}
                                  thousandSeparator={true}
                                />
                              </th>
                              <th></th>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <div className="row">
                <div className="col-md-12">
                  <Textarea
                    color="gray"
                    value={this.state.form.description}
                    name="description"
                    label="Descripción de la Operación"
                    onChange={this.handleChange}
                    error={`${
                      this.hasErrorFor("description") ? "is-invalid" : ""
                    }`}
                    invalidfeedback={this.renderErrorFor("description")}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md">
                  {submitted ? (
                    <div className="spinner-border text-primary" role="status">
                      <span className="sr-only">Loading...</span>
                    </div>
                  ) : (
                    <div id="button">
                      <Button block="true" type="submit" disabled={submitted}>
                        {this.state.textButton}
                      </Button>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </form>
        )}
      </React.Fragment>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(CreateEditInventoryAdjustment);
