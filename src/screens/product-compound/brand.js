import React from "react";

import { Input, Button } from "../../components";
import { Globals } from "../../utils";
import { Brand as Brands } from "../../services";

class Brand extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      form: {
        code: "",
        name: "",
        creator_id: props.Id
      },
      errors: []
    };
  }

  handleSubmit = emitter => {
    emitter.preventDefault();

    const { submitted, form } = this.state;

    if (submitted) {
      return;
    }

    this.setState({ submitted: true, errors: [] });

    Brands.createBrand(form)
      .then(response => {
        Globals.showSuccess("¡La marca fue creada exitosamente!");
        this.props.brand(response);
      })
      .catch(error => {
        if ((error || {}).message) {
          Globals.showError(error.message);
        }

        this.setState({ submitted: false, errors: error });
      });
  };

  handleChange = emitter => {
    const { name, value } = emitter.target;

    this.setState(state => ({
      form: {
        ...state.form,
        [name]: value
      }
    }));
  };

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback my-2 text-left">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="col-md-12">
          <Input
            color="gray"
            value={this.state.form.code}
            name="code"
            label="Código de Marca"
            onChange={this.handleChange}
            error={`${this.hasErrorFor("code") ? "is-invalid" : ""}`}
            invalidfeedback={this.renderErrorFor("code")}
          />
        </div>
        <div className="col-md-12">
          <Input
            color="gray"
            value={this.state.form.name}
            name="name"
            label="Nombre de Marca"
            onChange={this.handleChange}
            error={`${this.hasErrorFor("name") ? "is-invalid" : ""}`}
            invalidfeedback={this.renderErrorFor("name")}
          />
        </div>
        <div className="col-md-12">
          <div className="form-group">
            <div id="button">
              <Button block="true" type="button" onClick={this.handleSubmit}>
                Crear
              </Button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Brand;
