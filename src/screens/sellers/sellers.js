import React from "react";
import { connect } from "react-redux";

import { Globals, Constants, Format } from "../../utils";
import { Pagination, Table, Button, Icon, Modal } from "../../components";
import { User, Seller, Branch } from "../../services";

// Components
import Menu from "../menu";
import Filter from "./filterForm";
import View from "./view_seller";
import CreateEditSeller from "./create_edit_seller";

const TABLE_HEADER = [
  "Nombre de Usuario",
  "Nombre/Razón social",
  "Correo electrónico",
  "Cédula",
  "Estatus",
  "Acciones"
];

class Sellers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      Item: "",
      page: 1,
      current_page: 1,
      last_page: 1,
      create: false,
      view: false,
      branches: []
    };
  }

  componentDidMount() {
    this.load();
  }

  componentDidUpdate(_, prevState) {
    if (prevState.create !== this.state.create && this.state.create) {
      this.maybeLoadBranches();
    }
  }

  load = (form = {}) => {
    const { user } = this.props;
    const { page } = this.state;

    const userId =
      user.role === 3 ? user.id : user.enterprise_users.enterprise_id;

    Promise.all([Seller.getSellers(page, { id: userId, ...form })])
      .then(response => {
        const { current_page, data, last_page } = response[0];

        this.setState({
          data: data,
          current_page: current_page,
          last_page: last_page
        });
      })
      .catch(error => {
        Globals.showError();
      });
  };

  maybeLoadBranches = () => {
    const { user } = this.props;

    const id = user.role === 3 ? user.id : user.enterprise_users.enterprise_id;

    Promise.all([Branch.getBranchesByEnterprise({ Id: id })])
      .then(response => {
        const branchesMap = Format.rawBranches(
          response[0],
          user.assigned_warehouses,
          user.role
        ).map(({ name, id }) => ({
          label: name,
          value: id
        }));

        this.setState({
          branches: [{ label: "Todos", value: 0 }, ...branchesMap]
        });
      })
      .catch(error => {
        Globals.showError();
      });
  };

  verifyUser = (Item, Key) => {
    const { person } = Item;
    const name = person.first_name;

    Globals.confirm(`¿Desea verificar a: ${name}?`, () => {
      User.setStatusVerify({ Id: Item.id })
        .then(response => {
          this.load();
          Globals.showSuccess("¡Usuario verificado exitosamente!");
        })
        .catch(error => {
          Globals.showError();
        });
    });
  };

  activateUser = Item => {
    const { person } = Item;
    const name = person.first_name;

    Globals.confirm(`¿Desea activar a: ${name}?`, () => {
      User.setStatusActive({ Id: Item.id })
        .then(response => {
          //socket.emit('activate-user', { id: item.id });
          this.load();
          Globals.showSuccess("¡Usuario activado exitosamente!");
        })
        .catch(error => {
          Globals.showError();
        });
    });
  };

  inactiveUser = Item => {
    const { person } = Item;
    const name = person.first_name;

    Globals.confirm(`¿Desea suspender a: ${name}?`, () => {
      User.setStatusInactive({ Id: Item.id })
        .then(response => {
          // socket.emit('disable-user',{id: item.id});
          this.load();
          Globals.showSuccess("¡Usuario suspendido exitosamente!");
        })
        .catch(error => {
          Globals.showError();
        });
    });
  };

  removeUser = (Item, key) => {
    const { person } = Item;
    const name = person.first_name;

    Globals.confirm(`¿Desea eliminar a: ${name}?`, () => {
      User.removeUserById({ Id: Item.id })
        .then(response => {
          // socket.emit('disable-user',{id: item.id});
          this.load();
          Globals.showSuccess("¡Usuario eliminado exitosamente!");
        })
        .catch(error => {
          Globals.showError();
        });
    });
  };

  close = () => {
    this.setState(
      {
        create: false,
        view: false,
        Item: ""
      },
      () => {
        this.load();
      }
    );
  };

  render() {
    const { data, create, view, Item, branches } = this.state;
    const { user } = this.props;

    const userId = Globals.getUserId(user);
    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    return (
      <Menu history={this.props.history}>
        {create && (
          <Modal
            title={Item ? "Editar vendedor" : "Crear vendedor"}
            onClose={this.close}
            visible
          >
            <CreateEditSeller
              Item={Item}
              userId={userId}
              branches={branches.map(({ value, label }) => ({
                value: value,
                label: label
              }))}
              onClose={this.close}
            />
          </Modal>
        )}

        {view && (
          <Modal title="Ver vendedor" onClose={this.close} visible>
            <View user={Item} enterprise={enterpriseId} />
          </Modal>
        )}

        <div id="home">
          <div className="container-fluid">
            <Filter onSubmit={form => this.load(form)} />

            <Table
              data={data.length}
              title="Vendedores"
              header={TABLE_HEADER}
              right={
                <Button
                  title="Agregar Vendedores"
                  outline="true"
                  small="true"
                  onClick={() => {
                    this.setState({
                      create: true
                    });
                  }}
                >
                  <Icon name="plus" />
                </Button>
              }
            >
              {data.map((Item, key) => {
                return (
                  <tr key={Item.id}>
                    <td>{Item.name}</td>
                    <td>{Item.person.first_name}</td>
                    <td>{Item.email}</td>
                    <td>{Item.person.identity_document}</td>
                    <td
                      className={
                        Item.status == Constants.STATUS_ACTIVE
                          ? "text-success"
                          : Item.status == Constants.STATE_SUSPENDED
                          ? "text-warning"
                          : "text-danger"
                      }
                    >
                      {Item.status == Constants.STATUS_ACTIVE
                        ? "Activo"
                        : Item.status == Constants.STATE_SUSPENDED
                        ? "Suspendido"
                        : "Inactivo"}
                    </td>
                    <td>
                      <Button
                        title="Ver"
                        small="true"
                        onClick={() => {
                          this.setState({
                            Item: Item,
                            view: true
                          });
                        }}
                      >
                        <Icon name="eye" />
                      </Button>

                      <Button
                        title="Editar"
                        color="primary"
                        small="true"
                        onClick={() =>
                          this.setState({
                            Item: Item,
                            create: true
                          })
                        }
                      >
                        <Icon name="edit" />
                      </Button>

                      {Item.status === Constants.STATE_SUSPENDED && (
                        <Button
                          color="info"
                          title="Activar"
                          small="true"
                          onClick={() => {
                            this.activateUser(Item);
                          }}
                        >
                          <Icon name="check" />
                        </Button>
                      )}

                      {Item.status === Constants.STATUS_INACTIVE && (
                        <Button
                          color="success"
                          title="Verificar"
                          small="true"
                          onClick={() => {
                            this.verifyUser(Item);
                          }}
                        >
                          <Icon name="user-plus" />
                        </Button>
                      )}

                      {Item.status === Constants.STATUS_ACTIVE && (
                        <Button
                          color="info"
                          title="Suspender"
                          small="true"
                          onClick={() => this.inactiveUser(Item, key)}
                        >
                          <Icon name="close" />
                        </Button>
                      )}

                      <Button
                        color="red"
                        title="Eliminar"
                        small="true"
                        onClick={() => this.removeUser(Item, key)}
                      >
                        <Icon name="trash" />
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </Table>

            <Pagination
              pages={this.state.last_page}
              active={this.state.page}
              onChange={page => {
                this.setState(
                  {
                    page: page
                  },
                  () => this.load()
                );
              }}
            />
          </div>
        </div>
      </Menu>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(Sellers);
