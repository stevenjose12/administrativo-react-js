import React from "react";
import { connect } from "react-redux";

import {
  Table,
  Pagination,
  Button,
  Icon,
  Modal,
  Input,
  Select
} from "../../components";
import { axios, Globals, Colors } from "../../utils";

// Components
import Menu from "../menu";
import ViewProvider from "./view_provider";
import CreateEditProvider from "./create_edit_provider";

const TABLE_HEADER = [
  "Nombre",
  "Código",
  "Email",
  "RIF",
  "Teléfono",
  "Estatus",
  "Acciones"
];

const STATUS = [
  { value: "", label: "Todos" },
  { value: 1, label: "Activos" },
  { value: 2, label: "Suspendidos" }
];
class Providers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      last_page: 1,
      data: [],
      retentions: [],
      enterprises: [],
      user: null,
      view: false,
      form: {
        search: ""
      }
    };
  }

  componentDidMount() {
    this.load();
  }

  load = () => {
    Globals.setLoading();
    let param = this.state.form;
    param.role = this.props.user.role;
    if (this.props.user.role === 3) {
      param.user_id = this.props.user.id;
    }
    if (this.props.user.role === 4) {
      param.user_id = this.props.user.enterprise_users.enterprise_id;
    }
    axios
      .post("admin/providers/get?page=" + this.state.page, param)
      .then(res => {
        if (res.data.result) {
          this.setState({
            last_page: res.data.providers.last_page,
            data: res.data.providers.data,
            retentions: res.data.retentions,
            enterprises: res.data.enterprises
          });
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  close = async () => {
    await this.setState({
      create: false,
      view: false,
      edit: null,
      user: null
    });
    await this.load();
  };

  suspend = (item, i) => {
    Globals.confirm("¿Desea suspender a " + item.name + "?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/providers/suspend", { id: item.id })
        .then(res => {
          if (res.data.result) {
            Globals.showSuccess(res.data.msg);
            this.load();
          } else {
            Globals.showError();
          }
        })
        .catch(err => {
          Globals.showError();
        })
        .then(() => {
          Globals.quitLoading();
        });
    });
  };

  activate = (item, i) => {
    Globals.confirm("¿Desea activar a: " + item.name + "?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/providers/active", { id: item.id })
        .then(res => {
          if (res.data.result) {
            Globals.showSuccess(res.data.msg);
            this.load();
          } else {
            Globals.showError();
          }
        })
        .catch(err => {
          Globals.showError();
        })
        .then(() => {
          Globals.quitLoading();
        });
    });
  };

  delete = (item, i) => {
    Globals.confirm("¿Desea eliminar a: " + item.name + "?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/providers/delete", { id: item.id })
        .then(res => {
          Globals.showSuccess(res.data.msg);
          this.load();
        })
        .catch(err => {
          Globals.showError(err.response.data.msg);
        })
        .then(() => {
          Globals.quitLoading();
        });
    });
  };

  getStatus = status => {
    let respuesta = "";
    switch (status) {
      case 0:
        respuesta = {
          text: "Nuevo",
          color: Colors.red
        };
        break;

      case 1:
        respuesta = {
          text: "Activo",
          color: Colors.green
        };
        break;

      case 2:
        respuesta = {
          text: "Suspendido",
          color: Colors.orange
        };
        break;
      default:
        break;
    }
    return respuesta;
  };

  change = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };
  render() {
    const { view, create } = this.state;

    return (
      <Menu history={this.props.history}>
        {view && (
          <Modal title="Ver Proveedor" onClose={this.close} visible>
            <ViewProvider user={this.state.user} />
          </Modal>
        )}

        {create && (
          <Modal
            title={this.state.edit ? "Editar Proveedor" : "Nuevo Proveedor"}
            onClose={this.close}
            visible
          >
            <CreateEditProvider
              edit={this.state.edit}
              retentions={this.state.retentions}
              enterprises={this.state.enterprises}
              onClose={this.close}
            />
          </Modal>
        )}

        <div id="home">
          <div className="row">
            <div className="col-sm-12 col-md-5">
              <Input
                color="white"
                name="search"
                label="Búsqueda"
                onChange={this.change}
                value={this.state.search}
                placeholder="Buscar por Nombre, Código, RIF, Email o Teléfono"
              />
            </div>
            <div className="col-sm-12 col-md-5">
              <Select
                color="white"
                name="status"
                label="Status"
                defaultname="Seleccione"
                onChange={this.change}
                value={this.state.status}
                options={STATUS.map(Item => {
                  return {
                    value: Item.value,
                    label: Item.label
                  };
                })}
              />
            </div>
            <div className="col-sm-12 col-md-2">
              <Button className="btn-align-bottom" onClick={this.load}>
                Filtrar
              </Button>
            </div>
          </div>

          <Table
            data={this.state.data.length}
            title="Proveedores"
            header={TABLE_HEADER}
            right={
              <Button
                title="Agregar Proveedores"
                outline="true"
                small="true"
                onClick={() => {
                  this.setState({
                    create: true
                  });
                }}
              >
                <Icon name="plus" />
              </Button>
            }
          >
            {this.state.data.map((Item, key) => {
              return (
                <tr key={key}>
                  <td>{Item.name}</td>
                  <td>{Item.code}</td>
                  <td>{Item.email}</td>
                  <td>{Item.fiscal_identification}</td>
                  <td>{Item.phone}</td>
                  <td style={{ color: this.getStatus(Item.status).color }}>
                    {this.getStatus(Item.status).text}
                  </td>
                  <td>
                    <Button
                      title="Ver"
                      small="true"
                      onClick={() => {
                        this.setState({
                          user: Item,
                          view: true
                        });
                      }}
                    >
                      <Icon name="eye" />
                    </Button>
                    <Button
                      title="Editar"
                      color="primary"
                      small="true"
                      onClick={() =>
                        this.setState({
                          create: true,
                          edit: {
                            element: Item,
                            index: key
                          }
                        })
                      }
                    >
                      <Icon name="edit" />
                    </Button>
                    {Item.status === 2 && (
                      <Button
                        color="info"
                        title="Activar"
                        small="true"
                        onClick={() => {
                          this.activate(Item, key);
                        }}
                      >
                        <Icon name="check" />
                      </Button>
                    )}
                    {Item.status === 1 && (
                      <Button
                        color="info"
                        title="Suspender"
                        small="true"
                        onClick={() => this.suspend(Item, key)}
                      >
                        <Icon name="close" />
                      </Button>
                    )}
                    <Button
                      color="red"
                      title="Eliminar"
                      small="true"
                      onClick={() => this.delete(Item, key)}
                    >
                      <Icon name="trash" />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </Table>

          <Pagination
            pages={this.state.last_page}
            active={this.state.page}
            onChange={page => {
              this.setState(
                {
                  page: page
                },
                () => {
                  this.load();
                }
              );
            }}
          />
        </div>
      </Menu>
    );
  }
}
export default connect(state => {
  return {
    user: state.user
  };
})(Providers);
