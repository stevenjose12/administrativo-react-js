import React from "react";
import { connect } from "react-redux";
import Menu from "../menu";
import {
  Table,
  Pagination,
  Button,
  Icon,
  Modal,
  Select,
  Input,
  ViewSerial
} from "../../components";
import { Globals, Constants, Format } from "../../utils";
import CreateEditInventoryAdjustment from "./create_edit_inventory";
import { Product, Branch, Warehouse } from "../../services";

const HEADER_TABLE = [
  "Nombre",
  "Código",
  "Almacenes",
  "Sucursales",
  "Stock Actual",
  "Acciones"
];
class InventoryAdjustment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      last_page: 1,
      data: [],
      products: [],
      warehouses: [
        {
          value: "",
          label: "Todos"
        }
      ],
      branches: [
        {
          value: "",
          label: "Todos"
        }
      ],
      showLg: true,
      visibleCreate: false,
      user: null,
      view: false,
      view_serials: false,
      item_serials: null,
      serials: null,
      form: {
        user_id:
          props.user.role === Constants.ROLE_USER
            ? props.user.enterprise_users.enterprise_id
            : props.user.id,
        role: props.user.role,
        status: "",
        search: "",
        warehouse_id: "",
        branch_id: ""
      }
    };
  }

  abortController = new AbortController();

  componentDidMount() {
    this.load();
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.visibleCreate !== this.state.visibleCreate &&
      this.state.visibleCreate
    ) {
      this.maybeLoadProducts();
    }

    if (
      prevState.form.branch_id !== this.state.form.branch_id &&
      this.state.form.branch_id
    ) {
      this.getWarehouses();
    }
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  load = (resetPage = null) => {
    let { page, form } = this.state;
    const { user } = this.props;

    if (typeof resetPage === "number") {
      page = resetPage;
    }

    let newForm = { ...form };

    if (user.assigned_warehouses.length > 0 && !newForm.branch_id) {
      newForm.warehouse_id = user.assigned_warehouses.map(
        ({ warehouse_id }) => warehouse_id
      );
    } else if (
      user.assigned_warehouses.length > 0 &&
      newForm.warehouse_id === ""
    ) {
      newForm.warehouse_id = user.assigned_warehouses.map(
        ({ warehouse_id }) => warehouse_id
      );
    }

    Product.getProductsInventory(page, newForm)
      .then(({ products_warehouse }) => {
        if (products_warehouse.data.last_page > 0) {
          if (products_warehouse.data.length > 0) {
            this.setState({
              last_page: products_warehouse.last_page,
              data: products_warehouse.data,
              page: page
            });
          } else {
            this.load(1);
          }
        } else {
          this.setState({
            last_page: products_warehouse.last_page,
            data: products_warehouse.data
          });
        }
      })
      .catch(() => Globals.showError())
      .finally(() => this.getBranches());
  };

  close = () => {
    this.setState(
      {
        visibleCreate: false,
        showLg: true,
        view: false,
        edit: "",
        user: ""
      },
      () => this.load()
    );
  };

  change = emitter => {
    const { name, value } = emitter.target;

    this.setState({
      form: {
        ...this.state.form,
        [name]: value
      }
    });
  };

  maybeLoadProducts = () => {
    const { user } = this.props;
    const Id = user.role === 4 ? user.enterprise_users.enterprise_id : user.id;

    Product.getAllProductsByEnterprise({ user_id: Id })
      .then(response => {
        const rawProduct = response.map(Item => {
          let current_stock = 0;

          if (Item.last_movement) {
            current_stock =
              Item.last_movement.type === 1
                ? Item.last_movement.current_stock + Item.last_movement.amount
                : Item.last_movement.current_stock - Item.last_movement.amount;
          }

          return {
            id: Item.product.id,
            name: Item.product.name,
            code: Item.product.code,
            last_movement: current_stock,
            warehouse_id: Item.warehouse_id,
            cost: Item.cost,
            quantity: "",
            change_price: false,
            cost_total: 0,
            type: Item.product.type,
            stock: current_stock,
            stock_min: Item.stock_min,
            stock_max: Item.stock_max,
            serialization: Item.product.serialization,
            product_details: Item.product.product_details
          };
        });

        this.setState({ products: rawProduct });
      })
      .catch(() => Globals.showError());
  };

  getBranches = () => {
    let { user } = this.props;
    user.enterprise_id =
      user.role === 4 ? user.enterprise_users.enterprise_id : user.id;

    Branch.getBranches({
      role: user.role,
      enterprise_id: user.enterprise_id
    }).then(response => {
      if (response.result) {
        let branches_filtered = response.branches.data.filter(
          ({ deleted_at }) => !deleted_at
        );

        branches_filtered = Format.rawBranches(
          branches_filtered,
          user.assigned_warehouses,
          user.role
        );

        branches_filtered = branches_filtered.map(({ id, name }) => ({
          value: id,
          label: name
        }));

        this.setState({
          branches: [{ value: "", label: "Todos" }, ...branches_filtered]
        });
      }
    });
  };

  getWarehouses = () => {
    const { user } = this.props;
    const { form } = this.state;

    user.enterprise_id =
      user.role === 4 ? user.enterprise_users.enterprise_id : user.id;

    Warehouse.getWarehouses({
      role: user.role,
      user_id: user.enterprise_id,
      branch_id: form.branch_id
    }).then(response => {
      if (response.result) {
        let warehouses_filtered = response.warehouses.data.filter(
          ({ deleted_at }) => !deleted_at
        );

        warehouses_filtered = Format.rawWarehouse(
          warehouses_filtered,
          user.assigned_warehouses,
          user.role
        );

        warehouses_filtered = warehouses_filtered.map(({ id, name }) => ({
          value: id,
          label: name
        }));

        this.setState(state => ({
          warehouses: [{ value: "", label: "Todos" }, ...warehouses_filtered],
          form: {
            ...state.form,
            warehouse_id: ""
          }
        }));
      }
    });
  };

  view = item => {
    this.setState({
      view_serials: true,
      item_serials: item.product,
      serials: item.serials
    });
  };

  onCloseSerials = () => {
    this.setState({
      view_serials: false,
      item_serials: null,
      serials: null
    });
  };

  render() {
    const {
      visibleCreate,
      showLg,
      products,
      view_serials,
      item_serials,
      serials,
      warehouses,
      branches
    } = this.state;
    const { user } = this.props;

    const Id = user.role === 4 ? user.enterprise_users.enterprise_id : user.id;

    return (
      <Menu history={this.props.history}>
        {visibleCreate && (
          <Modal
            className={showLg ? "modal-product" : ""}
            title={"Operaciones de Inventario"}
            onClose={this.close}
            visible
          >
            <CreateEditInventoryAdjustment
              Id={Id}
              products={products}
              edit={this.state.edit}
              branches={branches.filter(({ value }) => !!value)}
              onClose={this.close}
              user={user}
            />
          </Modal>
        )}

        {view_serials && (
          <Modal
            className="modal-product"
            title="Ver Seriales"
            visible
            onClose={() =>
              this.setState({
                item_serials: null,
                view_serials: false,
                serials: null
              })
            }
          >
            <ViewSerial
              onBack={() =>
                this.setState({
                  item_serials: null,
                  view_serials: false,
                  serials: null
                })
              }
              product={item_serials}
              serials={serials}
            />
          </Modal>
        )}

        <div id="products">
          <div className="row">
            <div className="col-sm-12 col-md">
              <Input
                color="white"
                name="search"
                label="Búsqueda"
                onChange={this.change}
                value={this.state.search}
                placeholder="Buscar por Nombre"
              />
            </div>
            <div className="col-sm-12 col-md">
              <Select
                color="white"
                name="branch_id"
                label="Sucursales"
                defaultname="Seleccione"
                disabledFirst="true"
                onChange={emitter => this.change(emitter)}
                value={this.state.form.branch_id}
                options={branches}
              />
            </div>
            <div className="col-sm-12 col-md">
              <Select
                color="white"
                name="warehouse_id"
                label="Almacenes"
                defaultname="Seleccione"
                disabledFirst="true"
                onChange={this.change}
                value={this.state.form.warehouse_id}
                options={warehouses}
              />
            </div>
            <div className="col-md-2">
              <Button className="btn-align-bottom" onClick={this.load}>
                Filtrar
              </Button>
            </div>
          </div>
          <div className="row">
            <div className="col col-md">
              <Table
                data={this.state.data.length}
                title="Operaciones de Inventario"
                header={HEADER_TABLE}
                right={
                  <Button
                    title="Agregar Op. Inventario"
                    outline="true"
                    small="true"
                    onClick={() => {
                      this.setState({
                        visibleCreate: true,
                        showLg: true
                      });
                    }}
                  >
                    <Icon name="plus" />
                  </Button>
                }
              >
                {this.state.data.map((Item, key) => {
                  Item.current_stock = 0;

                  if (Item.last_movement) {
                    Item.current_stock =
                      Item.last_movement.type === 1
                        ? Item.last_movement.current_stock +
                          Item.last_movement.amount
                        : Item.last_movement.current_stock -
                          Item.last_movement.amount;
                  }

                  return (
                    <tr key={key}>
                      <td>{Item.product.name}</td>
                      <td>{Item.product.code}</td>
                      <td>
                        {Item.warehouse.name}{" "}
                        {Item.warehouse.deleted_at ? "(Eliminado)" : ""}
                      </td>
                      <td>
                        {Item.warehouse.branch.name}{" "}
                        {Item.warehouse.branch.deleted_at ? "(Eliminada)" : ""}
                      </td>
                      <td>{Item.current_stock}</td>
                      <td>
                        {Item.serials.length > 0 && (
                          <Button
                            color="primary"
                            title="Ver Seriales"
                            small="true"
                            onClick={() => this.view(Item)}
                          >
                            <Icon name="eye" />
                          </Button>
                        )}
                      </td>
                    </tr>
                  );
                })}
              </Table>
            </div>
          </div>
          <div className="row my-3">
            <div className="col col-md">
              <Pagination
                pages={this.state.last_page}
                active={this.state.page}
                onChange={page => {
                  this.setState(
                    {
                      page: page
                    },
                    () => this.load()
                  );
                }}
              />
            </div>
          </div>
        </div>
      </Menu>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(InventoryAdjustment);
