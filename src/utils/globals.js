import HoldOn from "react-hold-on";
import Swal from "sweetalert2";
import $ from "jquery";
import Colors from "./colors";
import { Constants } from ".";
import { Enterprise } from "../services";

class Globals {
  setLoading = () => {
    HoldOn.open({
      theme: "sk-bounce"
    });
  };

  quitLoading = () => {
    HoldOn.close();
  };

  soloNumeros = e => {
    var key = window.Event ? e.which : e.keyCode;
    var condition = key >= 48 && key <= 57;
    if (!condition) {
      e.preventDefault();
    }
  };

  formatMiles = (n, decimals = true) => {
    var c, d, t, j, s, i;
    c = isNaN((c = Math.abs(c))) ? 2 : c;
    d = d === undefined ? "," : d;
    t = t === undefined ? "." : t;
    s = n < 0 ? "-" : "";
    i = String(parseInt((n = Math.abs(Number(n) || 0).toFixed(c))));
    j = (j = i.length) > 3 ? j % 3 : 0;

    return (
      s +
      (j ? i.substr(0, j) + t : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
      (c
        ? d +
          Math.abs(n - i)
            .toFixed(c)
            .slice(2)
        : "")
    );
  };

  hideTooltip = async () => {
    await setTimeout(async () => {
      await $('[data-toggle="tooltip"]').tooltip("hide");
    }, 300);
  };

  showSuccess = message => {
    Swal.fire({
      title: "",
      text: message,
      type: "success",
      showCancelButton: false
    });
  };

  showWarning = message => {
    Swal.fire({
      title: "",
      text: message,
      type: "warning",
      showCancelButton: false
    });
  };

  showInformation = message => {
    Swal.fire({
      title: "",
      text: message,
      type: "info",
      showCancelButton: false
    });
  };

  showError = message => {
    Swal.fire({
      title: "",
      text: message ? message : "Se ha producido un error",
      type: "error",
      showCancelButton: false
    });
  };

  confirm = (message, callback, title = "") => {
    Swal.fire({
      title: title,
      text: message,
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar"
    }).then(async confirm => {
      await this.hideTooltip();
      if (confirm.value) {
        callback();
      }
    });
  };

  sure = (message, callback, title = "") => {
    Swal.fire({
      title: title,
      text: message,
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar"
    }).then(async confirm => {
      await this.hideTooltip();
      callback(confirm);
    });
  };

  getUserId = user => {
    const { id: userId, role: roleId, enterprise_users: enterprise } = user;

    return roleId === Constants.ROLE_ENTERPRISE
      ? userId
      : roleId === Constants.ROLE_SELLER
      ? userId
      : enterprise
      ? enterprise.enterprise_id
      : undefined;
  };

  getEvaluation = roleId => {
    switch (roleId) {
      case Constants.ROLE_SUPER:
        return false;
      case Constants.ROLE_ADMIN:
        return false;
      case Constants.ROLE_ENTERPRISE:
        return false;
      default:
        return true;
    }
  };

  setTypeRole = user => {
    const { role: roleId } = user;

    switch (roleId) {
      case Constants.ROLE_SUPER:
        return user;
      case Constants.ROLE_ADMIN:
        return user;
      default:
        return false;
    }
  };

  getRate = user => {
    const { role } = user;

    switch (role) {
      case Constants.ROLE_SELLER:
        return (user || {}).configuration_seller
          ? user.configuration_seller
          : null;
      default:
        return null;
    }
  };

  getZones = ({ zones: zonesId, role: roleId }) => {
    return roleId === Constants.ROLE_SELLER
      ? zonesId.map(({ zoneId }) => zoneId)
      : [];
  };

  getStatus = (
    status,
    custom_value = null,
    custom_text = "",
    custom_color = ""
  ) => {
    // custom_color debe ser un hexadecimal, custom_text debe ser un string
    let respuesta = "";
    switch (status) {
      case 0:
        respuesta = {
          text: "Nuevo",
          color: Colors.red
        };
        break;
      case 1:
        respuesta = {
          text: "Activo",
          color: Colors.green
        };
        break;
      case 2:
        respuesta = {
          text: "Suspendido",
          color: Colors.orange
        };
        break;
      case 3:
        respuesta = {
          text: "Eliminado",
          color: Colors.red
        };
        break;
      case custom_value:
        respuesta = {
          text: custom_text,
          color: custom_color
        };
        break;
      default:
        break;
    }
    return respuesta;
  };

  getPaymentsType = type => {
    let response = "";
    switch (type) {
      case 1:
        response = "Efectivo";
        break;
      case 2:
        response = "Debito";
        break;
      case 3:
        response = "Credito";
        break;
      case 4:
        response = "Transferencia";
        break;
      default:
        break;
    }
    return response;
  };

  getEnterpriseId = user => {
    const enterprise_id = user
      ? user.role === Constants.ROLE_ENTERPRISE ||
        user.role === Constants.ROLE_SELLER
        ? user.id
        : user.enterprise_users.enterprise_id
      : "";
    return enterprise_id;
  };

  getAssignedWarehouses = user => {
    let arr = [];
    // assigned_warehouses viene del backend cuando el usuario inicia sesion
    if (user.assigned_warehouses.length > 0) {
      arr = user.assigned_warehouses.map(Item => {
        return Item.warehouse_id;
      });
    }
    return arr;
  };

  filterWarehouseOrBranches = (arr_data, user, type) => {
    // type = branch o warehouse
    // Role = 3, Empresa
    // Role = 4, Usuario
    // Role = 5, Vendedor
    const param = type === "branch" ? "branch_id" : "warehouse_id";
    let filter = arr_data.filter(function(data) {
      return !data.deleted_at && data.status === 1;
    });
    if (user.role === 4) {
      filter = filter.filter(el => {
        return user.assigned_warehouses.some(f => {
          return parseInt(f[param]) === parseInt(el.id);
        });
      });
    } else {
      filter = filter;
    }
    return filter;
  };
}

export default new Globals();
