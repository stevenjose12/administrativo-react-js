import React from "react";
import { Button, Select, Input, Textarea } from "../../components";
import { axios, Globals } from "../../utils";

class CreateEditWarehouse extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {},
      branches: this.props.branches,
      users: [],
      textButton: "Crear"
    };
  }

  componentDidMount() {
    if (this.props.edit) {
      this.edit();
      this.getEnterprises();
    } else {
      this.getEnterprises();
    }
  }

  submit = async () => {
    let param = { ...this.state.form };
    Globals.setLoading();
    axios
      .upload(
        this.props.edit ? "admin/warehouses/edit" : "admin/warehouses/create",
        param
      )
      .then(res => {
        if (res.data.result) {
          this.setState({
            form: {}
          });
          Globals.showSuccess(res.data.msg);
          this.props.onClose();
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(() => Globals.showError())
      .then(() => Globals.quitLoading());
  };

  edit = async () => {
    await this.setState({
      form: {
        name: this.props.edit.element.name,
        description: this.props.edit.element.description,
        code: this.props.edit.element.code,
        location: this.props.edit.element.location ? this.props.edit.element.location : '',
        user_id: this.props.edit.element.user_id,
        creator_id: this.props.edit.element.creator_id,
        branch_id: this.props.edit.element.branch_id,
        id: this.props.edit.element.id
      },
      textButton: "Editar"
    });
  };

  getEnterprises = async () => {
    Globals.setLoading();
    axios
      .get("admin/warehouses/enterprises")
      .then(res => {
        if (res.data.result) {
          this.setState({
            users: res.data.users
          });
        }
      })
      .catch(() => Globals.showError())
      .then(() => Globals.quitLoading());
  };

  change = e => {
    this.setState({
      form: {
        user_id:
          this.props.user.role === 4
            ? this.props.user.enterprise_users.enterprise_id
            : this.props.user.id,
        creator_id: this.props.user.id,
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  render() {
    const admin =
      this.props.user.role === 1 || this.props.user.role === 2 ? true : false;
    let selectEnterprise;
    if (admin) {
      selectEnterprise = (
        <Select
          color="gray"
          name="user_id"
          label="Empresa"
          defaultname="Seleccione"
          onChange={this.change}
          value={this.state.form.user_id}
          options={this.state.users.map(i => {
            return {
              value: i.id,
              label: i.name
            };
          })}
        />
      );
    }
    return (
      <div className="text-center container-create-edit-user">
        <Input
          color="gray"
          value={this.state.form.name}
          name="name"
          label="Nombre"
          onChange={this.change}
        />
        <Textarea
          color="gray"
          value={this.state.form.description}
          name="description"
          label="Descripcion"
          onChange={this.change}
        />
        <Input
          color="gray"
          value={this.state.form.code}
          name="code"
          label="Código"
          onChange={this.change}
        />
        <Textarea
          color="gray"
          value={this.state.form.location}
          name="location"
          label="Ubicacion"
          onChange={this.change}
        />
        {selectEnterprise}
        <Select
          color="gray"
          name="branch_id"
          label="Sucursal"
          defaultname="Seleccione"
          disabledFirst="false"
          onChange={this.change}
          value={this.state.form.branch_id}
          options={this.state.branches.map(i => {
            return {
              value: i.id,
              label: i.name
            };
          })}
        />
        {/* <File
                    placeholder={this.props.edit ? 'Cambiar imagen de perfil (Opcional) ' : "Agregar Imagen de perfil"}
                    placeholdersuccess="Imagen de perfil Agregada"
                    showcheck={ true.toString() }
                    onChange={ this.change }
                    name="image"
                    value={ this.state.form.image }
                    inputstyle={{
                        display: 'contents'
                    }}
                    className="btn-product" /> */}
        <div id="button">
          <Button block="true" type="button" onClick={() => this.submit()}>
            {this.state.textButton}
          </Button>
        </div>
      </div>
    );
  }
}
export default CreateEditWarehouse;
