import React from 'react';
import { List, Table} from '../../components';
import { Globals } from '../../utils';

class ViewPurchase extends React.Component {

	state = {
        purchase: this.props.purchase,
        header: [
			'Código',
			'Cantidad',
            'Precio Unitario',
            'Total',
			'Nombre del Producto'
		],
	}
	
	render() {
		return (
			<div className="text-center container-view-user">
                <Table data={ this.state.purchase.details.length } title="Detalle del Informe de Visita" header={ this.state.header }>
                    { this.state.purchase.details.map((i,index) => {
                        return (
                            <tr key={ index }>
                                <td>{ i.product.code }</td>
                                <td>{ i.quantity }</td>
                                <td>{ 'Bs. ' + Globals.formatMiles(i.price) }</td>
                                <td>{ 'Bs. ' + Globals.formatMiles(i.total) }</td>
                                <td>{ i.product.name }</td>
                            </tr>
                        )
                    }) }
                </Table>
                <div className="row">
                    <div className="col-md-6">
                        <List.Container>
                            <label style={{paddingTop: '10px', fontWeight: 'bold'}}>Cliente</label>
                            <List.Item label="Nombre">
                                { this.state.purchase.client.name }
                            </List.Item>
                            <List.Item label="Email">
                                { this.state.purchase.client.email }
                            </List.Item>
                            <List.Item label="Teléfono">
                                { this.state.purchase.client.phone }
                            </List.Item>
                            <List.Item label="Direccion">
                                { this.state.purchase.client.address }
                            </List.Item>
                        </List.Container>
                    </div>
                    <div className="col-md-6">
                        <List.Container>
                            <label style={{paddingTop: '10px', fontWeight: 'bold'}}>Vendedor</label>
                            <List.Item label="Nombre">
                                { this.state.purchase.user.person.name + ' ' + this.state.purchase.user.person.lastname }
                            </List.Item>
                            <List.Item label="Email">
                                { this.state.purchase.user.email }
                            </List.Item>
                            <List.Item label="Cédula o RIF">
                                { this.state.purchase.user.person.document }
                            </List.Item>
                            <List.Item label="Teléfono">
                                { this.state.purchase.user.person.phone }
                            </List.Item>
                        </List.Container>
                    </div>
                </div>
			</div>
		)
	}
}

export default ViewPurchase;