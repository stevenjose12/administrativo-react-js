import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { Globals, Constants, Menus } from "../utils";
import { Sidebar } from "../services";
import { Scrollbars } from "react-custom-scrollbars";

// import Socket from '../utils/socket';
class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showMenu: false,
      leftOpen: true,
      rightOpen: true,
      menu: []
    };
  }

  componentDidMount() {
    const { sidebar } = this.props;
    this.maybeLoadMenu(sidebar);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.sidebar !== this.props.sidebar && this.props.sidebar) {
      this.maybeLoadMenu(this.props.sidebar);
    }
  }

  maybeLoadMenu = sidebar => {
    let sidebarNav = {
      Inventario: [],
      Ventas: [],
      Compras: [],
      Utilidades: [],
      Reportes: [],
      CuentasPorCobrar: [],
      Gastos: [],
      Pagos: [],
      Bancos: []
    };

    sidebar
      .filter(Item => Item.status === Constants.STATUS_ACTIVE)
      .forEach(Item => {
        if (Item.menu_id === Menus[1].id) {
          sidebarNav["Inventario"].push(Item);
        } else if (Item.menu_id === Menus[2].id) {
          sidebarNav["Ventas"].push(Item);
        } else if (Item.menu_id === Menus[3].id) {
          sidebarNav["Compras"].push(Item);
        } else if (Item.menu_id === Menus[4].id) {
          sidebarNav["Utilidades"].push(Item);
        } else if (Item.menu_id === Menus[5].id) {
          sidebarNav["Reportes"].push(Item);
        } else if (Item.menu_id === Menus[6].id) {
          sidebarNav["CuentasPorCobrar"].push(Item);
        } else if (Item.menu_id === Menus[7].id) {
          sidebarNav["Gastos"].push(Item);
        } else if (Item.menu_id === Menus[8].id) {
          sidebarNav["Pagos"].push(Item);
        } else if (Item.menu_id === Menus[9].id) {
          sidebarNav["Bancos"].push(Item);
        }
      });

    this.setState({ menu: sidebarNav, showMenu: true });
  };

  renderName = () => {
    const { user } = this.props;
    let name = "";
    if (user) {
      if (user.person) {
        name = user.person.first_name;
      }
      return name;
    }
  };
  renderCode = () => {
    const { user } = this.props;
    let code;
    if (user) {
      if (user.person) {
        code = user.person.code;
      }
      return code;
    }
  };

  logout = () => {
    Globals.confirm("¿Desea cerrar sesión?", () => {
      Globals.setLoading();
      setTimeout(() => {
        this.props.dispatch({
          type: "QUIT_USER"
        });

        if (this.props.admin) {
          this.props.dispatch({
            type: "QUIT_ADMIN"
          });
        }

        this.props.dispatch({
          type: "QUIT_SIDEBAR"
        });

        this.props.dispatch({
          type: "REMOVE_TOKEN"
        });

        this.props.history.push("/login");

        Globals.quitLoading();
      }, 1500);
    });
  };

  backAdmin = () => {
    let message = "¿Desea regresar como administrador?";
    if (this.props.admin.role === 2)
      message = "¿Desea regresar como superadministrador?";

    Globals.confirm(message, () => {
      Globals.setLoading();

      this.props.dispatch({
        type: "SET_USER",
        payload: this.props.admin
      });

      setTimeout(() => {
        Sidebar.getModules(this.props.admin.role)
          .then(response => {
            this.props.dispatch({
              type: "SET_SIDEBAR",
              payload: response
            });

            if (this.props.admin.role === 2 || this.props.admin.role === 1) {
              this.props.history.push("/enterprises");
            }
          })
          .catch()
          .finally(() => {
            Globals.quitLoading();
          });
      }, 1000);
    });
  };

  searchPath = path => {
    let pathC = this.props.user.permissions.find(i => i.path === path);
    if (pathC) return true;
    else return false;
  };

  toggleSidebar = event => {
    let key = `${event.currentTarget.parentNode.id}Open`;
    this.setState({ [key]: !this.state[key] });
  };
  render() {
    let leftOpen = this.state.leftOpen ? "open" : "closed";
    // let rightOpen = this.state.rightOpen ? "open" : "closed";

    const { sidebar, history, user, admin } = this.props;
    const { menu, showMenu } = this.state;
    let entAdmin = false;
    if (admin && admin.id !== user.id) entAdmin = true;
    let EntLogOut;

    if (entAdmin) {
      EntLogOut = (
        <button
          className="btn btn-default btn-logout-admin"
          onClick={this.backAdmin}
        >
          <i className="fa fa-sign-out"></i>
        </button>
      );
    }
    return (
      <React.Fragment>
        <nav
          className="navbar navbar-expand-md bg-dark navbar-dark fixed-top justify-content-end"
          id="content"
        >
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#collapsibleNavbar"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="collapsibleNavbar">
            <ul className="navbar-nav">
              {sidebar.length > 0
                ? sidebar.map((sidebarItem, key) => {
                    return (
                      <li
                        key={key}
                        className={
                          history.location.pathname === sidebarItem.path
                            ? "active"
                            : ""
                        }
                      >
                        <Link to={sidebarItem.path}>
                          {sidebarItem.name}
                          {sidebarItem.name === "Chats" &&
                            this.props.support > 0 && (
                              <span className="badge">
                                {this.props.support}
                              </span>
                            )}
                        </Link>
                      </li>
                    );
                  })
                : null}

              <li className="nav-item">
                <button
                  data-toggle="collapse"
                  data-target=".navbar-collapse.show"
                  className="btn nav-link"
                  onClick={this.logout}
                >
                  Cerrar Sesión
                </button>
              </li>
            </ul>
          </div>
        </nav>

        <div id="layout">
          <div id="left" className={leftOpen}>
            <div className="icon mt-2 ml-2" onClick={this.toggleSidebar}>
              &equiv;
            </div>

            <div className={`sidebar ${leftOpen}`}>
              <div className="container-menu">
                <div className="menu">
                  <Scrollbars
                    renderTrackHorizontal={({ style, ...props }) => (
                      <div
                        {...props}
                        style={{ ...style, backgroundColor: " blue " }}
                      />
                    )}
                  >
                    <button
                      className="btn btn-default btn-logout"
                      onClick={this.logout}
                    >
                      <i className="fa fa-power-off"></i>
                    </button>
                    {EntLogOut}
                    <div className="container-user mt-5">
                      {/* <img src={ ENV.BasePublic + 'img/users/' + this.props.user.photo.file } /> */}
                      <h2>
                        {this.renderName()} <br />{" "}
                        {this.renderCode()
                          ? "Codigo: " + this.renderCode()
                          : ""}
                      </h2>
                    </div>

                    <ul className="menu ">
                      {showMenu && menu["Inventario"].length > 0 ? (
                        <li>
                          <a
                            href="#inventory"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[1].name}
                          </a>
                          <ul
                            className="collapse navbar-collapse"
                            id="inventory"
                          >
                            {menu["Inventario"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}
                      
                      {showMenu && menu["Ventas"].length > 0 ? (
                        <li>
                          <a
                            href="#sales"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[2].name}
                          </a>
                          <ul className="collapse navbar-collapse" id="sales">
                            {menu["Ventas"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}

                      {showMenu && menu["Compras"].length > 0 ? (
                        <li>
                          <a
                            href="#purchase"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[3].name}
                          </a>
                          <ul
                            className="collapse navbar-collapse"
                            id="purchase"
                          >
                            {menu["Compras"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}

                      {showMenu && menu["Utilidades"].length > 0 ? (
                        <li>
                          <a
                            href="#utilities"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[4].name}
                          </a>
                          <ul
                            className="collapse navbar-collapse"
                            id="utilities"
                          >
                            {menu["Utilidades"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name === "Usuarios" &&
                                    this.props.user.role === 1
                                      ? "Multiempresas"
                                      : Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}

                      {showMenu && menu["Reportes"].length > 0 ? (
                        <li>
                          <a
                            href="#reports"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[5].name}
                          </a>
                          <ul className="collapse navbar-collapse" id="reports">
                            {menu["Reportes"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}

                      {/*<ul class="colapse">
                <a
                  href="#top"
                  data-toggle="sidebar-colapse"
                  class="bg-dark list-group-item list-group-item-action d-flex align-items-center pt-1 pb-1"
                >
                  <div class="d-flex w-100 justify-content-start align-items-center">
                    
                      <span id="collapse-icon">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                      </span>
                      <span id="collapse-text" class="menu-collapsed pl-2">
                        Ocultar
                      </span>
                  </div>
                </a>
              </ul>*/}

                      {showMenu && menu["CuentasPorCobrar"].length > 0 ? (
                        <li>
                          <a
                            href="#accounts_to_pay"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[6].name}
                          </a>
                          <ul
                            className="collapse navbar-collapse"
                            id="accounts_to_pay"
                          >
                            {menu["CuentasPorCobrar"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}

                      {showMenu && menu["Gastos"].length > 0 ? (
                        <li>
                          <a
                            href="#expenses"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[7].name}
                          </a>
                          <ul
                            className="collapse navbar-collapse"
                            id="expenses"
                          >
                            {menu["Gastos"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}
                      
                      {showMenu && menu["Pagos"].length > 0 ? (
                        <li>
                          <a
                            href="#payments"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[8].name}
                          </a>
                          <ul
                            className="collapse navbar-collapse"
                            id="payments"
                          >
                            {menu["Pagos"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}

                      {showMenu && menu["Bancos"].length > 0 ? (
                        <li>
                          <a
                            href="#banks"
                            data-toggle="collapse"
                            aria-expanded="false"
                            className="dropdown-toggle"
                          >
                            {Menus[9].name}
                          </a>
                          <ul
                            className="collapse navbar-collapse"
                            id="banks"
                          >
                            {menu["Bancos"].map((Item, key) => {
                              return (
                                <li
                                  key={key}
                                  className={
                                    history.location.pathname === Item.path
                                      ? "active"
                                      : ""
                                  }
                                >
                                  <Link className="nav-link" to={Item.path}>
                                    {Item.name}
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : null}
                    </ul>
                  </Scrollbars>
                </div>
              </div>
            </div>
          </div>
          {/*<ul class="colapse pt-2 pl-1 ">
            <a>
              <div class="btn btn-dark">
                <span id="collapse-icon">
                  <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                </span>
              </div>
            </a>
          </ul>*/}
          <div className="container-router">
            <div className="container-fluid">{this.props.children}</div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(state => {
  return {
    user: state.user,
    support: state.support,
    admin: state.admin,
    sidebar: state.sidebar
  };
})(Menu);
