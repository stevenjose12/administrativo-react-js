import axios from "../../utils/axios";

class Model {
  static async createModel(param) {
    return new Promise((resolve, reject) => {
      axios.post("admin/models/model", param).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }
}

export default Model;
