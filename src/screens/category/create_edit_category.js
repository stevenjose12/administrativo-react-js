import React from "react";
import { Button, Select, Input } from "../../components";
import { axios, Globals } from "../../utils";

class CreateEditCategory extends React.Component {
  state = {
    form: {},
    users: [],
    textButton: "Crear"
  };
  componentDidMount() {
    if (this.props.edit) {
      this.edit();
      this.getEnterprises();
    } else {
      this.getEnterprises();
    }
  }
  submit = async () => {
    let param = { ...this.state.form };
    Globals.setLoading();
    axios
      .upload(
        this.props.edit ? "admin/categories/edit" : "admin/categories/create",
        param
      )
      .then(res => {
        if (res.data.result) {
          this.setState({
            form: {}
          });
          Globals.showSuccess(res.data.msg);
          this.props.onClose();
        } else {
          Globals.showError(res.data.msg);
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  edit = async () => {
    await this.setState({
      form: {
        creator_id: this.props.edit.element.creator_id,
        code: this.props.edit.element.code,
        name: this.props.edit.element.name,
        user_id: this.props.edit.element.user_id,
        id: this.props.edit.element.id
      },
      textButton: "Editar"
    });
  };

  change = e => {
    if (e.target.name === "code") {
      e.target.value = e.target.value.toUpperCase();
    }
    let user_id = "";
    if (this.props.user.role === 1 || this.props.user.role === 2) {
      user_id = this.props.user.id;
    } else {
      user_id =
        this.props.user.role === 4
          ? this.props.user.enterprise_users.enterprise_id
          : this.props.user.id;
    }
    this.setState({
      form: {
        user_id: user_id,
        creator_id: this.props.user.id,
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  getEnterprises = async () => {
    Globals.setLoading();
    axios
      .get("admin/warehouses/enterprises")
      .then(res => {
        if (res.data.result) {
          this.setState({
            users: res.data.users
          });
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  render() {
    const admin =
      this.props.user.role === 1 || this.props.user.role === 2 ? true : false;
    let selectEnterprise;
    if (admin) {
      selectEnterprise = (
        <Select
          color="gray"
          name="user_id"
          label="Empresa"
          defaultname="Seleccione"
          disabledFirst={false}
          onChange={this.change}
          value={this.state.form.user_id}
          options={this.state.users.map(i => {
            return {
              value: i.id,
              label: i.name
            };
          })}
        />
      );
    }
    return (
      <div className="text-center container-create-edit-user">
        <Input
          color="gray"
          value={this.state.form.name}
          name="name"
          label="Nombre"
          onChange={this.change}
        />
        <Input
          color="gray"
          value={this.state.form.code}
          name="code"
          label="Código"
          onChange={this.change}
        />
        {selectEnterprise}
        {/* <File
                    placeholder={this.props.edit ? 'Cambiar imagen de perfil (Opcional) ' : "Agregar Imagen de perfil"}
                    placeholdersuccess="Imagen de perfil Agregada"
                    showcheck={ true.toString() }
                    onChange={ this.change }
                    name="image"
                    value={ this.state.form.image }
                    inputstyle={{
                        display: 'contents'
                    }}
                    className="btn-product" /> */}
        <div id="button">
          <Button block="true" type="button" onClick={() => this.submit()}>
            {this.state.textButton}
          </Button>
        </div>
      </div>
    );
  }
}
export default CreateEditCategory;
