const admin = (state = false, action) => {
  switch (action.type) {
    case "SET_ADMIN":
      return action.payload;
    case "QUIT_ADMIN":
      return false;
    default:
      return state;
  }
};

export default admin;
