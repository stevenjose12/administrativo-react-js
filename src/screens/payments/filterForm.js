import React from "react";

import { Select, Button, Input } from "../../components";
import { Constants, Format, Globals } from "../../utils";
import { Warehouse } from "../../services";
import constants from "../../utils/constants";

class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        branch_id: [],
        warehouse_id: Globals.getAssignedWarehouses(props.user),
        zone_id: 0,
        client: 0,
        seller: 0,
        user_id: Globals.getUserId(props.user)
      },
      warehouses: []
    };
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.form.branch_id !== this.state.form.branch_id &&
      this.state.form.branch_id
    ) {
      this.maybeLoadWarehouses();
    }
  }

  maybeLoadWarehouses = () => {
    const { form } = this.state;
    const { user } = this.props;
    const warehouse_id = Globals.getAssignedWarehouses(user)

    const userId = (user.enterprise_users || {}).enterprise_id || user.id;

    Warehouse.getWarehouseByBranch({
      user_id: userId,
      branch_id: form.branch_id
    }).then(response => {
      let warehouses_filtered = response.filter(
        ({ deleted_at }) => !deleted_at
      );

      const warehousesMap = Format.rawWarehouse(
        warehouses_filtered,
        user.assigned_warehouses,
        user.role
      )
        .filter(({ status }) => status === Constants.STATUS_ACTIVE)
        .map(({ id, name }) => ({
          value: id,
          label: name
        }));

      this.setState(state => ({
        form: {
          ...state.form,
          warehouse_id: warehouse_id
        },
        warehouses: warehousesMap
      }));
    });
  };

  handleChange = (emitter, string) => {
    let { name, value } = emitter.target;

    if(name === 'branch_id' || name === 'warehouse_id') {
      value = [parseInt(value)]
    }

    this.setState(state => ({
      form: {
        ...state.form,
        [name]: value
      }
    }));
  };

  render() {
    
    const { form, warehouses } = this.state;
    const { branches, sellers, customers, zones } = this.props;

    return (
      <form
        onSubmit={emitter => {
          emitter.preventDefault();
          this.props.submitted(form);
        }}
      >
        <div className="row">
          <div className="col-md">
            <Select
              color="white"
              name="branch_id"
              label="Sucursal"
              style={{ textTransform: "capitalize" }}
              onChange={emitter => this.handleChange(emitter, null)}
              value={form.branch_id}
              options={branches}
            />
          </div>
          <div className="col-md">
            <Select
              color="white"
              name="warehouse_id"
              label="Almacenes"
              style={{ textTransform: "capitalize" }}
              onChange={emitter => this.handleChange(emitter, null)}
              value={form.warehouse_id}
              options={[...warehouses, { value: 0, label: "Todos" }].reverse()}
            />
          </div>
          {sellers && sellers.length > 0 && this.props.user.role !== constants.ROLE_SELLER && (
            <div className="col-md">
              <Select
                color="white"
                name="seller"
                label="Vendedores"
                style={{ textTransform: "capitalize" }}
                onChange={emitter => this.handleChange(emitter, null)}
                value={form.seller}
                options={[...sellers, { value: 0, label: "Todos" }].reverse()}
              />
            </div>
          )}
        </div>
        <div className="row">
          <div className="col-md">
            <Select
              color="white"
              name="client"
              label="Clientes"
              style={{ textTransform: "capitalize" }}
              onChange={emitter => this.handleChange(emitter, null)}
              value={form.client}
              options={[...customers, { value: 0, label: "Todos" }].reverse()}
            />
          </div>
          <div className="col-md">
            <Select
              color="white"
              name="zone_id"
              label="Zona"
              style={{ textTransform: "capitalize" }}
              onChange={emitter => this.handleChange(emitter, null)}
              value={form.zone_id}
              options={zones}
            />
          </div>
          <div className="col-md">
            <div className="form-group">
              <Button
                className="btn-block btn-align-bottom"
                onClick={this.load}
              >
                Filtrar
              </Button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default Filter;
