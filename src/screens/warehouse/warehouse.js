import React from "react";
import { connect } from "react-redux";
import {
  Table,
  Pagination,
  Button,
  Icon,
  Modal,
  Select,
  Input
} from "../../components";
import { axios, Globals, Format } from "../../utils";

// Components
import Menu from "../menu";
import CreaEditWarehouse from "./create_edit_warehouse";
import ViewWarehouse from "./view_warehouse";

class Warehouse extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      header: ["Nombre", "Código", "Sucursal", "Status", "Acciones"],
      page: 1,
      last_page: 1,
      data: [],
      branches: [],
      warehouse: null,
      view: false,
      list_status: [
        { value: "", label: "Todos" },
        { value: 1, label: "Activos" },
        { value: 2, label: "Suspendidos" },
        { value: 3, label: "Eliminados" }
      ],
      form: {
        status: "",
        search: "",
        enterprise_id: Globals.getEnterpriseId(props.user),
        user_id: props.user.id,
        role: props.user.role
      }
    };
  }

  componentDidMount() {
    this.load();
  }

  load = () => {
    const { page, form } = this.state;
    const { user } = this.props;

    form.branch_id = user.assigned_warehouses.map(Item => {
      return Item.branch_id;
    });

    form.branch_id = user.role === 3 ? "" : form.branch_id;

    axios
      .post("admin/warehouses/get?page=" + page, form)
      .then(({ data }) => {
        if (data.result) {
          const branchesMap = Format.rawBranches(
            data.branches,
            user.assigned_warehouses,
            user.role
          );

          this.setState({
            last_page: data.warehouses.last_page,
            data: data.warehouses.data,
            branches: branchesMap
          });
        }
      })
      .catch(err => {
        Globals.showError();
      });
  };

  close = () => {
    this.setState(
      {
        create: false,
        view: false,
        edit: null,
        warehouse: null
      },
      () => {
        this.load();
      }
    );
  };

  suspend = (item, i) => {
    let text = item.status === 1 ? "¿Desea suspender el almacen " : "¿Desea activar el almacen "
    Globals.confirm(text + item.name + "?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/warehouses/suspend", { id: item.id })
        .then(res => {
          if (res.data.result) {
            Globals.showSuccess(res.data.msg);
            this.load();
          } else {
            Globals.showError();
          }
        })
        .catch(err => {
          Globals.showError();
        })
        .then(() => {
          Globals.quitLoading();
        });
    });
  };

  delete = (item, i) => {
    Globals.confirm("Se evaluara si el almacén no tiene existencia de productos para ser eliminado, ¿Desea continuar?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/warehouses/delete", { id: item.id })
        .then(res => {
            Globals.showSuccess(res.data.msg);
            this.load();
        })
        .catch(err => {
          Globals.showError(err.response.data.msg);
        })
        .then(() => {
          Globals.quitLoading();
        });
    }, "¿Desea eliminar el almacen: " + item.name + "?");
  };
  change = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };
  render() {
    const { view, create } = this.state;

    return (
      <Menu history={this.props.history}>
        {view && (
          <Modal title="Ver Almacen" onClose={this.close} visible>
            <ViewWarehouse warehouse={this.state.warehouse} />
          </Modal>
        )}

        {create && (
          <Modal
            title={this.state.edit ? "Editar Almacen" : "Nuevo Almacen"}
            onClose={this.close}
            visible
          >
            <CreaEditWarehouse
              user={this.props.user}
              edit={this.state.edit}
              branches={this.state.branches}
              onClose={this.close}
            />
          </Modal>
        )}

        <div id="home">
          <div className="row">
            <div className="col col-md-5">
              <Input
                color="white"
                name="search"
                label="Búsqueda"
                onChange={this.change}
                value={this.state.search}
                placeholder="Buscar por Nombre, Código o Sucursal"
              />
            </div>
            <div className="col col-md-5">
              <Select
                color="white"
                name="status"
                label="Estatus"
                defaultname="Seleccione"
                onChange={this.change}
                value={this.state.status}
                options={this.state.list_status.map(i => {
                  return {
                    value: i.value,
                    label: i.label
                  };
                })}
              />
            </div>
            <div className="col col-md">
              <Button className="btn-align-bottom" onClick={this.load}>
                Filtrar
              </Button>
            </div>
          </div>
          <div className="row">
            <div className="col col-md">
              <Table
                data={this.state.data.length}
                title="Almacenes"
                header={this.state.header}
                right={
                  <Button
                    title="Agregar Almacenes"
                    outline="true"
                    small="true"
                    onClick={() => {
                      this.setState({
                        create: true
                      });
                    }}
                  >
                    <Icon name="plus" />
                  </Button>
                }
              >
                {this.state.data.map((i, index) => {
                  return (
                    <tr key={index}>
                      <td>{i.name}</td>
                      <td>{i.code}</td>
                      <td>
                        {i.branch
                          ? i.branch.name
                          : "No posee sucursal asignada"}
                      </td>
                      {!i.deleted_at && (
                        <td style={{ color: Globals.getStatus(i.status).color }}>
                          {Globals.getStatus(i.status).text}
                        </td>
                      )}
                      {i.deleted_at && (
                        <td style={{ color: Globals.getStatus(3).color}}>{Globals.getStatus(3).text}</td>
                      )}
                      <td>
                        <Button
                          title="Ver"
                          small="true"
                          onClick={() => {
                            this.setState({
                              warehouse: i,
                              view: true
                            });
                          }}
                        >
                          <Icon name="eye" />
                        </Button>
                        {!i.deleted_at && (
                          <Button
                            title="Editar"
                            color="primary"
                            small="true"
                            onClick={() =>
                              this.setState({
                                create: true,
                                edit: {
                                  element: i,
                                  index: index
                                }
                              })
                            }
                          >
                            <Icon name="edit" />
                          </Button>
                        )}
                        {(i.status === 1 || i.status === 2) && !i.deleted_at && (
                          <Button
                            color="info"
                            title={i.status === 1 ? 'Suspender' : 'Activar'}
                            small="true"
                            onClick={() => this.suspend(i, index)}
                          >
                            <Icon name={i.status === 1 ? 'close' : 'check'} />
                          </Button>
                        )}
                        {!i.deleted_at && this.props.user.role === 3 && (
                          <Button
                            color="red"
                            title="Eliminar"
                            small="true"
                            onClick={() => this.delete(i, index)}
                          >
                            <Icon name="trash" />
                          </Button>
                        )}
                      </td>
                    </tr>
                  );
                })}
              </Table>
            </div>
          </div>
          <div className="row my-3">
            <div className="col col-md">
              <Pagination
                pages={this.state.last_page}
                active={this.state.page}
                onChange={page => {
                  this.setState(
                    {
                      page: page
                    },
                    () => {
                      this.load();
                    }
                  );
                }}
              />
            </div>
          </div>
        </div>
      </Menu>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(Warehouse);
