import React from 'react';
import Menu from '../menu';
import { Table, Pagination, Button, Icon, Modal, Select, Input } from '../../components';
import { axios, Globals, Colors } from '../../utils';
import CreateEditModerator from './create_edit_moderator';
// import socket from '../../utils/socket';
import ViewModerator from './view_moderator';
import { connect } from 'react-redux';
class Moderators extends React.Component {
	state={
		header: [
			'Nombre',
			'Email',
			'Teléfono',
            'Cédula',
            'Status',
			'Acciones'
		],
		page: 1,
		last_page: 1,
		data: [],
		user: null,
		view: false,
		list_status: [
			{ value: '', label: 'Todos' },
			{ value: 1, label: 'Activos' },
			{ value: 2, label: 'Suspendidos' },
		],
		form:{
			status: '',
			search: ''
		}
	}
	componentDidMount(){
		this.load()
	}
	load = () => {
		let param = { ... this.state.form };
		if(this.props.user.level === 2) param.moderator = this.props.user.id
		Globals.setLoading();
		axios.post('admin/moderators/get?page=' + this.state.page, param)
			.then(res => {
				if (res.data.result) {
					this.setState({
						last_page: res.data.moderators.last_page,
						data: res.data.moderators.data
					});
				}
			})
			.catch(err => {
				Globals.showError();
			})
			.then(() => {
				Globals.quitLoading();
			});
    }
    
	close = async () => {
		await this.setState({
			create: false,
			view: false,
			edit: null,
			user: null
		});
		await this.load()
	}

	delete = (item, i) => {
		Globals.confirm('¿Desea eliminar el Administrador: ' + item.person.name + ' '+item.person.lastname+ '?',() => {
			Globals.setLoading("Guardando...");
			axios.post('admin/moderators/delete',{ id: item.id })
				.then(res => {
					if (res.data.result) {
                        // socket.emit('disable-user',{id: item.id});
						Globals.showSuccess(res.data.msg);
						this.load();
					}else{
						Globals.showError();
					}
				})
				.catch(err => {
					Globals.showError();
				})
				.then(() => {
					Globals.quitLoading();
				});
		});
    }
    
    suspend = (item, i) =>{
		Globals.confirm('¿Desea suspender a ' + item.person.name + ' ' + item.person.lastname + '?',() => {
			Globals.setLoading("Guardando...");
			axios.post('admin/moderators/suspend',{ id: item.id })
				.then(res => {
					if (res.data.result) {
						// socket.emit('disable-user',{id: item.id});
						Globals.showSuccess(res.data.msg);
						this.load();
					}else{
						Globals.showError();
					}
				})
				.catch(err => {
					Globals.showError();
				})
				.then(() => {
					Globals.quitLoading();
				});
		});
    }
    
    activate = (item, i) =>{
		Globals.confirm('¿Desea activar la cuenta de: ' + item.person.name + ' ' + item.person.lastname + '?',() => {
			Globals.setLoading("Guardando...");
			axios.post('admin/moderators/activate',{ id: item.id })
				.then(res => {
					if (res.data.result) {
						// socket.emit('disable-user',{id: item.id});
						Globals.showSuccess(res.data.msg);
						this.load();
					}else{
						Globals.showError();
					}
				})
				.catch(err => {
					Globals.showError();
				})
				.then(() => {
					Globals.quitLoading();
				});
		});
	}

    getStatus = (status) => {
		let respuesta = "";
		switch(status) {
			case 0:
				respuesta = {
					text: "Nuevo",
					color: Colors.red
				}
				break;
				
			case 1:
				respuesta = {
					text: "Activo",
					color: Colors.green
				}
				break;

			case 2:
				respuesta = {
					text: "Suspendido",
					color: Colors.orange
				}
				break;
		}
		return respuesta;
	}

	change = e => {
		this.setState({
			form: {
				...this.state.form,
				[e.target.name]: e.target.value
			}
        });
    }

	render() {
		return (
            <Menu history={ this.props.history }>
                <Modal
                    title="Ver Administrador" 
                    onClose={ this.close }
                    visible={ this.state.view }>
                    <ViewModerator data={ this.state.product } />
                </Modal>
				<Modal
					title={this.state.edit ? 'Editar Administrador' : "Nuevo Administrador"}
					onClose={ this.close } 
					visible={ this.state.create }>
					<CreateEditModerator
						edit={this.state.edit}
						onClose={ this.close } />
				</Modal>
				<div id="products">
					<div className="row">
						<div className="col-md-7">
							<Input
								color="white" 
								name="search" 
								label="Búsqueda"
								onChange={ this.change } 
								value={ this.state.search }
								placeholder="Buscar por Nombre o cédula" />
						</div>
						<div className="col-md-3">
							<Select
								color="white"
								name="status"
								label="Status"
								defaultname="Todos"
								onChange={ this.change }
								value={ this.state.status }
								options={ this.state.list_status.map(i => {
									return {
										value: i.value,
										label: i.label
									}
								}) } />
						</div>
						<div className="col-md-2">
							<Button className="btn-align-bottom" onClick={ this.load }>
								Filtrar
							</Button>
						</div>
					</div>
					<Table data={ this.state.data.length } title="Administradores" header={ this.state.header } right={
								<Button outline="true" small="true" onClick={ () => {
									this.setState({
										create: true
									});
								}}>
									<Icon name="plus" />
								</Button>
							}>
							{ this.state.data.map((i,index) => {
								return (
									<tr key={ index }>
										<td>{ i.person.name+' '+i.person.lastname }</td>
										<td>{ i.email }</td>
										<td>{ i.person.phone }</td>
										<td>{ i.person.document }</td>
                                        <td style={ {color: this.getStatus(i.status).color }}>
											{ this.getStatus(i.status).text }
										</td>
										<td>
											<Button title="Ver" small="true" onClick={ () => {
												this.setState({
													product: i,
													view: true
												});
											} }>
												<Icon name="eye" />												
											</Button>
											<Button title="Editar" color="primary" small="true" onClick={ () => 
												this.setState({
													create: true,
													edit: {
														element: i,
														index: index,
													}
												}) }>
												<Icon name="edit" />												
											</Button>
                                            { i.status == 2 && <Button color="info" title="Activar" small="true" onClick={ () => {
												this.activate(i,index);
											} }>
												<Icon name="check" />
											</Button> }
                                            { i.status == 1 && (
												<Button color="info" title="Suspender" small="true" onClick={ () => this.suspend(i,index) }>
													<Icon name="close" />												
												</Button>
											) }
											<Button color="red" title="Eliminar" small="true" onClick={ () => this.delete(i,index) }>
												<Icon name="trash" />												
											</Button>
										</td>
									</tr>
								)
							}) }
						</Table>

						<Pagination 
							pages={ this.state.last_page } 
							active={ this.state.page }
							onChange={ async page => {
								await this.setState({
									page: page
								});
								this.load();
						} } />
				</div>
			</Menu>
		)
	}
}

export default connect(state => {
	return {
		user: state.user
	}
})(Moderators);