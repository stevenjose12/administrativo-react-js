import React from "react";
import { Button, Input, Select, File } from "../../components";
import { Globals, Constants } from "../../utils";
import { Zones, Module, Seller, Warehouse } from "../../services";

class CreateEditSeller extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      isEdit: false,
      form: {
        id: "",
        name: "",
        first_name: "",
        last_name: "",
        phone: "",
        price_offer: 0,
        price_max: 0,
        document_type: "V",
        document: "",
        level: 4,
        role: 5,
        percentage: "",
        email: "",
        password: "",
        password_confirmation: "",
        branch_id: "",
        warehouse_id: "",
        creator_id: props.userId,
        type_percentage: 1,
        zone_id: "",
        image: "",
        modules: [],
        zones: []
      },
      errors: [],
      warehouses: [],
      modules: [],
      zones: [],
      textButton: "Crear"
    };
  }

  componentDidMount() {
    const { Item } = this.props;

    this.maybeLoadData();

    if (Item) {
      let documentIdentity = Item.person.identity_document.split("-");
      let userName = Item.name.split("-");

      const zones =
        Item.zones.map(({ id, name }) => ({
          value: id,
          label: name
        })) || [];

      const assignments = (Item || {}).assigned_warehouses || [];
      const modules = Item.modules_seller.map(({ id }) => id) || [];
      this.setState(
        state => ({
          isEdit: true,
          form: {
            ...state.form,
            id: Item.id,
            name: (userName || [])[1] || "",
            first_name: Item.person.first_name,
            last_name: Item.person.last_name,
            phone: Item.person.phone,
            document_type: documentIdentity[0] || "" || "V",
            document: documentIdentity[1] || "" || "",
            price_offer: (Item || {}).configuration_seller.price_offer,
            price_max: (Item || {}).configuration_seller.price_max,
            email: Item.email,
            password: "",
            password_confirmation: "",
            branch_id:
              assignments.length === 1
                ? assignments[0].branch_id
                : assignments.length > 1
                ? 0
                : "",
            warehouse_id:
              assignments.length === 1
                ? assignments[0].warehouse_id
                : assignments.length > 1
                ? 0
                : "",
            image: Item.person.avatar,
            percentage: (Item || {}).configuration_seller.percentage || "",
            type_percentage:
              (Item || {}).configuration_seller.type_percentage || 1,
            modules: modules,
            zones: zones
          },
          textButton: "Editar"
        }),
        () => this.maybeLoadWarehouses(true)
      );
    }
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.form.branch_id !== this.state.form.branch_id &&
      this.state.form.branch_id
    ) {
      this.maybeLoadWarehouses(false);
    }
  }

  maybeLoadData = () => {
    Promise.all([Zones.getZones(), Module.getModulesSeller()])
      .then(response => {
        const rawZones = response[0].map(({ id, name }) => ({
          value: id,
          label: name,
          visible: true
        }));

        this.setState({ zones: rawZones, modules: response[1] });
      })
      .catch(() => Globals.showError());
  };

  maybeLoadWarehouses = (setWarehouse = false) => {
    const { userId } = this.props;
    const { form } = this.state;

    Warehouse.getWarehouseByBranch({
      user_id: userId,
      branch_id: form.branch_id
    })
      .then(response => {
        const warehousesMap = response.map(({ id, name }) => ({
          value: id,
          label: name
        }));

        this.setState(state => ({
          form: {
            ...state.form,
            warehouse_id: setWarehouse ? form.warehouse_id : 0
          },
          warehouses: [{ value: 0, label: "Todos" }, ...warehousesMap]
        }));
      })
      .catch(() => Globals.showError());
  };

  handleChange = emitter => {
    const { name, value } = emitter.target;

    this.setState({
      form: {
        ...this.state.form,
        [name]: value
      }
    });
  };

  handleCheckbox = emitter => {
    const { name, value } = emitter.target;

    this.setState({
      form: {
        ...this.state.form,
        [name]: value === "0" ? 1 : 0
      }
    });
  };

  handleArrayChange = emitter => {
    const { name, value, checked } = emitter.target;

    switch (checked) {
      case true:
        this.setState(state => ({
          form: {
            ...state.form,
            [name]: [...state.form[name], parseInt(value)]
          }
        }));
        break;
      case false:
        this.setState(state => ({
          form: {
            ...state.form,
            [name]: state.form[name].filter(Item => Item !== parseInt(value))
          }
        }));
        break;

      default:
        break;
    }
  };

  assignZone = () => {
    const { zones, form } = this.state;

    const key = zones.findIndex(
      ({ value }) => value === parseInt(form.zone_id)
    );
    const newZones = [...zones];

    newZones[key] = {
      ...newZones[key],
      visible: false
    };

    this.setState(state => ({
      form: {
        ...state.form,
        zone_id: "",
        zones: [...state.form.zones, newZones[key]]
      },
      zones: newZones
    }));
  };

  removeZone = zoneId => {
    const { form, zones } = this.state;

    const stateZones = [...zones];
    const key = zones.findIndex(({ value }) => value === parseInt(zoneId));
    const newZones = form.zones.filter(({ value }) => value !== zoneId);

    stateZones[key] = {
      ...stateZones[key],
      visible: true
    };

    this.setState(state => ({
      form: {
        ...state.form,
        zones: newZones
      },
      zones: stateZones
    }));
  };

  handleSubmit = emitter => {
    emitter.preventDefault();

    const { form, submitted, isEdit } = this.state;

    if (submitted) {
      return;
    }

    if (form.modules.length <= 0) {
      Globals.showError("¡Debe agregar al vendedor al menos un modulo!");
      return;
    } else if (form.zones.length <= 0) {
      Globals.showError("¡Debe agregar al vendedor al menos una zona!");
      return;
    }

    // Vaciando los campos de errores al enviar solicitud
    this.setState({ submitted: true, errors: [] });

    const newForm = {
      ...form,
      modules: JSON.stringify(form.modules),
      zones: JSON.stringify(form.zones)
    };

    switch (isEdit) {
      case true:
        Seller.update(newForm)
          .then(() => {
            Globals.showSuccess("¡El vendedor se ha editado exitosamente!");
            this.props.onClose();
          })
          .catch(error => {
            if ((error || {}).message) {
              Globals.showError(error.message);
            }

            this.setState({ submitted: false, errors: error });
          });
        break;
      default:
        Seller.create(newForm)
          .then(() => {
            Globals.showSuccess("¡El vendedor se ha creado exitosamente!");
            this.props.onClose();
          })
          .catch(error => {
            if ((error || {}).message) {
              Globals.showError(error.message);
            }

            this.setState({ submitted: false, errors: error });
          });
        break;
    }
  };

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback my-2 text-left">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  render() {
    const { submitted, warehouses, form, modules, zones, isEdit } = this.state;
    const { branches } = this.props;

    const showPrices = form.modules.includes(22);

    return (
      <form onSubmit={this.handleSubmit}>
        <div className="text-center container-create-edit-seller">
          <div className="row">
            <div className="col col-md">
              <Input
                type="text"
                color="gray"
                value={this.state.form.name}
                name="name"
                label="Nombre de usuario"
                error={`${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                invalidfeedback={this.renderErrorFor("name")}
                onChange={this.handleChange}
              />
            </div>
            <div className="col col-md">
              <Input
                type="text"
                color="gray"
                value={this.state.form.first_name}
                name="first_name"
                error={`${this.hasErrorFor("first_name") ? "is-invalid" : ""}`}
                invalidfeedback={this.renderErrorFor("first_name")}
                label="Nombre y Apellido"
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md">
              <Input
                color="gray"
                value={this.state.form.phone}
                name="phone"
                type="text"
                maxLength={11}
                onKeyPress={e => {
                  Globals.soloNumeros(e);
                }}
                label="Teléfono"
                error={`${this.hasErrorFor("phone") ? "is-invalid" : ""}`}
                invalidfeedback={this.renderErrorFor("phone")}
                onChange={this.handleChange}
              />
            </div>
            <div className="col-md-6">
              <div style={{ display: "flex" }}>
                <Select
                  color="gray"
                  label="Tipo"
                  name="document_type"
                  options={Constants.TYPE_DOCUMENTS}
                  value={this.state.form.document_type}
                  error={`${
                    this.hasErrorFor("document_type") ? "is-invalid" : ""
                  }`}
                  invalidfeedback={this.renderErrorFor("document_type")}
                  onChange={this.handleChange}
                />
                <Input
                  color="gray"
                  value={this.state.form.document}
                  type="text"
                  name="document"
                  label="Cédula"
                  maxLength={this.state.form.document_type === "J" ? 9 : 8}
                  onKeyPress={e => {
                    Globals.soloNumeros(e);
                  }}
                  error={`${this.hasErrorFor("document") ? "is-invalid" : ""}`}
                  invalidfeedback={this.renderErrorFor("document")}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col col-md">
              <Select
                color="gray"
                name="branch_id"
                label="Sucursales"
                defaultname="Seleccione"
                onChange={this.handleChange}
                value={this.state.form.branch_id}
                invalidfeedback={this.renderErrorFor("branch_id")}
                error={`${this.hasErrorFor("branch_id") ? "is-invalid" : ""}`}
                options={branches}
              />
            </div>
            <div className="col col-md">
              <Select
                color="gray"
                name="warehouse_id"
                label="Almacen"
                onChange={this.handleChange}
                value={this.state.form.warehouse_id}
                invalidfeedback={this.renderErrorFor("warehouse_id")}
                error={`${
                  this.hasErrorFor("warehouse_id") ? "is-invalid" : ""
                }`}
                options={warehouses.map(({ value, label }) => ({
                  value: value,
                  label: label
                }))}
              />
            </div>
          </div>
          <div className="row">
            <div className="col col-md">
              <Input
                color="gray"
                type="email"
                value={this.state.form.email}
                name="email"
                label="Email"
                error={`${this.hasErrorFor("email") ? "is-invalid" : ""}`}
                invalidfeedback={this.renderErrorFor("email")}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md">
              <Input
                color="gray"
                value={this.state.form.password}
                name="password"
                type="password"
                autoComplete="off"
                label="Contraseña"
                error={`${this.hasErrorFor("password") ? "is-invalid" : ""}`}
                invalidfeedback={this.renderErrorFor("password")}
                onChange={this.handleChange}
              />
            </div>
            <div className="col-md">
              <Input
                color="gray"
                value={this.state.form.password_confirmation}
                name="password_confirmation"
                type="password"
                autoComplete="off"
                label="Confirmar Contraseña"
                error={`${
                  this.hasErrorFor("password_confirmation") ? "is-invalid" : ""
                }`}
                invalidfeedback={this.renderErrorFor("password_confirmation")}
                onChange={this.handleChange}
              />
            </div>
          </div>
        </div>

        {modules.length > 0 && (
          <div className="row">
            <div className="col-md">
              <div className="form-group">
                <table className="table table-bordered table-products table-sm">
                  <thead>
                    <tr>
                      <td className="text-center" colSpan="2">
                        MODULOS
                      </td>
                    </tr>
                    <tr style={{ whiteSpace: "nowrap" }}>
                      <td>Modulo</td>
                      <td className="text-center">Acceso</td>
                    </tr>
                  </thead>
                  <tbody>
                    {modules.map((Item, key) => {
                      return (
                        <tr key={key}>
                          <td>{Item.name}</td>
                          <td className="text-center">
                            <input
                              type="checkbox"
                              title={Item.name}
                              value={Item.id}
                              name="modules"
                              defaultChecked={
                                this.state.form.modules.includes(Item.id)
                                  ? "checked"
                                  : null
                              }
                              onClick={this.handleArrayChange}
                            />
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        )}

        {showPrices && (
          <div className="row text-center">
            <div className="col col-md">
              <div className="form-group form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  name="price_offer"
                  id="price_offer"
                  defaultChecked={
                    this.state.form.price_offer ? "checked" : null
                  }
                  onClick={this.handleCheckbox}
                  value={form.price_offer}
                />
                <label className="form-check-label" htmlFor="price_offer">
                  PRECIO OFERTA
                </label>
              </div>
            </div>
            <div className="col col-md">
              <div className="form-group form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  name="price_max"
                  id="price_max"
                  defaultChecked={this.state.form.price_max ? "checked" : null}
                  onClick={this.handleCheckbox}
                  value={form.price_max}
                />
                <label className="form-check-label" htmlFor="price_max">
                  PRECIO MAX
                </label>
              </div>
            </div>
          </div>
        )}

        {showPrices && (
          <React.Fragment>
            <div className="row text-center">
              <div className="col-md">
                <div className="form-group form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="type_percentage"
                    onChange={this.handleChange}
                    defaultChecked={
                      this.state.form.type_percentage === Constants.TYPE_PRODUCT
                        ? "checked"
                        : null
                    }
                    value={1}
                  />
                  <label className="form-check-label">
                    Descuento por producto
                  </label>
                </div>
              </div>
              <div className="col-md">
                <div className="form-group form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="type_percentage"
                    onChange={this.handleChange}
                    defaultChecked={
                      this.state.form.type_percentage === Constants.TYPE_SALE
                        ? "checked"
                        : null
                    }
                    value={2}
                  />
                  <label className="form-check-label">
                    Descuento por venta
                  </label>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md">
                <Input
                  color="gray"
                  value={this.state.form.percentage}
                  name="percentage"
                  type="text"
                  autoComplete="off"
                  label="Margen de Descuento"
                  placeholder="Ej: 30"
                  maxLength={3}
                  onKeyPress={e => {
                    Globals.soloNumeros(e);
                  }}
                  error={`${
                    this.hasErrorFor("percentage") ? "is-invalid" : ""
                  }`}
                  invalidfeedback={this.renderErrorFor("percentage")}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </React.Fragment>
        )}

        {zones.length > 0 && (
          <div className="row">
            <div className="col col-md">
              <Select
                color="gray"
                name="zone_id"
                label="Zona"
                onChange={this.handleChange}
                value={this.state.form.zone_id}
                options={zones.filter(
                  ({ value, visible }) =>
                    visible &&
                    !form.zones.some(
                      ({ value: formValue }) => formValue === value
                    )
                )}
              />
              <div id="button">
                <Button
                  block="true"
                  disabled={this.state.form.zone_id ? "" : "disabled"}
                  type="button"
                  onClick={() => this.assignZone()}
                >
                  Agregar Zona
                </Button>
              </div>
            </div>
          </div>
        )}

        {form.zones.length > 0 && (
          <div className="row">
            <div className="col-md my-1">
              <ul className="list-group">
                {form.zones.map((zone, key) => {
                  return (
                    <li
                      key={key}
                      className="list-group-item d-flex justify-content-between align-items-center"
                    >
                      {zone.label}
                      <span
                        style={{ cursor: "pointer" }}
                        className="badge badge-danger badge-pill word-break"
                        onClick={() => this.removeZone(zone.value)}
                      >
                        X
                      </span>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        )}

        <div className="row">
          <div className="col-md">
            <File
              placeholder={isEdit ? "Cambiar Imagen de Perfil (opcional)" : "Agregar Imagen de Perfil (opcional)"}
              placeholdersuccess="Imagen Agregada de Perfil (opcional)"
              showcheck={true.toString()}
              onChange={this.handleChange}
              name="image"
              value={this.state.form.image}
              inputstyle={{
                display: "contents"
              }}
              className="btn-product"
            />
          </div>
        </div>
        <div className="row text-center">
          <div className="col col-md">
            <div id="button">
              <Button
                submitted={submitted ? submitted : undefined}
                block="true"
                type="submit"
                disabled={submitted}
              >
                {this.state.textButton}
              </Button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default CreateEditSeller;
