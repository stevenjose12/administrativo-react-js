import React from "react";

import { Button, Input } from "../../components";
import { Globals, Constants } from "../../utils";
import { Payment } from "../../services";

class ProcessingPayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitted: false,
      form: {
        user_id: props.customer.id,
        expenses: [],
        creator_id: props.Id,
        amount_available: props.customer.amount_available
      },
      customer: { ...props.customer }
    };
  }

  handleCheck = (emitter, key) => {
    const { value } = emitter.target;
    const { form, customer } = this.state;
    const debts = customer.debts[key];

    let amountAvailable;
    let amountOwed;
    let toPay = 0;

    let expense = findExpense(form.expenses, debts);

    const calcAmountAvailable = customer.amount_available - value; // Resta de mi monto acumulado

    const calcAmountOwed =
      calcAmountAvailable < 0
        ? customer.amount_owed - customer.amount_available
        : customer.amount_owed - value; // Resta de mi monto adeudado

    if (expense) {
      const expensesFilter = form.expenses.filter(
        ({ action_id }) => action_id !== expense.action_id
      );

      amountAvailable =
        parseFloat(customer.amount_available) + parseFloat(expense.toPay); // Devuelvo mi monto que resto de mi monto acumulado

      amountOwed = parseFloat(customer.amount_owed) + parseFloat(expense.toPay); // Devuelvo mi monto que resto de mi monto adeudado

      this.setState(state => ({
        customer: {
          ...state.customer,
          amount_available: amountAvailable,
          amount_owed: amountOwed
        },
        form: {
          ...state.form,
          expenses: expensesFilter
        }
      }));

      return;
    } else if (customer.amount_available <= 0) {
      return;
    }

    amountAvailable = calcAmountAvailable <= 0 ? 0 : calcAmountAvailable;
    toPay = amountAvailable <= 0 ? customer.amount_available : value;

    expense = {
      id: debts.id,
      action_id: debts.action_id,
      total: parseFloat(parseFloat(value).toFixed(2)),
      toPay: parseFloat(parseFloat(toPay).toFixed(2)),
      pending: parseFloat(parseFloat(value).toFixed(2)),
      usedPayments: [],
      payed: false
    };

    this.setState(state => ({
      customer: {
        ...state.customer,
        amount_available: amountAvailable,
        amount_owed: calcAmountOwed
      },
      form: {
        ...state.form,
        expenses: [...state.form.expenses, expense]
      }
    }));
  };

  handleSubmit = async (emitter) => {
    emitter.preventDefault();

    const { form, customer } = this.state;
    const { customer:customerProp } = this.props;

    let propAmountAvailable = parseFloat(customerProp.amount_available);

    let stateAmountAvailable = parseFloat(customer.amount_available);

    let expensesFormat = [];
    
    const expenses = [...form.expenses];

    if(expenses.length > 0){

      if (propAmountAvailable <= 0) {
        Globals.showWarning(
          "¡Disculpe, el cliente no tiene saldo suficiente para procesar la operacion!"
        );
        return;
      }

      const expensesAmount = expenses.reduce(function(amount, expense) {
        return amount + expense.toPay;
      }, 0);

      if(expensesAmount > propAmountAvailable) {
        Globals.showWarning(
          "¡Disculpe, el cliente no tiene saldo suficiente para procesar la operacion 2!"
        );
        return;
      }

      expensesFormat = await this.checkExpensesWithPayments(expenses);

    } else {
      if (propAmountAvailable <= 0 && expenses.length <= 0) {
        Globals.showWarning(
          "¡Disculpe, no tiene un monto a favor para procesar esta cuenta!"
        );
        return;
      } else if (propAmountAvailable >= 0 && expenses.length <= 0) {
        let rawExpenses = [];
        for (let indexDebt = 0; indexDebt <  customer.debts.length; indexDebt++) {
          let debt = customer.debts[indexDebt]
          let calcAmount = stateAmountAvailable - debt.expensesMade; // 1,00 - 5,57 = -4,57
          let payment =
            Math.sign(calcAmount) <= Constants.VALUE_ZERO
              ? stateAmountAvailable
              : debt.expensesMade;
  
          stateAmountAvailable -= payment;
  
          if (stateAmountAvailable >= 0) {
            const expense = {
              id: debt.id,
              action_id: debt.action_id,
              total: parseFloat(parseFloat(debt.expensesMade).toFixed(2)),
              toPay: parseFloat(parseFloat(payment).toFixed(2)),
              pending: parseFloat(parseFloat(debt.expensesMade).toFixed(2)),
              payed: false,
              usedPayments: []
            };
  
            rawExpenses = [...rawExpenses, expense];
          }
        };
        expensesFormat = await this.checkExpensesWithPayments(rawExpenses);
      }
    }

    this.setState(
      state => ({
        form: {
          ...state.form,
          expenses: expensesFormat
        }
      }),
      () => this.processingPayment()
    );
  };

  checkExpensesWithPayments = async (expenses) => {

    for (let indexExpense = 0; indexExpense < expenses.length; indexExpense++) {

      const { customer } = this.state;
      let { payments } = customer;
      let paymentsMapped = payments.filter(payment => parseInt(payment.status) === 0 && parseFloat(payment.current_amount) > 0)
      if(paymentsMapped.length === 0) {
        break;
      }
      let currentExpense = expenses[indexExpense];
      let usedPaymentObj = {};

      for (let indexPayment = 0; indexPayment < paymentsMapped.length; indexPayment++) {
        let currentPayment = paymentsMapped[indexPayment];
        if(currentExpense.pending > currentPayment.current_amount) {
          currentExpense.pending = currentExpense.pending - currentPayment.current_amount;
          currentExpense.toProcess = currentPayment.current_amount
          usedPaymentObj = {
            id: currentPayment.id,
            full_payment: false, //PAGO PARCIAL - ABONO
            available_amount: 0,
            used_amount: currentPayment.current_amount,
            status: 1
          }
          currentExpense.usedPayments = [...currentExpense.usedPayments, usedPaymentObj]
          currentPayment.current_amount = 0;
          currentPayment.status = 1;
        } else if (currentExpense.pending === currentPayment.current_amount) {
          currentExpense.pending = currentExpense.pending - currentPayment.current_amount;
          currentExpense.toProcess = currentPayment.current_amount;
          usedPaymentObj = {
            id: currentPayment.id,
            full_payment: true, //PAGO COMPLETO
            available_amount: 0,
            used_amount: currentExpense.pending,
            status: 1
          }
          currentExpense.usedPayments = [...currentExpense.usedPayments, usedPaymentObj]
          currentExpense.payed = true;
          currentPayment.current_amount = 0;
          currentPayment.status = 1;
        } else if (currentExpense.pending < currentPayment.current_amount) {
          currentPayment.current_amount = currentPayment.current_amount - currentExpense.pending;
          usedPaymentObj = {
            id: currentPayment.id,
            full_payment: true, //PAGO COMPLETO
            available_amount: currentPayment.current_amount,
            used_amount: currentExpense.pending,
            status: 0
          }
          currentExpense.pending = 0;
          currentExpense.toProcess = currentPayment.current_amount;
          currentExpense.payed = true;
          currentExpense.usedPayments = [...currentExpense.usedPayments, usedPaymentObj]
        }

        paymentsMapped[indexPayment] = currentPayment
        expenses[indexExpense] = currentExpense;

        await this.setState(
          state => ({
            ...state,
            form: {
              ...state.form,
              expenses: expenses
            },
            customer: {
              ...state.customer,
              payments: payments
            }
          }));
        if(currentExpense.pending === 0){
          break;
        }
      }
    }
    return expenses;
  }

  processingPayment = () => {
    const { form, submitted } = this.state;

    if (submitted) {
      return;
    }

    // Vaciando los campos de errores al enviar solicitud
    this.setState({ submitted: true, errors: [] });

    Payment.processingPayment(form)
      .then(() => {
        Globals.showSuccess(`¡Pago(s) procesado(s) exitosamente!`);
        this.setState({ submitted: false });

        this.props.onClose();
      })
      .catch((err) => Globals.showError(err.message));
  };

  render() {
    const { submitted, customer, form } = this.state;
    const { addPayment } = this.props;

    return (
      <React.Fragment>
        <form onSubmit={this.handleSubmit}>
          {customer ? (
            <div className="container-create-edit-payment">
              <div className="row text-right">
                <div className="col-md">
                  <div className="form-group">
                    <Button type="button" onClick={() => addPayment()}>
                      Agregar Pago
                    </Button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md">
                  <Input
                    color="gray"
                    value={(customer.person || {}).first_name || ""}
                    name="name"
                    label="Nombre y apellido"
                    disabled
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md">
                  <div className="form-group">
                    <div className="table-responsive">
                      <table className="table table-bordered table-sm">
                        <thead>
                          <tr>
                            <td colSpan="2"></td>
                            <th>A Favor</th>
                            <th className="text-center text-success">
                              {Globals.formatMiles(customer.amount_available)}
                            </th>
                            <th></th>
                          </tr>
                          <tr>
                            <th>Tipo</th>
                            <th>Fecha</th>
                            <th>Código</th>
                            <th>Monto</th>
                            <th className="text-center">Seleccionar</th>
                          </tr>
                        </thead>
                        <tbody>
                          {customer.debts.length > 0
                            ? customer.debts.map((debt, key) => {
                                return (
                                  <tr key={key} className={debt.expired ? 'red-row' : ''}>
                                    <td>{debt.type}</td>
                                    <td>{debt.date}</td>
                                    <td>{debt.code}</td>
                                    <td>
                                      {Globals.formatMiles(debt.expensesMade)}
                                    </td>
                                    <td className="text-center">
                                      <input
                                        type="checkbox"
                                        name="payment"
                                        value={debt.expensesMade}
                                        onChange={emitter =>
                                          this.handleCheck(emitter, key)
                                        }
                                        disabled={
                                          customer.amount_available <= 0 &&
                                          !form.expenses.find(
                                            Item =>
                                              Item.action_id === debt.action_id
                                          )
                                        }
                                      />
                                    </td>
                                  </tr>
                                );
                              })
                            : null}

                          <tr>
                            <th colSpan="2">Total Deuda</th>
                            <th colSpan="3" className="text-center text-danger">
                              {Globals.formatMiles(customer.amount_owed)}
                            </th>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="row text-center">
              <div className="col-md">
                <div className="spinner-border text-primary" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            </div>
          )}

          <div className="row text-center">
            <div className="col-md">
              <div id="button">
                <Button
                  submitted={submitted ? submitted : undefined}
                  type="submit"
                  block="true"
                  disabled={submitted}
                >
                  Procesar
                </Button>
              </div>
            </div>
          </div>
        </form>
      </React.Fragment>
    );
  }
}

function findExpense(expenses, { action_id: actionId }) {
  return expenses.find(({ action_id }) => action_id === actionId);
}

export default ProcessingPayment;
