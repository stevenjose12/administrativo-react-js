import React from "react";
import { Button, Input } from "../../components";
import { axios, Globals } from "../../utils";

class CreateEditModel extends React.Component {
  state = {
    form: {},
    textButton: "Crear"
  };
  componentDidMount() {
    if (this.props.edit) this.edit();
  }

  submit = async () => {
    let param = { ...this.state.form };
    Globals.setLoading();
    axios
      .upload(
        this.props.edit ? "admin/models/edit" : "admin/models/create",
        param
      )
      .then(res => {
        this.setState({
          form: {}
        });
        Globals.showSuccess(res.data.msg);
        this.props.onClose();
      })
      .catch(err => {
        if (err.response.status === 422) {
          Globals.showError(err.response.data.msg);
          return;
        }
        Globals.showError();
      })
      .then(() => {
        Globals.quitLoading();
      });
  };

  edit = async () => {
    await this.setState({
      form: {
        name: this.props.edit.element.name,
        code: this.props.edit.element.code,
        brand_id: this.props.edit.element.brand_id,
        creator_id: this.props.edit.element.creator_id,
        id: this.props.edit.element.id
      },
      textButton: "Editar"
    });
  };

  change = e => {
    if (e.target.name === "code") {
      e.target.value = e.target.value.toUpperCase();
    }
    this.setState({
      form: {
        creator_id: this.props.user.id,
        brand_id: this.props.brand_id,
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  };

  render() {
    return (
      <div className="text-center container-create-edit-user">
        <div className="row">
          <div className="col-md-12">
            <Input
              color="gray"
              value={this.state.form.name}
              name="name"
              label="Nombre del modelo"
              onChange={this.change}
            />
          </div>
          <div className="col-md-12">
            <Input
              color="gray"
              value={this.state.form.code}
              name="code"
              label="Código del modelo"
              onChange={this.change}
            />
          </div>
        </div>
        <div id="button">
          <Button block="true" type="button" onClick={() => this.submit()}>
            {this.state.textButton}
          </Button>
        </div>
      </div>
    );
  }
}

export default CreateEditModel;
