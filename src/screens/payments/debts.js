import React from "react";

import { Input } from "../../components";
import { Globals } from "../../utils";

const Debts = props => {
  const { Item } = props;
  return (
    <React.Fragment>
      <div className="row">
        <div className="col-md">
          <Input
            color="gray"
            value={(Item.person || {}).first_name || ""}
            name="name"
            label="Nombre y apellido"
            disabled
          />
        </div>
      </div>
      <div className="row">
        <div className="col-md">
          <div className="form-group">
            <div className="table-responsive">
              <table className="table table-bordered table-sm">
                <thead>
                  <tr>
                    <td>Tipo</td>
                    <td>Fecha</td>
                    <td>Código</td>
                    <td>Monto</td>
                    <td>Vendedor</td>
                  </tr>
                </thead>
                <tbody>
                  {Item.debts.length > 0
                    ? Item.debts.map((debt, key) => {
                        return (
                          <tr key={key} className={debt.expired ? 'red-row' : ''}>
                            <td>{debt.type}</td>
                            <td>{debt.date}</td>
                            <td>{debt.code}</td>
                            <td>{Globals.formatMiles(debt.expensesMade)}</td>
                            <td>{debt.seller}</td>
                          </tr>
                        );
                      })
                    : null}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Debts;
