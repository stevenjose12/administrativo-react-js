import React from 'react';
import {Button, Select, Input, Textarea } from '../../components';
import File from '../../components/file';
import { axios, Globals } from '../../utils';

class CreateChat extends React.Component{
    state={
        form:{},
        users: [],
        edit: false,
        textButton: 'Crear'
    }
    componentDidMount(){
        if (this.props.edit) this.edit()
        this.load();
    }

    load = () => {
        Globals.setLoading();
        axios.post('admin/support/users')
            .then(res => {
                if (res.data.result) {
                    let users = [];
                    if(res.data.users.length > 0){
                        res.data.users.forEach((element, i) => {
                            users.push({
                                label: element.person.name+' '+element.person.lastname,
                                value: element.id
                            })
                        })
                    }
                    this.setState({
                        users: users,
                        form: {
                            user: users[0] && users[0].value ? users[0].value : null,
                        }
                    })
                }
                else {
                    Globals.showError(res.data.msg);
                }
            })
        .catch(err => {
            Globals.showError();
        })
        .then(() => {
            Globals.quitLoading();
        });
    }

    submit = async () => {
        Globals.setLoading();
        axios.upload('admin/support/chatCreate', this.state.form)
            .then(res => {
                if (res.data.result) {
                    this.setState({
                        form:{}
                    })
                    Globals.showSuccess(res.data.msg)
                    this.props.onClose()
                }
                else {
                    Globals.showError(res.data.msg);
                }
            })
        .catch(err => {
            Globals.showError();
        })
        .then(() => {
            Globals.quitLoading();
        });
    }

    change = e => {
		this.setState({
			form: {
				...this.state.form,
				[e.target.name]: e.target.value
			}
        });
    }
    
    render(){
        return(
            <div className="text-center container-create-edit-user">
                <Select
                    color="gray"
                    label="Usuario"
                    name="user"
                    defaultname="user"
                    onChange={ this.change }
                    value={ this.state.form.user }
                    options={this.state.users} />
                <div id="button">
                    <Button block="true" type="button" onClick={() => this.submit()} >
                        {this.state.textButton}
                    </Button>
                </div>
			</div>
        )
    }
}
export default CreateChat;