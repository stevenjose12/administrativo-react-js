import React from "react";
import { connect } from "react-redux";
import { Globals, Constants } from "../../utils";
import { Provider } from "../../services";
import { Select, Input, Button } from "../../components";

class FormProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      form: {
        user_id: props.user.id,
        enterprise_id: props.Id,
        name: "",
        code: "",
        fiscal_document_type: "V",
        fiscal_identification: "",
        phone: "",
        email: "",
        time_limit: 0
      },
      errors: [],
      textButton: "Crear",
      buttonBack: "Ir atras"
    };
  }

  handleSubmit = emitter => {
    emitter.preventDefault();

    const { form, submitted } = this.state;

    if (submitted) {
      return;
    }

    this.setState({ submitted: true, errors: [] });
    Provider.ProviderFromOrder(form)
      .then(response => {
        this.props.provider(response);
        this.setState({ submitted: false, errors: [] });
      })
      .catch(error => {
        this.setState({ submitted: false, errors: error });
      });
  };

  handleChange = emitter => {
    const { name, value } = emitter.target;

    switch (name) {
      default:
        this.setState({
          form: {
            ...this.state.form,
            [name]: value
          }
        });
        break;
    }
  };

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback my-2 text-left">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  render() {
    const { submitted } = this.state;

    return (
      <div className="text-center container-create-edit-order">
        <div className="row">
          <div className="col col-sm">
            <Input
              color="gray"
              value={this.state.form.name}
              name="name"
              label="Nombre del Proveedor"
              error={`${this.hasErrorFor("name") ? "is-invalid" : ""}`}
              invalidfeedback={this.renderErrorFor("name")}
              onChange={this.handleChange}
            />
          </div>
          <div className="col col-sm">
            <Input
              color="gray"
              value={this.state.form.code}
              name="code"
              label="Código del proveedor"
              error={`${this.hasErrorFor("code") ? "is-invalid" : ""}`}
              invalidfeedback={this.renderErrorFor("code")}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="row">
          <div className="col col-sm">
            <div style={{ display: "flex" }}>
              <Select
                color="gray"
                label="Tipo"
                name="fiscal_document_type"
                value={this.state.form.fiscal_document_type}
                error={`${
                  this.hasErrorFor("fiscal_document_type") ? "is-invalid" : ""
                }`}
                invalidfeedback={this.renderErrorFor("fiscal_document_type")}
                onChange={this.handleChange}
                options={Constants.TYPE_DOCUMENTS}
              />
              <Input
                color="gray"
                value={this.state.form.fiscal_identification}
                name="fiscal_identification"
                label="RIF"
                onKeyPress={e => {
                  Globals.soloNumeros(e);
                }}
                maxLength={this.state.form.document_type === "J" ? 11 : 10}
                error={`${
                  this.hasErrorFor("fiscal_identification") ? "is-invalid" : ""
                }`}
                invalidfeedback={this.renderErrorFor("fiscal_identification")}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="col col-sm">
            <Input
              color="gray"
              value={this.state.form.phone}
              name="phone"
              label="Teléfono"
              maxLength={11}
              onKeyPress={e => {
                Globals.soloNumeros(e);
              }}
              error={`${this.hasErrorFor("phone") ? "is-invalid" : ""}`}
              invalidfeedback={this.renderErrorFor("phone")}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="row">
          <div className="col col-sm">
            <Input
              color="gray"
              value={this.state.form.email}
              name="email"
              type="email"
              label="E-Mail"
              error={`${this.hasErrorFor("email") ? "is-invalid" : ""}`}
              invalidfeedback={this.renderErrorFor("email")}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="row">
          <div className="col col-sm">
            <Input
              color="gray"
              value={this.state.form.time_limit}
              name="time_limit"
              type="number"
              label="Plazo de Credito"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm">
            {submitted ? (
              <div className="spinner-border text-primary" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            ) : (
              <div id="button">
                <Button
                  type="button"
                  block="true"
                  onClick={this.handleSubmit}
                  disabled={submitted}
                >
                  {this.state.textButton}
                </Button>
                <Button
                  type="button"
                  block="true"
                  color="danger"
                  onClick={this.props.removeAttribute()}
                  disabled={submitted}
                >
                  {this.state.buttonBack}
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(FormProvider);
