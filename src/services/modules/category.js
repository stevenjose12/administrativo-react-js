import axios from "../../utils/axios";

class Category {
  static async getCategories(form) {
    return new Promise((resolve, reject) => {
      axios.post("admin/categories/get", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }

  static async getSubcategories(form) {
    return new Promise((resolve, reject) => {
      axios.post("admin/subcategories/get", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }

  static async createCategory(form) {
    return new Promise((resolve, reject) => {
      axios.post("admin/categories/category", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }

  static async createSubcategory(form) {
    return new Promise((resolve, reject) => {
      axios.post("admin/subcategories/subcategory", form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }
}

export default Category;
