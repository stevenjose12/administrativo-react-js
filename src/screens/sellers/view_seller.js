import React from "react";

import { Avatar, List } from "../../components";
import { ENV, Constants, Format, Globals } from "../../utils";
import User from "../../assets/img/user.jpg";
import { Branch, Warehouse } from "../../services";

class ViewSeller extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: props.user,
      branches: [],
      branch_id: '',
      selected_branch: {},
      warehouses: [],
      warehouse_id: '',
      selected_warehouse: {}
    };
  }

  componentDidMount() {
    this.maybeLoadBranches();
  }

  maybeLoadBranches = () => {
    const { user, enterprise } = this.props;
    const assignments = (user || {}).assigned_warehouses || [];
    const enterpriseId = enterprise;
    const branchesFiltered = getUnique(assignments, 'branch_id')
    const branch_id = branchesFiltered.length === 1
    ? branchesFiltered[0].branch_id
    : branchesFiltered.length > 1
    ? 0
    : "";

    Promise.all([Branch.getBranchesByEnterprise({ Id: enterpriseId })])
      .then(response => {
        const branchesMap = Format.rawBranches(
          response[0],
          user.assigned_warehouses,
          user.role
        ).map(({ name, id }) => ({
          label: name,
          value: id
        }));

        const branches = [{ label: "Todos", value: 0 }, ...branchesMap]
        const selected_branch = branches.find(branch => branch.value === branch_id);

        this.setState(
          state => ({
            branch_id:
            assignments.length === 1
              ? assignments[0].branch_id
              : assignments.length > 1
              ? 0
              : "",
            branches: branches,
            selected_branch: selected_branch
          }),
          () => this.maybeLoadWarehouses(true)
        );
      })
      .catch(error => {
        Globals.showError();
      });
  };

  maybeLoadWarehouses = (setWarehouse = false) => {
    const { user, enterprise } = this.props;
    const assignments = (user || {}).assigned_warehouses || [];
    const enterpriseId = enterprise;
    const warehousesFiltered = getUnique(assignments, 'warehouse_id')
    const { branch_id } = this.state;
    const warehouse_id = warehousesFiltered.length === 1
    ? warehousesFiltered[0].warehouse_id
    : warehousesFiltered.length > 1
    ? 0
    : "";

    Warehouse.getWarehouseByBranch({
      user_id: enterpriseId,
      branch_id: branch_id
    })
      .then(response => {
        const warehousesMap = response.map(({ id, name }) => ({
          value: id,
          label: name
        }));

        const warehouses = [{ value: 0, label: "Todos" }, ...warehousesMap]
        const selected_warehouse = warehouses.find(warehouse => warehouse.value === warehouse_id);

        this.setState(state => ({
          warehouse_id: setWarehouse ? warehouse_id : 0,
          warehouses: warehouses,
          selected_warehouse: selected_warehouse
        }));
      })
      .catch(() => Globals.showError());
  };

  render() {
    const { user, selected_branch, selected_warehouse } = this.state;
    
    let prices = "Precio Min"
    const hasSalePoint = user.modules_seller.some((moduleSeller) => {
      return moduleSeller.id === Constants.SELLER_MODULES.SALE_POINT
    })
    if(hasSalePoint) {
      prices += user.configuration_seller.price_max ? ', Precio Max' : ''
      prices += user.configuration_seller.price_offer ? ', Precio Oferta.' : ''
    }

    const first_name = (user.person || {}).first_name || "Sin descripción";

    return (
      <div className="text-center container-view-user">
        <Avatar
          source={
            user.person.avatar ? ENV.BasePublic + user.person.avatar : User
          }
          size="130px"
        />
        <List.Container>
          <List.Item label="Nombre de vendedor">
            {user.name}
          </List.Item>
          <List.Item label="Nombre/Razon Social">{first_name}</List.Item>
          <List.Item label="Email">{user.email}</List.Item>
          <List.Item label="Cédula de identidad">
            {user.person.identity_document}
          </List.Item>
          <List.Item label="Teléfono">{user.person.phone}</List.Item>
          <List.Item label="Estatus">
            <span
              className={
                user.status === Constants.STATUS_ACTIVE ? "text-success" : "text-warning"
              }
            >
              {user.status === Constants.STATUS_ACTIVE
                ? "Activo"
                : "Suspendido"}
            </span>
          </List.Item>
          <List.Item label="Sucursal">{ selected_branch ? selected_branch.label : ''}</List.Item>
          <List.Item label="Almacen">{ selected_warehouse ? selected_warehouse.label : ''}</List.Item>
          <List.Item label="Precios">{prices}</List.Item>
          <List.Item label="Tipo de Descuento">{user.configuration_seller.type_percentage === Constants.DISCOUNT_TYPES.byProduct ? "Descuento por producto" :
          user.configuration_seller.type_percentage === Constants.DISCOUNT_TYPES.bySale ? "Descuento por venta" : 
          "No tiene descuento asignado." }</List.Item>
          <List.Item label="Margen de Descuento">{user.configuration_seller.percentage ? user.configuration_seller.percentage + "%" :
          "No tiene margen de descuento asignado." }</List.Item>
          <List.Item>
            {user.modules_seller.length > 0 && (
              <table className="table table-bordered table-products table-sm">
                <thead>
                  <tr>
                    <td className="text-center" colSpan="2">
                      Modulos
                    </td>
                  </tr>
                </thead>
                <tbody>
                  {user.modules_seller.map((Item, key) => {
                    return (
                      <tr key={key}>
                        <td className="text-center">{Item.name}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            )}
            {user.zones.length > 0 && (
              <table className="table table-bordered table-products table-sm">
                <thead>
                  <tr>
                    <td className="text-center" colSpan="2">
                      Zonas
                    </td>
                  </tr>
                </thead>
                <tbody>
                  {user.zones.map((Item, key) => {
                    return (
                      <tr key={key}>
                        <td className="text-center">{Item.name}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            )}
          </List.Item>
        </List.Container>
      </div>
    );
  }
}

function getUnique(arr, comp) {

  const unique = arr
       .map(e => e[comp])

     // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e]).map(e => arr[e]);

   return unique;
}

export default ViewSeller;
