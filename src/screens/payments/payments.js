import React from "react";
import { connect } from "react-redux";

import { Button, Modal, Table, Icon, Pagination } from "../../components";
import { Globals, Format, Constants } from "../../utils";
import { Bank, Request, Payment, Branch, Seller, Zones } from "../../services";

// Components
import Menu from "../menu";
import CreateEditPayment from "./create_edit_payment";
import ProcessingPayment from "./processing_payment";
import Debts from "./debts";
import Filter from "./filterForm";

const TABLE_HEADER = [
  "Nombre/Razón social",
  "Cédula/RIF",
  "Teléfono",
  "Cantidad",
  "Monto Deuda",
  "Monto a Favor",
  "Acciones"
];

class Payments extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      page: 1,
      Item: "",
      current_page: 1,
      last_page: 1,
      visible: false,
      visibleCreate: false,
      visiblePrecessing: false,
      amountOwed: 0,
      amountAvailable: 0,
      customers: [],
      customersFilter: [],
      sellers: [],
      zones: [],
      branches: [],
      banks: []
    };
  }

  componentDidMount() {
    this.load();
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.visibleCreate !== this.state.visibleCreate &&
      this.state.visibleCreate
    ) {
      this.maybeLoad();
    }
  }

  load = () => {
    const { user } = this.props;
    const { page } = this.state;
    
    const zonesId = Globals.getZones(user);
    const warehouse_id = Globals.getAssignedWarehouses(user);
    
    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    const getCustomers =
      user.role === Constants.ROLE_SELLER
        ? Request.getCustomersBySeller({
            zones_id: zonesId,
            enterprise_id: enterpriseId
          })
        : Request.getCustomersByEnterprise({ Id: enterpriseId });

    Promise.all([
      Payment.getCustomers(page, {
        user_id: user.id,
        Id: enterpriseId,
        warehouse_id: warehouse_id,
        role: user.role
       }),
      Branch.getBranchesByEnterprise({ Id: enterpriseId }),
      Seller.getSellersByEnterprise({ user_id: enterpriseId }),
      Zones.getZones(),
      getCustomers,
      Bank.getBanks({enterprise_id: enterpriseId})
    ])
      .then(response => {
        const { current_page, data, last_page } = response[0];

        const paymentsMap = Format.rawPayments(data).filter(
          Item => Item.amount_owed > 0
        );
        const amountOwed = paymentsMap.reduce(function(amount, Payment) {
          return amount + Payment.amount_owed;
        }, 0);

        const amountAvailable = paymentsMap.reduce(function(amount, Payment) {
          return amount + Payment.amount_available;
        }, 0);

        let branches_filtered = response[1].filter(
          ({ deleted_at }) => !deleted_at
        );

        branches_filtered = Format.rawBranches(
          branches_filtered,
          user.assigned_warehouses,
          user.role
        );

        const branchesMap = branches_filtered
          .filter(({ status }) => status === Constants.STATUS_ACTIVE)
          .map(({ id, name }) => ({
            value: id,
            label: name
          }));

        const sellersMap = Format.rawSeller(response[2]);

        const rawZones = response[3].map(({ id, name }) => ({
          value: id,
          label: name
        }));

        const customersMap = response[4];

        const banks = response[5]
          .filter(({ status }) => status === Constants.STATUS_ACTIVE)
          .map(({ id, name, type }) => ({
            value: id,
            label: name,
            type: type
          }));

        this.setState({
          data: paymentsMap,
          current_page: current_page,
          last_page: last_page,
          amountOwed: amountOwed,
          amountAvailable: amountAvailable,
          branches: branchesMap,
          sellers: sellersMap,
          zones: rawZones,
          customersFilter: customersMap,
          banks: banks
        });
      })
      .catch((err) => {
        Globals.showError()
      });
  };

  filterMap = form => {
    const { user } = this.props;
    const { page } = this.state;

    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;

    Payment.getCustomers(page, { role: user.role, Id: enterpriseId, ...form })
      .then(response => {
        const { current_page, data, last_page } = response;
        const paymentsMap = Format.rawPayments(data).filter(
          Item => Item.amount_owed > 0
        );
        const amountOwed = paymentsMap.reduce(function(amount, Payment) {
          return amount + Payment.amount_owed;
        }, 0);

        const amountAvailable = paymentsMap.reduce(function(amount, Payment) {
          return amount + Payment.amount_available;
        }, 0);

        this.setState({
          data: paymentsMap,
          current_page: current_page,
          last_page: last_page,
          amountOwed: amountOwed,
          amountAvailable: amountAvailable,
          form: { ...form },
        });
      })
      .catch(() => Globals.showError());
  };

  maybeLoad() {
    const { user } = this.props;

    const Id = user.role === 3 ? user.id : user.enterprise_users.enterprise_id;

    Payment.getCustomersWithDebts({ Id: Id })
      .then(response => {
        const customersMap = response.filter(
          Item =>
            Item.request.length > 0 && Item.status === Constants.STATUS_ACTIVE
        );

        this.setState({
          customers: customersMap
        });
      })
      .catch(() => Globals.showError());
  };

  loadMore = () => {

    const { user } = this.props;
    const { page, form } = this.state;
    const enterpriseId = (user.enterprise_users || {}).enterprise_id || user.id;
    const warehouse_id = Globals.getAssignedWarehouses(user);

    Payment.getCustomers(page, { ...form, role: user.role, Id: enterpriseId, user_id: user.id, warehouse_id: warehouse_id })
      .then(response => {
        const { current_page, data, last_page } = response;

        const paymentsMap = Format.rawPayments(data).filter(
          Item => Item.amount_owed > 0
        );

        const amountOwed = paymentsMap.reduce(function(amount, Payment) {
          return amount + Payment.amount_owed;
        }, 0);

        const amountAvailable = paymentsMap.reduce(function(amount, Payment) {
          return amount + Payment.amount_available;
        }, 0);

        this.setState({
          data: paymentsMap,
          current_page: current_page,
          last_page: last_page,
          amountOwed: amountOwed,
          amountAvailable: amountAvailable
        });
      })
      .catch(() => Globals.showError());
  };

  addPayment = () => {
    this.setState({ visibleCreate: true, visibleProcessing: false });
  };

  processingPayment = (newPayment) => {

    const { Item, data } = this.state;
    const key = data.findIndex(({ id }) => id === Item.id);
    const newData = [...data];

    newData[key] = {
      ...newData[key],
      amount_available: newData[key].amount_available + parseFloat(newPayment.amount)
    };

    newPayment.amount = parseFloat(newPayment.amount);
    newPayment.current_amount = parseFloat(newPayment.current_amount);
    newPayment.type = parseInt(newPayment.type);
    newPayment.status = 0;

    this.setState(state => ({
      visibleCreate: false,
      visibleProcessing: true,
      Item: {
        ...state.Item,
        amount_available: newData[key].amount_available,
        payments: [...state.Item.payments, newPayment]
      },
      data: newData
    }));
  };

  close = () => {
    this.setState(
      {
        Item: "",
        visible: false,
        visibleCreate: false,
        visibleProcessing: false
      }, 
      this.loadMore());
  };

  render() {
    const {
      visible,
      visibleCreate,
      visibleProcessing,
      customers,
      customersFilter,
      sellers,
      branches,
      zones,
      banks,
      data,
      amountOwed,
      last_page,
      page,
      Item
    } = this.state;
    const { user } = this.props;
    const Id = user.role === 4 ? user.enterprise_users.enterprise_id : user.id;

    return (
      <Menu history={this.props.history}>
        {visible && (
          <Modal title="Listado de deudas" onClose={this.close} visible>
            <Debts Item={Item} />
          </Modal>
        )}

        {visibleCreate && (
          <Modal title="Agregar pago" onClose={this.close} visible>
            <CreateEditPayment
              Id={Id}
              customer={Item}
              customers={customers}
              onClose={this.close}
              processingPayment={this.processingPayment}
              banks={banks}
            />
          </Modal>
        )}

        {visibleProcessing && (
          <Modal
            title="Procesar cuenta por cobrar"
            onClose={this.close}
            visible
          >
            <ProcessingPayment
              Id={Id}
              customer={Item}
              onClose={this.close}
              addPayment={this.addPayment}
            />
          </Modal>
        )}

        <div id="home">
          <div className="container-fluid">
            <label><b>Importante:</b> Aquellas cuentas por cobrar en <b style={{color: 'red'}}>ROJO</b> significan que los plazos de dias de
            creditos para ese cliente se vencieron.</label>
            <Filter
              submitted={form => this.filterMap(form)}
              Id={Id}
              user={user}
              branches={[...branches, { value: 0, label: "Todos" }].reverse()}
              sellers={sellers}
              zones={[...zones, { value: 0, label: "Todos" }]}
              customers={customersFilter.map((Item, key) => {
                return {
                  value: Item.client_id,
                  label:
                    (Item.client.person || {}).first_name ||
                    (Item.client || {}).name
                };
              })}
            />

            <div className="row">
              <div className="col-md">
                <Table
                  data={this.state.data.length}
                  title="Cuentas por cobrar"
                  header={TABLE_HEADER}
                  right={
                    user.role === Constants.ROLE_ENTERPRISE && (
                      <Button
                        title="Agregar Pago"
                        outline="true"
                        small="true"
                        onClick={() => {
                          this.setState({
                            visibleCreate: true
                          });
                        }}
                      >
                        AGREGAR PAGO
                      </Button>
                    )
                  }
                >
                  {data.map((Item, index) => {
                    return (
                      <tr key={index} className={ (Item.configuration_client || {}).days_deadline == 0 ? '' : Item.expiredDebts > 0 ? 'red-row' : ''}>
                        <td>{(Item.person || {}).first_name || "-"}</td>
                        <td>
                          {(Item.person || {}).identity_document ||
                            (Item.person || {}).fiscal_identification ||
                            "-"}
                        </td>
                        <td>{(Item.person || {}).phone || "-"}</td>
                        <td>{Item.request.length}</td>
                        <td className="text-danger">
                          {Globals.formatMiles(Item.amount_owed)}
                        </td>
                        <td className="text-success">
                          {Globals.formatMiles(Item.amount_available)}
                        </td>
                        <td>
                          <Button
                            title="Ver"
                            small="true"
                            onClick={() =>
                              this.setState({ visible: true, Item: Item })
                            }
                          >
                            <Icon name="eye" />
                          </Button>
                            <Button
                              title="Editar"
                              color="primary"
                              small="true"
                              onClick={() =>
                                this.setState({
                                  visibleProcessing: true,
                                  Item: Item
                                })
                              }
                            >
                              <Icon name="edit" />
                            </Button>
                        </td>
                      </tr>
                    );
                  })}

                  {data.length > 0 && (
                    <tr>
                      <th colSpan="4">TOTAL CUENTA POR COBRAR</th>
                      <th colSpan="3" className="text-danger">
                        {Globals.formatMiles(amountOwed)}
                      </th>
                    </tr>
                  )}
                </Table>
              </div>
            </div>

            <div className="row my-3">
              <div className="col-md">
                <Pagination
                  pages={last_page}
                  active={page}
                  onChange={page => {
                    this.setState(
                      {
                        page: page
                      },
                      () => {
                        this.loadMore();
                      }
                    );
                  }}
                />
              </div>
            </div>

          </div>
        </div>
      </Menu>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(Payments);
