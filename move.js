const fs = require('fs-extra')

fs.remove('/var/www/html/admin-reactjs/web')
 .then(()=>{
   console.log('>>: Carpeta Eliminada')
   fs.rename('/var/www/html/admin-reactjs/admin/build', '/var/www/html/admin-reactjs/admin/web', err => {
    if (err) {
      console.error(err)
      return
    }
      
    console.log("Nombre Modificado");
    
    fs.move('/var/www/html/admin-reactjs/admin/web', '/var/www/html/admin-reactjs/web', err => {
      if (err) {
        console.error(err)
        return
      }
  
      console.log('Administrativo Instalado');
    })
  
  })
 })
 .catch(err => {
   console.log('>>: error al eliminar la carpeta', err)
 })