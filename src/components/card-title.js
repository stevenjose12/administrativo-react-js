import React from 'react';

const CardTitle = props => (
	<h2 className="card-title-component">{ props.children }</h2>
)

export default CardTitle;