import axios from "../../utils/axios";

class Movement {
  static async getMovements(page, form) {
    return new Promise((resolve, reject) => {
      axios.post(`admin/movements/get?page=${page}`, form).then(
        ({ data }) => {
          // http success
          resolve(data);
        },
        ({ response }) => {
          const { data } = response;
          // http failed
          reject(data);
        }
      );
    });
  }
}

export default Movement;
