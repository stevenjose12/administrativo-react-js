import React from "react";
import { connect } from "react-redux";
import Menu from "../menu";
import {
  Table,
  Pagination,
  Button,
  Select,
  Icon,
  Modal,
  Input
} from "../../components";
import { axios, Globals } from "../../utils";
import CreateEditClient from "./create_edit_client";
import ViewClient from "./view_client";

const HEADER_TABLE = [
  "Nombre/Razón social",
  "Email",
  "Cedula",
  "RIF",
  "Teléfono",
  "Estatus",
  "Acciones"
];
class Clients extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 1,
      last_page: 1,
      data: [],
      zones: [],
      enterprises: [],
      retentions: [],
      user: null,
      view: false,
      list_status: [
        { value: "", label: "Todos" },
        { value: 1, label: "Activos" },
        { value: 2, label: "Suspendidos" }
      ],
      list_person_type: [
        { value: "", label: "Todos" },
        { value: 1, label: "Naturales" },
        { value: 2, label: "Juridicas" }
      ],
      form: {
        status: "",
        person_type: "",
        zone: "",
        search: "",
        user_id: Globals.getEnterpriseId(props.user),
        role: props.user.role
      }
    };
  }

  componentDidMount() {
    this.load();
  }

  load = () => {
    const { page, form } = this.state;
    Globals.setLoading();
    axios
      .post("admin/clients/get?page=" + page, form)
      .then(res => {
        if (res.data.result) {
          this.setState({
            last_page: res.data.clients.last_page,
            data: res.data.clients.data
          });
        }
      })
      .catch(err => {
        Globals.showError();
      })
      .then(() => {
        this.getEnterprises();
        this.getRetentions();
        this.getZones();
      });
  };

  close = () => {
    this.setState(
      {
        create: false,
        view: false,
        edit: null,
        user: null
      },
      () => this.load()
    );
  };

  delete = item => {
    Globals.confirm("¿Desea eliminar a: " + item.name + "?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/clients/delete", { id: item.id })
        .then(res => {
          Globals.showSuccess(res.data.msg);
          this.load();
        })
        .catch(err => Globals.showError(err.response.data.msg))
        .then(() => Globals.quitLoading());
    });
  };

  suspend = item => {
    let message;
    message = item.status === 1 ? "¿Desea suspender a " : "¿Desea activar a ";
    Globals.confirm(message + item.first_name + "?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/clients/suspend", { id: item.id })
        .then(res => {
          Globals.showSuccess(res.data.msg);
          this.load();
        })
        .catch(err => Globals.showError(err.response.data.msg))
        .then(() => Globals.quitLoading());
    });
  };

  verify = item => {
    Globals.confirm("¿Desea verificar a: " + item.first_name + "?", () => {
      Globals.setLoading("Guardando...");
      axios
        .post("admin/clients/verify", { id: item.id })
        .then(res => {
          Globals.showSuccess(res.data.msg);
          this.load();
        })
        .catch(err => Globals.showError(err.response.data.msg))
        .then(() => Globals.quitLoading());
    });
  };

  change = ({ target }) => {
    const { name, value } = target;

    this.setState(state => ({
      form: {
        ...state.form,
        [name]: value
      }
    }));
  };

  getZones = () => {
    axios
      .get("admin/clients/zones")
      .then(({ data }) => {
        data.unshift({ id: "", name: "Todos" });

        this.setState({
          zones: data
        });
      })
      .catch(() => Globals.showError())
      .then(() => Globals.quitLoading());
  };

  getEnterprises = () => {
    axios
      .get("admin/warehouses/enterprises")
      .then(res => {
        if (res.data.result) {
          this.setState({
            enterprises: res.data.users
          });
        }
      })
      .catch(() => Globals.showError());
  };

  getRetentions = () => {
    axios
      .get("admin/clients/retentions")
      .then(res => {
        this.setState({
          retentions: res.data
        });
      })
      .catch(() => Globals.showError());
  };

  render() {
    const {
      create,
      data,
      edit,
      enterprises,
      last_page,
      list_person_type,
      retentions,
      page,
      person_type,
      search,
      status,
      list_status,
      user: client,
      view,
      zone,
      zones
    } = this.state;

    const { history, user } = this.props;

    const filtered_zones = zones.filter(Item => {
      return Item.id;
    });

    return (
      <Menu history={history}>
        {view && (
          <Modal title="Ver Cliente" onClose={this.close} visible>
            <ViewClient user={client} />
          </Modal>
        )}

        {create && (
          <Modal
            title={edit ? "Editar Cliente" : "Nuevo Cliente"}
            onClose={this.close}
            visible
          >
            <CreateEditClient
              user={user}
              edit={edit}
              enterprises={enterprises}
              zones={filtered_zones}
              retentions={retentions}
              onClose={this.close}
            />
          </Modal>
        )}
        <div id="home">
          <div className="row">
            <div className="col-sm-12 col-md">
              <Input
                color="white"
                name="search"
                label="Búsqueda"
                onChange={this.change}
                value={search}
                placeholder="Buscar por Nombre/Razón social, Email, Cedula, RIF o Teléfono"
              />
            </div>
            <div className="col-sm-12 col-md">
              <Select
                color="white"
                name="status"
                label="Estatus"
                defaultname="Seleccione"
                onChange={this.change}
                value={status}
                options={list_status.map(({ value, label }) => ({
                  value: value,
                  label: label
                }))}
              />
            </div>
            <div className="col-sm-12 col-md">
              <Select
                color="white"
                name="person_type"
                label="Personalidad"
                defaultname="Seleccione"
                onChange={this.change}
                value={person_type}
                options={list_person_type.map(i => {
                  return {
                    value: i.value,
                    label: i.label
                  };
                })}
              />
            </div>
            <div className="col-sm-12 col-md">
              <Select
                color="white"
                name="zone"
                label="Zonas"
                defaultname="Seleccione"
                onChange={this.change}
                value={zone}
                options={zones.map(i => {
                  return {
                    value: i.id,
                    label: i.name
                  };
                })}
              />
            </div>
            <div className="col-sm-12 col-md">
              <Button className="btn-align-bottom" onClick={this.load}>
                Filtrar
              </Button>
            </div>
          </div>
          <Table
            data={data.length}
            title="Clientes"
            header={HEADER_TABLE}
            right={
              <Button
                title="Agregar Clientes"
                outline="true"
                small="true"
                onClick={() => {
                  this.setState({
                    create: true
                  });
                }}
              >
                <Icon name="plus" />
              </Button>
            }
          >
            {data.map((i, index) => {
              return (
                <tr key={index}>
                  <td>{i.first_name}</td>
                  <td>{i.email}</td>
                  <td align={i.identity_document ? "" : "center"}>
                    {i.identity_document ? i.identity_document : "-"}
                  </td>
                  <td align={i.fiscal_identification ? "" : "center"}>
                    {i.fiscal_identification ? i.fiscal_identification : "-"}
                  </td>
                  <td>{i.phone}</td>
                  <td style={{ color: Globals.getStatus(i.status).color }}>
                    {Globals.getStatus(i.status).text}
                  </td>
                  <td>
                    <Button
                      title="Ver"
                      small="true"
                      onClick={() => {
                        this.setState({
                          user: i,
                          view: true
                        });
                      }}
                    >
                      <Icon name="eye" />
                    </Button>
                    <Button
                      title="Editar"
                      color="primary"
                      small="true"
                      onClick={() =>
                        this.setState({
                          create: true,
                          edit: {
                            element: i,
                            index: index
                          }
                        })
                      }
                    >
                      <Icon name="edit" />
                    </Button>
                    {i.status === 2 && (
                      <Button
                        color="info"
                        title="Activar"
                        small="true"
                        onClick={() => {
                          this.suspend(i, index);
                        }}
                      >
                        <Icon name="check" />
                      </Button>
                    )}
                    {i.status === 1 && (
                      <Button
                        color="info"
                        title="Suspender"
                        small="true"
                        onClick={() => this.suspend(i, index)}
                      >
                        <Icon name="close" />
                      </Button>
                    )}
                    <Button
                      color="red"
                      title="Eliminar"
                      small="true"
                      onClick={() => this.delete(i, index)}
                    >
                      <Icon name="trash" />
                    </Button>
                  </td>
                </tr>
              );
            })}
          </Table>

          <Pagination
            pages={last_page}
            active={page}
            onChange={page => {
              this.setState(
                {
                  page: page
                },
                () => {
                  this.load();
                }
              );
            }}
          />
        </div>
      </Menu>
    );
  }
}

export default connect(state => {
  return {
    user: state.user
  };
})(Clients);
