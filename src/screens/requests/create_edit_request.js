import React from "react";
import moment from "moment";

import {
  Button,
  Select,
  Input,
  DatePicker,
  Textarea,
  Icon,
  ModalScan
} from "../../components";
import { Globals, Constants, Format } from "../../utils";
import { Request, Warehouse } from "../../services";

// Components
import FormClient from "./formClient";

const IVA = 0.16;

class CreateEditRequest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      create: props.Item ? false : true,
      search: "",
      showCustomer: false,
      form: {
        id: "",
        forthright: false,
        warehouse_id: "",
        branch_id: "",
        client_id: "",
        code: "",
        currency_id: "",
        date_emission: new Date(),
        observations: "",
        subtotal: 0.0,
        taxable: 0.0,
        exempt: 0.0,
        vat: 0.0,
        total: 0.0,
        creator_id: props.user.id,
        enterprise_id: props.Id,
        products: []
      },
      dates: {
        maxRequestDate: moment.now()
      },
      errors: [],
      products: [],
      suggestions: [],
      warehouses: [],
      textButton: "Crear",
      edit_serial: false,
      item_serial: null
    };
  }

  componentDidMount() {
  const { Id, user } = this.props;
  
    this.setState(state => ({
      form: {
        ...state.form,
        enterprise_id: Id,
        creator_id: user.id
      }
    }));
    
    if (this.props.Item && !this.state.create) {
      const { Item } = this.props;

      this.maybeLoadData(Item, user);
      this.maybeLoadGenerateId();
    } else {
      this.maybeLoadId(Id);
    }
  }

  componentDidUpdate(_, prevState) {
    if (
      prevState.form.branch_id !== this.state.form.branch_id &&
      this.state.form.branch_id
    ) {
      this.maybeLoadWarehouses();
    }

    if (
      prevState.form.forthright !== this.state.form.forthright &&
      this.state.form.forthright
    ) {
      this.maybeLoadGenerateId();
    } else if (
      prevState.form.forthright !== this.state.form.forthright &&
      !this.state.form.forthright
    ) {
      this.maybeLoadId();
    }
  }

  maybeLoadGenerateId = () => {
    const { Id } = this.props;

    Request.generateId({ Id: Id })
      .then(response => {
        this.setState(state => ({
          form: {
            ...state.form,
            code: response
          }
        }));
      })
      .catch(() => Globals.showError());
  };

  maybeLoadData = (Item, user) => {
    const { details } = Item;

    const rawProducts = arrayTransform(details, Item.warehouse_id, user);

    this.setState(state => ({
      form: {
        ...state.form,
        id: Item.id,
        warehouse_id: Item.warehouse_id,
        branch_id: "",
        client_id: Item.client_id,
        code: "",
        currency_id: Item.currency_id,
        date_emission: new Date(),
        observations: Item.observations,
        taxable: Item.taxable,
        exempt: Item.exempt,
        subtotal: Item.subtotal,
        vat: Item.vat,
        total: Item.total,
        products: rawProducts
      },
      textButton: "Procesar"
    }));
  };

  maybeLoadWarehouses = () => {
    const { form } = this.state;
    const { user } = this.props;

    const userId = (user.enterprise_users || {}).enterprise_id || user.id;

    Warehouse.getWarehouseByBranch({
      user_id: userId,
      branch_id: form.branch_id
    }).then(response => {
      let warehouses_filtered = response.filter(
        ({ deleted_at }) => !deleted_at
      );

      const warehousesMap = Format.rawWarehouse(
        warehouses_filtered,
        user.assigned_warehouses,
        user.role
      )
        .filter(({ status }) => status === Constants.STATUS_ACTIVE)
        .map(({ id, name }) => ({
          value: id,
          label: name
        }));

      this.setState(state => ({
        form: {
          ...state.form,
          warehouse_id: ""
        },
        warehouses: warehousesMap
      }));
    });
  };

  AutoComplete = ({ search }) => {
    const { products } = this.props;
    const { form } = this.state;

    if (!search) {
      this.setState({ suggestions: [] });
      return;
    }

    let searchLowerCase = search.toLowerCase();

    const filterProducts = products.filter(Item => {
      let nameLowerCase = Item.name.toLowerCase();

      return (
        nameLowerCase.indexOf(searchLowerCase) !== -1 &&
        parseInt(form.warehouse_id) === Item.warehouse_id &&
        Item.last_movement > 0 &&
        form.products.every(query => {
          return parseInt(query.id) !== parseInt(Item.id);
        })
      );
    });

    this.setState({ suggestions: filterProducts });
  };

  handleClick = Product => {
    const { form } = this.state;

    Globals.confirm(
      `¿Desea remover del pedido el producto: ${Product.name}?`,
      () => {
        const filterMap = form.products.filter(Item => Item.id !== Product.id);

        this.setState(state => ({
          form: {
            ...state.form,
            products: filterMap,
            subtotal: FloatNum(reduceMap(filterMap, "subtotal")),
            taxable: FloatNum(taxableMap(filterMap)),
            exempt: FloatNum(exemptMap(filterMap)),
            vat: FloatNum(reduceMap(filterMap, "vat")),
            total: FloatNum(reduceMap(filterMap, "total"))
          }
        }));
      }
    );
  };

  handleChange = emitter => {
    const { name, value } = emitter.target;
    const { form } = this.state;

    switch (name) {
      case "search":
        this.setState({ [name]: value }, () => this.AutoComplete(this.state));
        break;
      case "warehouse_id":
      case "branch_id":
        if (form.products.length === 0) {
          this.setState(state => ({
            form: {
              ...state.form,
              [name]: value
            }
          }));
          break;
        }

        Globals.sure(
          "¡Perderá su información de productos al cambiar esta opción!",
          ({ value: Status }) => {
            switch (Status) {
              case true:
                this.setState(state => ({
                  form: {
                    ...state.form,
                    [name]: value,
                    products: []
                  }
                }));
                break;
              default:
                break;
            }
          }
        );
        break;
      default:
        this.setState(state => ({
          form: {
            ...state.form,
            [name]: value
          }
        }));
        break;
    }
  };

  handleNum = (emitter, key) => {
    const { name, value } = emitter.target;
    const { form } = this.state;

    let newValue = value ? parseInt(value) : value;
    let rawProducts = form.products;
    let quantityValue;
    let rate = 1;

    switch (name) {
      case "quantity":
        quantityValue =
          newValue > 0 && newValue !== ""
            ? newValue * rawProducts[key].price
            : 0;

        if (
          rawProducts[key].stock_min >=
          rawProducts[key].last_movement - newValue
        ) {
          Globals.showWarning(
            `¡El producto ${rawProducts[key].name} estara por debajo del stock mínimo!`
          );
        }

        rawProducts[key] = {
          ...rawProducts[key],
          quantity: newValue,
          subtotal: parseFloat(quantityValue),
          vat: FloatNum(rawProducts[key].exempt ? 0 : quantityValue * IVA),
          total: FloatNum(
            rawProducts[key].exempt
              ? quantityValue
              : quantityValue * IVA + quantityValue
          ),
          stock: rawProducts[key].last_movement - newValue
        };
        break;
      case "rate":
        rate =
          parseInt(value) === 1
            ? rawProducts[key].price_base
            : parseInt(value) === 2
            ? rawProducts[key].price_max
            : rawProducts[key].price_offer;

        quantityValue = rawProducts[key].quantity * rate;

        rawProducts[key] = {
          ...rawProducts[key],
          [name]: parseInt(value),
          price: rate,
          subtotal: parseFloat(quantityValue),
          vat: FloatNum(rawProducts[key].exempt ? 0 : quantityValue * IVA),
          total: FloatNum(
            rawProducts[key].exempt
              ? quantityValue
              : quantityValue * IVA + quantityValue
          )
        };
        break;
    }

    this.setState(state => ({
      form: {
        ...state.form,
        products: rawProducts
      }
    }));

    this.setState(state => ({
      form: {
        ...state.form,
        subtotal: FloatNum(reduceMap(rawProducts, "subtotal")),
        taxable: FloatNum(taxableMap(rawProducts)),
        exempt: FloatNum(exemptMap(rawProducts)),
        vat: FloatNum(reduceMap(rawProducts, "vat")),
        total: FloatNum(reduceMap(rawProducts, "total"))
      }
    }));
  };

  takeProduct = Product => {
    const exists = this.state.form.products.find(Item => {
      return Item.id === Product.id;
    });

    if (Product.quantity == "") {
      Product.quantity = 0;
    }

    if (!exists) {
      this.setState(state => ({
        search: "",
        form: {
          ...state.form,
          products: [...state.form.products, Product]
        },
        suggestions: []
      }));
    }
  };

  handleSubmit = emitter => {
    emitter.preventDefault();

    const { form, submitted, create } = this.state;
    if (submitted) {
      return;
    }

    // Filtrando productos para valores nulos
    const findValuesNull = form.products.filter(
      Item => Item.quantity <= 0 || Item.quantity === ""
    );

    // Filtrando productos con precios en 0
    const findNotValues = form.products.filter(Item => Item.total <= 0);

    if (form.products.length === 0) {
      Globals.showError("¡El pedido debe tener productos agregados!");
      return;
    } else if (findValuesNull.length > 0) {
      Globals.showError("¡Los productos deben tener un valor mayor a 0!");
      return;
    } else if (findNotValues.length > 0) {
      Globals.showError("¡Los productos deben tener un precio mayor a 0!");
      return;
    }

    // Validando si hay productos por serializar
    if(!create)
    {
      const cant = form.products.filter(
        i =>
          i.serialization == Constants.SERIALIZATION.ACTIVE && i.serials == null
      ).length;
      if (cant > 0) {
        Globals.showError("¡Hay productos que necesitan serializarse!");
        return;
      }

    }


    // Vaciando los campos de errores al enviar solicitud
    this.setState({ submitted: true, errors: [] });
    switch (create) {
      case true:
        Request.createRequest(form)
          .then(response => {
            const { code } = response;
            this.setState(this.defaultState);
            Globals.showSuccess(`¡Pedido Nº ${code} creada exitosamente!`);
            this.props.onClose();
          })
          .catch(error => {
            if ((error || {}).message) {
              Globals.showError(error.message);
            }

            if (error.hasOwnProperty("msg")) {
              Globals.showError(error.msg);
            }

            this.setState({ submitted: false, errors: error });
          });
        break;
      case false:
        Request.updateRequest(form)
          .then(response => {
            const { code } = response;

            this.setState(this.defaultState);
            Globals.showSuccess(`¡Pedido Nº ${code} creada exitosamente!`);
            this.props.onClose();
          })
          .catch(error => {
            if ((error || {}).message) {
              Globals.showError(error.message);
            }

            if (error.hasOwnProperty("msg")) {
              Globals.showError(error.msg);
            }
            
            this.setState({ submitted: false, errors: error });
          });
        break;
      default:
        break;
    }
  };

  maybeLoadId = () => {
    const { Id } = this.props;

    Request.getId({ Id: Id })
      .then(response => {
        this.setState(state => ({
          form: {
            ...state.form,
            code: response
          }
        }));
      })
      .catch(() => Globals.showError());
  };

  removeAttribute = () => {
    this.setState({ showCustomer: false }, () => this.props.removeAttribute());
  };

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback my-2 text-left">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  handleSerial = item => {
    this.setState({
      edit_serial: true,
      item_serial: item
    });
  };

  onSuccess = async item => {
    let products = [...this.state.form.products];
    const index = products.findIndex(i => i.id == this.state.item_serial.id);
    products[index].serials = item;
    products[index].quantity = item.length;
    await this.setState({
      edit_serial: false,
      form: {
        ...this.state.form,
        products: products
      }
    });
    this.handleNum(
      {
        target: {
          value: item.length,
          name: "quantity"
        }
      },
      index
    );
  };

  onCancel = () => {
    this.setState({
      edit_serial: false
    });
  };

  render() {
    const { currencies, customers, branches, Id, havePermission } = this.props;
    const {
      dates,
      form,
      search,
      submitted,
      showCustomer,
      suggestions,
      warehouses,
      create,
      edit_serial,
      item_serial
    } = this.state;
    return (
      <React.Fragment>
        {edit_serial ? (
          <React.Fragment>
            <ModalScan
              onSuccess={this.onSuccess}
              onCancel={this.onCancel}
              serials={item_serial.serials}
            />
          </React.Fragment>
        ) : (
          <form onSubmit={this.handleSubmit}>
            {showCustomer ? (
              <FormClient
                Id={Id}
                back
                customer={Customer =>
                  this.setState({ showCustomer: false }, () => {
                    this.props.newCustomer(Customer);
                  })
                }
                removeAttribute={() => this.removeAttribute}
              />
            ) : (
              <div className="text-center container-create-edit-request">
                {/* {create && (
                  <div className="row text-left">
                    <div className="col-md-6">
                      <div className="form-group">
                        <div className="form-check">
                          <input
                            id="forthright"
                            type="checkbox"
                            name="forthright"
                            value={this.state.form.forthright}
                            onClick={() => {
                              let boolean = this.state.form.forthright;

                              this.setState(state => ({
                                form: {
                                  ...state.form,
                                  forthright: !boolean,
                                  date_emission: !boolean ? moment.now() : ""
                                }
                              }));
                            }}
                            className="form-check-input"
                          />
                          <label className="form-check-label" htmlFor="forthright">
                            Nota de entrega
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                )} */}
                <div className="row">
                  <div className="col-md">
                    <Select
                      color="gray"
                      label="Cliente"
                      name="client_id"
                      options={customers.map(Item => {
                        return {
                          value: Item.client_id,
                          label:
                            (Item.client.person || {}).first_name ||
                            (Item.client || {}).name
                        };
                      })}
                      disabled={!create}
                      value={form.client_id}
                      error={`${
                        this.hasErrorFor("client_id") ? "is-invalid" : ""
                      }`}
                      invalidfeedback={this.renderErrorFor("client_id")}
                      onChange={this.handleChange}
                      icon={
                        create && havePermission ? (
                          <Button
                            color="blue"
                            type="button"
                            title="Agregar otro"
                            small="true"
                            onClick={() => {
                              let inverseShow = !showCustomer;
                              this.setState(
                                { showCustomer: inverseShow },
                                () => {
                                  this.props.removeAttribute();
                                }
                              );
                            }}
                          >
                            <Icon name={showCustomer ? "minus" : "plus"} />
                          </Button>
                        ) : null
                      }
                    />
                  </div>
                  <div className="col-md">
                    <Input
                      type="text"
                      color="gray"
                      value={form.code}
                      placeholder="Código alfanumérico"
                      name="code"
                      maxLength="10"
                      error={`${this.hasErrorFor("code") ? "is-invalid" : ""}`}
                      invalidfeedback={this.renderErrorFor("code")}
                      disabled
                      label={
                        !create || form.forthright
                          ? "Nro. de Nota de Entrega"
                          : "Nro. de Pedido"
                      }
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-md">
                    <Select
                      color="gray"
                      label="Divisa"
                      name="currency_id"
                      options={currencies}
                      disabled={!create}
                      value={form.currency_id}
                      error={`${
                        this.hasErrorFor("currency_id") ? "is-invalid" : ""
                      }`}
                      invalidfeedback={this.renderErrorFor("currency_id")}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-md">
                    <DatePicker
                      color="gray"
                      label={
                        !create || form.forthright
                          ? "Fecha de emisión"
                          : "Fecha de registro"
                      }
                      maxDate={dates.maxRequestDate}
                      value={form.date_emission}
                      disabled={!create || form.forthright}
                      onChange={value => {
                        const name = "date_emission";
                        this.setState(state => ({
                          form: {
                            ...state.form,
                            [name]: value
                          }
                        }));
                      }}
                    />
                  </div>
                </div>
                {create && (
                  <div className="row">
                    <div className="col col-md">
                      <Select
                        color="gray"
                        name="branch_id"
                        label="Sucursal"
                        value={form.branch_id}
                        options={branches}
                        error={`${
                          this.hasErrorFor("branch_id") ? "is-invalid" : ""
                        }`}
                        invalidfeedback={this.renderErrorFor("branch_id")}
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="col col-md">
                      <Select
                        color="gray"
                        name="warehouse_id"
                        label="Almacen"
                        onChange={this.handleChange}
                        value={this.state.form.warehouse_id}
                        options={warehouses}
                        error={`${
                          this.hasErrorFor("warehouse_id") ? "is-invalid" : ""
                        }`}
                        invalidfeedback={this.renderErrorFor("warehouse_id")}
                      />
                    </div>
                  </div>
                )}
                {create && form.warehouse_id && (
                  <div className="row">
                    <div className="col col-md">
                      <Input
                        color="gray"
                        name="search"
                        placeholder="Introduce el nombre"
                        label="Buscar Productos"
                        value={search}
                        onChange={this.handleChange}
                        error={`${
                          this.hasErrorFor("products") ? "is-invalid" : ""
                        }`}
                        invalidfeedback={this.renderErrorFor("products")}
                      />
                      <div id="suggestion">
                        {suggestions.length > 0
                          ? suggestions.map((Item, key) => {
                              return (
                                <div
                                  key={key}
                                  onClick={() => {
                                    this.takeProduct(Item);
                                  }}
                                >
                                  {Item.code + " - " + Item.name}
                                </div>
                              );
                            })
                          : null}
                      </div>
                    </div>
                    <div className="col col-md"></div>
                  </div>
                )}
                {form.products.length > 0 && (
                  <div className="row">
                    <div className="col-md">
                      <div className="form-group">
                        <div className="table-responsive table-request">
                          <table className="table table-bordered table-products table-sm">
                            <thead>
                              <tr>
                                <td colSpan="7">PRODUCTOS</td>
                              </tr>
                              <tr style={{ whiteSpace: "nowrap" }}>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Cantidad</th>
                                <th>Tasa de cobro</th>
                                <th>Precio</th>
                                <th>Total</th>
                                <th>Acción</th>
                              </tr>
                            </thead>
                            <tbody>
                              {form.products.map((Item, key) => {
                                return (
                                  <tr key={key}>
                                    <td>{Item.code}</td>
                                    <td>{Item.name}</td>
                                    <td>
                                      {
                                        <input
                                          type="text"
                                          name="quantity"
                                          className="form-control-sm"
                                          value={Item.quantity}
                                          disabled={
                                            Item.serialization ===
                                            Constants.SERIALIZATION.ACTIVE && !create
                                          }
                                          maxLength="10"
                                          onKeyPress={emitter => {
                                            Globals.soloNumeros(emitter);
                                          }}
                                          onChange={async emitter => {
                                            let products = [
                                              ...this.state.form.products
                                            ];
                                            products[key].serials = null;
                                            this.setState({
                                              edit_serial: false,
                                              form: {
                                                ...this.state.form,
                                                products: products
                                              }
                                            });
                                            this.handleNum(emitter, key);
                                          }}
                                        />
                                      }
                                    </td>
                                    {
                                      <th>
                                        <Select
                                          color="gray"
                                          name="rate"
                                          size="small"
                                          onChange={emitter => {
                                            this.handleNum(emitter, key);
                                          }}
                                          value={Item.rate}
                                          options={Item.prices.map(
                                            ({ value, label }) => ({
                                              value: value,
                                              label: label
                                            })
                                          )}
                                        />
                                      </th>
                                    }
                                    <td>{Globals.formatMiles(Item.price)}</td>
                                    <td>
                                      {Globals.formatMiles(Item.subtotal)}
                                    </td>
                                      <th>
                                        <div className="container-btn-actions">
                                          <Button
                                            color="red"
                                            small="true"
                                            title="Remover"
                                            type="button"
                                            onClick={() => {
                                              this.handleClick(Item);
                                            }}
                                          >
                                            <Icon name="minus" />
                                          </Button>
                                          {/*
                                              Boton de serialización, se muestra solo si se puede serializar el producto
                                            */}
                                          {Item.serialization ===
                                            Constants.SERIALIZATION.ACTIVE && !create && (
                                            <Button
                                              color={
                                                !Item.serials
                                                  ? "yellow"
                                                  : "green"
                                              }
                                              small="true"
                                              title={
                                                !Item.serials
                                                  ? "Serializar"
                                                  : "Serializado"
                                              }
                                              type="button"
                                              className="btn-actions-orders"
                                              onClick={() => {
                                                this.handleSerial(Item);
                                              }}
                                            >
                                              <Icon name="list" />
                                            </Button>
                                          )}
                                        </div>
                                      </th>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                <div className="row">
                  <div className="col-md-12">
                    <Textarea
                      color="gray"
                      value={form.observations}
                      name="observations"
                      label="Observaciones"
                      onChange={this.handleChange}
                      error={`${
                        this.hasErrorFor("observations") ? "is-invalid" : ""
                      }`}
                      invalidfeedback={this.renderErrorFor("observations")}
                    />
                  </div>
                </div>
                {form.products.length > 0 && (
                  <div className="row">
                    <div className="col-md">
                      <div className="form-group">
                        <div className="table-responsive">
                          <table className="table table-bordered table-sm">
                            <thead>
                              <tr>
                                <th style={{ width: "25%" }}>SUBTOTAL:</th>
                                <td className="text-left">
                                  {Globals.formatMiles(form.subtotal)}
                                </td>
                              </tr>
                              <tr>
                                <th style={{ width: "25%" }}>EXENTO:</th>
                                <td className="text-left">
                                  {Globals.formatMiles(form.exempt)}
                                </td>
                              </tr>
                              <tr>
                                <th style={{ width: "25%" }}>
                                  BASE IMPONIBLE:
                                </th>
                                <td className="text-left">
                                  {Globals.formatMiles(form.taxable)}
                                </td>
                              </tr>
                              <tr>
                                <th style={{ width: "25%" }}>IVA:</th>
                                <td className="text-left">
                                  {Globals.formatMiles(form.vat)}
                                </td>
                              </tr>
                              <tr>
                                <th style={{ width: "25%" }}>TOTAL:</th>
                                <td className="text-left">
                                  {Globals.formatMiles(form.total)}
                                </td>
                              </tr>
                            </thead>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                <div className="row">
                  <div className="col-md">
                    {submitted ? (
                      <div
                        className="spinner-border text-primary"
                        role="status"
                      >
                        <span className="sr-only">Loading...</span>
                      </div>
                    ) : (
                      <div id="button">
                        <Button block="true" type="submit" disabled={submitted}>
                          {this.state.textButton}
                        </Button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            )}
          </form>
        )}
      </React.Fragment>
    );
  }
}

function arrayTransform(Items, warehouse_id, user) {
  return Items.map((Item, key) => {
    let query = Item.product_warehouse.find(
      Item => Item.warehouse_id === parseInt(warehouse_id)
    );

    const price_base =
      parseInt(Item.pivot.rate) === 1
        ? query.price
        : parseInt(Item.pivot.rate) === 2
        ? query.price_max
        : query.price_offer;

    let prices = []
    if(user.role === Constants.ROLE_SELLER) {
      prices = [
        {
          value: 1,
          label: "Precio Min."
        }
      ]
      if(user.configuration_seller.price_max === 1)
        prices.push({value: 2, label: "Precio Max."})
      if(user.configuration_seller.price_offer === 1)
        prices.push({value: 3, label: "Precio Oferta"})

    } else {
      prices = [
        {
          value: 1,
          label: "Precio Min."
        },
        {
          value: 2,
          label: "Precio Max."
        },
        {
          value: 3,
          label: "Precio Oferta"
        }
      ]
    }

    return {
      id: Item.id,
      name: Item.name,
      code: Item.code,
      warehouse_id: query.warehouse_id,
      prices: prices,
      stock_min: query.stock_min,
      quantity: Item.pivot.quantity,
      rate: Item.pivot.rate,
      price: price_base,
      price_base: query.price,
      price_max: query.price_max,
      price_offer: query.price_offer,
      exempt: Item.exempt,
      subtotal: Item.pivot.subtotal,
      vat: Item.pivot.vat,
      total: Item.pivot.total,
      stock: 0,
      serialization: Item.serialization
    };
  });
}

function taxableMap(Items) {
  return Items.filter(Item => Item.exempt === Constants.EXEMPT_ACTIVE).reduce(
    (value, product) => {
      return value + parseFloat(product.subtotal);
    },
    0
  );
}

function exemptMap(Items) {
  return Items.filter(Item => Item.exempt === Constants.EXEMPT_INACTIVE).reduce(
    (value, product) => {
      return value + parseFloat(product.subtotal);
    },
    0
  );
}

function reduceMap(Items, name) {
  return Items.reduce((value, product) => {
    return value + parseFloat(product[name]);
  }, 0);
}

function FloatNum(number) {
  return number.toFixed(2);
}

CreateEditRequest.defaultState = {
  form: {
    warehouse_id: "",
    branch_id: "",
    customer_id: "",
    code: "",
    currency_id: "",
    date_emission: new Date(),
    observations: "",
    subtotal: 0,
    vat: 0,
    total: 0,
    creator_id: "",
    enterprise_id: "",
    products: []
  }
};

export default CreateEditRequest;
